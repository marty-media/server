# Marty Media Server
[![Latest Release](https://gitlab.com/marty-media/server/-/badges/release.svg)](https://gitlab.com/marty-media/server/-/releases)
[![pipeline status](https://gitlab.com/marty-media/server/badges/main/pipeline.svg)](https://gitlab.com/marty-media/server/-/commits/main)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=marty-media_server&metric=alert_status)](https://sonarcloud.io/summary/overall?id=marty-media_server)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=marty-media_server&metric=coverage)](https://sonarcloud.io/summary/overall?id=marty-media_server)

The perfect media server for movies and TV shows.

**!!! This project is work in progress and not stable !!!**

## Download instructions and Documentation for Users
Check out the documentation following [marty-media.org/docs](https://www.marty-media.org/docs/)

## For Developers
### Run Locally
Requirements:
- nodejs 20
- golang 21

First, clone this repository. Then run Node.js to install all dependencies:
```sh
npm install
```

Before running the backend you must initially run at least once the build:
```sh
npm run build
```

To start the frontend simply run:
```sh
npm run dev
```

To start the backend simply run:
```sh
go run -tags="develop" ./cmd/server server --config=config.json
```
The flag `develop` allows to modify static files (like CSS or JavaScript for `apidoc`) without restarting the server on every change.

## Visual Studio Code
Use the following plugins:
- [Vue Language Features (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.volar)
- [Tailwind CSS IntelliSense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)

Disable `Vetur`, because it is incompatible with `Vue Language Features (Volar)`.

## License
Marty Media Server (including all fixes and merge requests) is published under the [Server Side Public License](LICENSE).
