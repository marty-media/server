# Release Checklist
## Publish Public Beta
- update versions in
    - internal/api/version.txt
    - internal/api/CHANGELOG.md
    - internal/api/documentation/swagger/index.html
- merge/rebase content from `develop` to `beta` branch
- approve the `semantic-release` job
- update version number in `server/container.js` of the `website` project

## Publish Release
- rebase `beta` branch to `main` branch
- approve the `semantic-release` job
- publish new release on social media
    - [Mastodon](https://mastodon.social/@martymedia)
- update version number in `server/container.js` of the `website` project
- update Helm Chart
