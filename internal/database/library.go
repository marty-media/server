/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/marty-media/server/internal/common"
)

type LibraryType int64

const (
	LibraryTypeMovie  LibraryType = 1
	LibraryTypeTVShow LibraryType = 2
)

var (
	allLibraryTypes = []LibraryType{
		LibraryTypeMovie,
		LibraryTypeTVShow,
	}
)

type Library struct {
	ID         int64        `db:"id,autoincrement"`
	Name       string       `db:"name,requiredForInsert"`
	Type       LibraryType  `db:"type,requiredForInsert"`
	Language   string       `db:"language,requiredForInsert"` // ISO639_3
	CreatedAt  time.Time    `db:"created_at,nowForInsert"`
	ModifiedAt sql.NullTime `db:"modified_at"`
}

func (library Library) TableName() string {
	return "library"
}

func (library Library) Validate() error {
	// validate name
	if err := validateLibraryName(library.Name); err != nil {
		return err
	}

	// validate type
	validLibraryType := false
	for _, libraryType := range allLibraryTypes {
		if library.Type == libraryType {
			validLibraryType = true
			break
		}
	}
	if !validLibraryType {
		return fmt.Errorf("invalid library type")
	}

	// validate language
	validLanguage := false
	for _, language := range common.ISO639_3 {
		if language.ISO639_3 == library.Language {
			validLanguage = true
			break
		}
	}
	if !validLanguage {
		return fmt.Errorf("invalid language")
	}

	return nil
}

func validateLibraryName(name string) error {
	if name == "" {
		return fmt.Errorf("missing name")
	} else if len(name) > 40 {
		return fmt.Errorf("name too long")
	}
	return nil
}

func (transaction *Transaction) GetLibraries() ([]*Library, error) {
	return selectRows[Library](transaction, selectRowsRequest{})
}

func (db *Database) GetLibraries() ([]*Library, error) {
	result, err := singleExecute(db, func(tx *Transaction) (*genericSelectListResult[Library], error) {
		data, err := tx.GetLibraries()
		return &genericSelectListResult[Library]{
			data: data,
		}, err
	})
	return result.data, err
}

func (transaction *Transaction) CreateLibrary(library Library) (int64, error) {
	entries := []DatabaseTable{library}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: library.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("database insert failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreateLibrary(library Library) (int64, error) {
	return singleInsert(db, func(tx *Transaction) (*genericInsertResult, error) {
		id, err := tx.CreateLibrary(library)
		return &genericInsertResult{
			id: id,
		}, err
	})
}

func (transaction *Transaction) GetLibrary(id int64) (*Library, error) {
	// load rows from database
	return selectRow[Library](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) GetLibrary(id int64) (*Library, error) {
	return singleExecute(db, func(tx *Transaction) (*Library, error) {
		return tx.GetLibrary(id)
	})
}

// TODO: create update request struct
func (transaction *Transaction) UpdateLibrary(id int64, name string, updateName bool) error {
	// build request
	request := updateRowsRequest{
		tableName: Library{}.TableName(),
		fields: []updateField{{
			columnName:        "modified_at",
			nowTimestampValue: true,
		}},
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	}
	if updateName {
		if err := validateLibraryName(name); err != nil {
			return err
		}
		request.fields = append(request.fields, updateField{
			columnName: "name",
			value:      name,
		})
	}

	// execute update
	err := transaction.updateRows(request)
	if err != nil {
		log.WithField("inherit", err).Error("update of library failed")
		return err
	}

	return nil
}

func (db *Database) UpdateLibrary(id int64, name string, updateName bool) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.UpdateLibrary(id, name, updateName)
	})
	return err
}

func (transaction *Transaction) DeleteLibrary(id int64) error {
	return transaction.deleteRows(deleteRowsRequest{
		tableName: Library{}.TableName(),
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) DeleteLibrary(id int64) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.DeleteLibrary(id)
	})
	return err
}
