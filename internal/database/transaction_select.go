/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
)

type selectRowsRequest struct {
	filter     []filterRow
	limit      int64
	forUpdate  bool
	skipLocked bool
}

func selectRows[T DatabaseTable](transaction *Transaction, request selectRowsRequest) ([]*T, error) {
	queryString, queryParameters := buildSelectQuery[T](request)

	// execute statement
	rows, err := transaction.tx.Query(queryString, queryParameters...)
	if err != nil {
		log.WithFields(log.Fields{
			"query":   queryString,
			"inherit": err,
		}).Error("query execution failed")
		return nil, err
	}

	// parse results
	fields := getFieldsForSelect[T]()
	var responseRows []*T
	for rows.Next() {
		// create new struct instance
		row := new(T)

		// create fields for scan
		scanFields := getScanFieldsForSelect[T](fields)

		// scan fields
		if err := rows.Scan(scanFields...); err != nil {
			log.WithField("inherit", err).Error("scan of result failed")
			return nil, err
		}

		// build response
		mapFieldsForSelect(row, fields, scanFields)
		responseRows = append(responseRows, row)
	}

	return responseRows, nil
}

func selectRow[T DatabaseTable](transaction *Transaction, request selectRowsRequest) (*T, error) {
	rows, err := selectRows[T](transaction, request)
	if len(rows) > 0 {
		return rows[0], err
	}
	return nil, err
}

func getFieldsForSelect[T DatabaseTable]() []reflect.StructField {
	val := reflect.ValueOf(*new(T))
	t := val.Type()

	// parse fields of struct
	response := []reflect.StructField{}
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		// parse tag
		tagString, _ := field.Tag.Lookup(tagName)
		if tagString == "" {
			continue
		}

		// store field
		response = append(response, field)
	}

	return response
}

func getColumnsForSelect[T DatabaseTable]() []string {
	fields := getFieldsForSelect[T]()
	var response []string
	for _, field := range fields {
		// parse tag
		tagString, _ := field.Tag.Lookup(tagName)
		columnName := tagByString(tagString).Name
		response = append(response, columnName)
	}
	return response
}

func getScanFieldsForSelect[T DatabaseTable](fields []reflect.StructField) []interface{} {
	var response []interface{}
	for _, field := range fields {
		val := reflect.New(field.Type)
		response = append(response, val.Interface())
	}
	return response
}

func mapFieldsForSelect[T DatabaseTable](response *T, fields []reflect.StructField, scanFields []interface{}) {
	reflectResponse := reflect.ValueOf(response)
	directResponse := reflect.Indirect(reflectResponse)
	for i, field := range fields {
		val := reflect.ValueOf(scanFields[i])
		valueField := directResponse.FieldByName(field.Name)
		valueField.Set(reflect.Indirect(val))
	}
}

func buildSelectQuery[T DatabaseTable](request selectRowsRequest) (string, []any) {
	query := "SELECT "
	for i, columnName := range getColumnsForSelect[T]() {
		if i != 0 {
			query += ", "
		}
		query += "\"" + columnName + "\""
	}

	tableName := T.TableName(*new(T))
	query += " FROM \"mms\".\"" + tableName + "\""

	// build where condition
	whereSQL, parameters := buildWhereClause(request.filter, 0)
	query += whereSQL

	// check for row limit
	if request.limit > 0 {
		query += fmt.Sprintf(" LIMIT %d", request.limit)
	}

	// check for lock
	if request.forUpdate {
		query += " FOR UPDATE"

		// check for skip locked
		if request.skipLocked {
			query += " SKIP LOCKED"
		}
	}

	return query, parameters
}

type genericSelectListResult[T any] struct {
	data []*T
}
