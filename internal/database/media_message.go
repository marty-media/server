/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	log "github.com/sirupsen/logrus"
	"time"
)

type MediaMessage struct {
	ID            int64         `db:"id,autoincrement"`
	MessageID     int64         `db:"message"`
	MediaID       int64         `db:"media_id,requiredForInsert"`
	MediaItemID   sql.NullInt64 `db:"media_item_id"`
	MediaStreamID sql.NullInt64 `db:"media_stream_id"`
	EncodeSetID   sql.NullInt64 `db:"encode_set_id"`
	CreatedAt     time.Time     `db:"created_at,nowForInsert"`
	ModifiedAt    sql.NullTime  `db:"modified_at"`
}

const (
	MediaMessageIDUnknownColorPrimaries = 1
	MediaMessageIDUnknownColorSpace     = 2
	MediaMessageIDUnknownTransferMatrix = 3
)

func (mediaMessage MediaMessage) TableName() string {
	return "media_message"
}

func (transaction *Transaction) CreateMediaMessage(mediaMessage MediaMessage) (int64, error) {
	entries := []DatabaseTable{mediaMessage}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: mediaMessage.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("create new media message failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreateMediaMessage(mediaMessage MediaMessage) (int64, error) {
	return singleInsert(db, func(tx *Transaction) (*genericInsertResult, error) {
		id, err := tx.CreateMediaMessage(mediaMessage)
		return &genericInsertResult{
			id: id,
		}, err
	})
}
