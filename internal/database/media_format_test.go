/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import "testing"

func testMediaFormat(t *testing.T, mediaItemID int64) {
	id := testCreateMediaFormat(t, mediaItemID)
	testUpdateMediaFormat(t, id)
}

func testCreateMediaFormat(t *testing.T, mediaItemID int64) int64 {
	// create new media format
	newMediaFormat := MediaFormat{
		MediaItemID:    mediaItemID,
		Latest:         true,
		FormatName:     "matroska,webm",
		FormatLongName: "Matroska / WebM",
	}
	id, err := Instance.CreateMediaFormat(newMediaFormat)
	if err != nil {
		t.Error("unable to create new media format", err)
	}

	// TODO: validate data by retrieving new media format by ID

	return id
}

func testUpdateMediaFormat(t *testing.T, mediaFormatID int64) {
	if err := Instance.UpdateMediaFormat(UpdateMediaFormatRequest{
		ID:           mediaFormatID,
		Latest:       false,
		UpdateLatest: true,
	}); err != nil {
		t.Error("unable to update new media format", err)
	}

	// TODO: validate update
}
