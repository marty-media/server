/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import "testing"

func testLibrary(t *testing.T) {
	testLibraryValidation(t)
	libraryID := testLibraryCreate(t)
	testLibraryLocation(t, libraryID)
	testLibraryUpdate(t, libraryID)
	testLibraryDelete(t, libraryID)
}

func testLibraryValidation(t *testing.T) {
	// check for missing name
	err := Library{
		Type:     LibraryTypeMovie,
		Language: "eng",
	}.Validate()
	if err == nil {
		t.Error("validation of library name failed")
	}

	// check for invalid name
	err = Library{
		Name:     "Test Test Test Test Test Test Test Test Test", // name too long
		Type:     LibraryTypeMovie,
		Language: "eng",
	}.Validate()
	if err == nil {
		t.Error("validation of library name failed")
	}

	// check library type
	err = Library{
		Name:     "Test",
		Type:     99, // invalid library type
		Language: "eng",
	}.Validate()
	if err == nil {
		t.Error("validation of library type failed")
	}

	// check for invalid language
	err = Library{
		Name:     "Test",
		Type:     LibraryTypeMovie,
		Language: "xyz", // invalid language code
	}.Validate()
	if err == nil {
		t.Error("validation of library language failed")
	}
}

func testLibraryCreate(t *testing.T) int64 {
	// use valid data for library
	newLibrary := Library{
		Name:     "Movies",
		Type:     LibraryTypeMovie,
		Language: "deu",
	}

	// validate the data
	if err := newLibrary.Validate(); err != nil {
		t.Error("validation of library failed", err)
	}

	// create new library
	id, err := Instance.CreateLibrary(newLibrary)
	if err != nil {
		t.Error("create library failed", err)
	}

	// re-load library
	libraries, err := Instance.GetLibraries()
	if err != nil {
		t.Error("unable to load all libraries", err)
	}
	if len(libraries) != 1 {
		t.Error("invalid amount of libraries received")
	}
	if libraries[0].ID != id {
		t.Error("invalid library ID received")
	}

	return id
}

func testLibraryUpdate(t *testing.T, libraryID int64) {
	newName := "New Movies"
	if err := Instance.UpdateLibrary(libraryID, newName, true); err != nil {
		t.Error("update of library failed", err)
	}

	// check new name
	library, err := Instance.GetLibrary(libraryID)
	if err != nil {
		t.Error("unable to load library by ID", err)
	}
	if library.Name != newName {
		t.Error("new library name not correctly stored")
	}
}

func testLibraryDelete(t *testing.T, libraryID int64) {
	if err := Instance.DeleteLibrary(libraryID); err != nil {
		t.Error("unable to delete library", err)
	}
}
