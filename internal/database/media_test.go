/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"gitlab.com/marty-media/server/internal/common"
	"testing"
)

func testMedia(t *testing.T, libraryID int64, libraryLocationID int64) {
	id := testCreateMedia(t, libraryID, libraryLocationID)
	testMediaItem(t, id, libraryLocationID)
	testDeleteMedia(t, id)
}

func testCreateMedia(t *testing.T, libraryID int64, libraryLocationID int64) int64 {
	path := "test"
	uid, _ := common.RandomHex(common.RandomLengthMedia)
	mediaID, err := Instance.CreateMedia(Media{
		LibraryID: libraryID,
		UID:       uid,
		Path:      path,
	})
	if err != nil {
		t.Error("unable to create media", err)
	}

	// load media by ID
	media, err := Instance.GetMedia(mediaID)
	if err != nil {
		t.Error("get media by ID failed", err)
	} else if media.ID != mediaID {
		t.Error("invalid response from get media by ID")
	}

	// find media entries by library
	mediaEntries, err := Instance.FindMediaEntriesByLibrary(libraryID)
	if err != nil {
		t.Error("find media entries by library failed", err)
	} else if len(mediaEntries) != 1 || mediaEntries[0].ID != mediaID {
		t.Error("invalid response from find media entries by library")
	}

	// find media entry
	media, err = Instance.FindMediaByPath(libraryID, path)
	if err != nil {
		t.Error("find media item by location failed", err)
	} else if media.ID != mediaID {
		t.Error("invalid media return by location")
	}

	return mediaID
}

func testDeleteMedia(t *testing.T, mediaID int64) {
	if err := Instance.DeleteMedia(mediaID); err != nil {
		t.Error("unable to delete media", err)
	}
}
