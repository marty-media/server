/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	log "github.com/sirupsen/logrus"
	"time"
)

type MediaStreamTag struct {
	ID          int64          `db:"id,autoincrement"`
	MediaStream int64          `db:"media_stream_id,requiredForInsert"`
	Key         string         `db:"key,requiredForInsert"`
	Value       sql.NullString `db:"value"`
	CreatedAt   time.Time      `db:"created_at,nowForInsert"`
	ModifiedAt  sql.NullTime   `db:"modified_at"`
}

func (mediaStreamTag MediaStreamTag) TableName() string {
	return "media_stream_tag"
}

func (transaction *Transaction) CreateMediaStreamTag(mediaStreamTag MediaStreamTag) (int64, error) {
	entries := []DatabaseTable{mediaStreamTag}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: mediaStreamTag.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("create new media stream tag failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreateMediaStreamTag(mediaStreamTag MediaStreamTag) (int64, error) {
	return singleInsert(db, func(tx *Transaction) (*genericInsertResult, error) {
		id, err := tx.CreateMediaStreamTag(mediaStreamTag)
		return &genericInsertResult{
			id: id,
		}, err
	})
}

func (transaction *Transaction) FindMediaStreamTagsByMediaStream(mediaStreamID int64) ([]*MediaStreamTag, error) {
	return selectRows[MediaStreamTag](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "media_stream_id",
			operator:   filterOperatorEqual,
			value:      mediaStreamID,
		}},
	})
}

func (db *Database) FindMediaStreamTagsByMediaStream(mediaStreamID int64) ([]*MediaStreamTag, error) {
	result, err := singleExecute(db, func(tx *Transaction) (*genericSelectListResult[MediaStreamTag], error) {
		streamTags, err := tx.FindMediaStreamTagsByMediaStream(mediaStreamID)
		return &genericSelectListResult[MediaStreamTag]{
			data: streamTags,
		}, err
	})
	return result.data, err
}
