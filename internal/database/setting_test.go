/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import "testing"

func testSettings(t *testing.T) {
	testSettingDebugLog(t)
}

func testSettingDebugLog(t *testing.T) {
	// load debug log setting
	enabledDebugLog, err := Instance.GetSettingDebugLog()
	if err != nil {
		t.Error("retrieve debug log setting failed", err)
	}
	if enabledDebugLog != nil {
		t.Error("received invalid log level")
	}

	// TODO: update log level

	// TODO: check value again
}
