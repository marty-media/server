/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import log "github.com/sirupsen/logrus"

func (transaction *Transaction) deleteRows(request deleteRowsRequest) error {
	query, fields := transaction.db.instance.BuildDeleteQuery(request)

	// execute delete
	_, err := transaction.tx.Exec(query, fields...)
	if err != nil {
		log.WithFields(log.Fields{
			"sql":     query,
			"inherit": err,
		}).Error("statement execution failed")
		return err
	}

	return nil
}

type deleteRowsRequest struct {
	tableName string
	filter    []filterRow
}
