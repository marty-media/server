/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	log "github.com/sirupsen/logrus"
	"time"
)

type MediaFormat struct {
	ID             int64           `db:"id,autoincrement"`
	MediaItemID    int64           `db:"media_item_id,requiredForInsert"`
	Latest         bool            `db:"latest"`
	FormatName     string          `db:"format_name"`
	FormatLongName string          `db:"format_long_name"`
	StartTime      sql.NullFloat64 `db:"start_time"`
	Duration       sql.NullFloat64 `db:"duration"`
	CreatedAt      time.Time       `db:"created_at,nowForInsert"`
	ModifiedAt     sql.NullTime    `db:"modified_at"`
}

func (mediaFormat MediaFormat) TableName() string {
	return "media_format"
}

func (transaction *Transaction) CreateMediaFormat(mediaFormat MediaFormat) (int64, error) {
	entries := []DatabaseTable{mediaFormat}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: mediaFormat.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("create new media format failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreateMediaFormat(mediaFormat MediaFormat) (int64, error) {
	return singleInsert(db, func(tx *Transaction) (*genericInsertResult, error) {
		id, err := tx.CreateMediaFormat(mediaFormat)
		return &genericInsertResult{
			id: id,
		}, err
	})
}

func (transaction *Transaction) GetMediaFormat(id int64) (*MediaFormat, error) {
	// load rows from database
	return selectRow[MediaFormat](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) GetMediaFormat(id int64) (*MediaFormat, error) {
	return singleExecute(db, func(tx *Transaction) (*MediaFormat, error) {
		return tx.GetMediaFormat(id)
	})
}

func (transaction *Transaction) FindMediaFormatsByMediaItem(mediaItemID int64) ([]*MediaFormat, error) {
	return selectRows[MediaFormat](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "media_item_id",
			operator:   filterOperatorEqual,
			value:      mediaItemID,
		}},
	})
}

func (db *Database) FindMediaFormatsByMediaItem(mediaItemID int64) ([]*MediaFormat, error) {
	result, err := singleExecute(db, func(tx *Transaction) (*genericSelectListResult[MediaFormat], error) {
		rows, err := tx.FindMediaFormatsByMediaItem(mediaItemID)
		return &genericSelectListResult[MediaFormat]{
			data: rows,
		}, err
	})
	return result.data, err
}

type UpdateMediaFormatRequest struct {
	ID           int64
	Latest       bool
	UpdateLatest bool
}

func (transaction *Transaction) UpdateMediaFormat(request UpdateMediaFormatRequest) error {
	// build update request
	updateRequest := updateRowsRequest{
		tableName: MediaFormat{}.TableName(),
		fields: []updateField{{
			columnName:        "modified_at",
			nowTimestampValue: true,
		}},
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      request.ID,
		}},
	}
	if request.UpdateLatest {
		updateRequest.fields = append(updateRequest.fields, updateField{
			columnName: "latest",
			value:      request.Latest,
		})
	}

	// execute update
	err := transaction.updateRows(updateRequest)
	if err != nil {
		log.WithField("inherit", err).Error("update of media format failed")
		return err
	}

	return nil
}

func (db *Database) UpdateMediaFormat(request UpdateMediaFormatRequest) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.UpdateMediaFormat(request)
	})
	return err
}
