/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"testing"
)

func testEncodeSet(t *testing.T, mediaFormatID int64, mediaStreamID int64, mediaID int64, encodeLocationID int64) {
	encodeSetID := testEncodeSetCreate(t, mediaFormatID, mediaStreamID, mediaID, encodeLocationID)
	testEncodeSegment(t, encodeSetID)
	testEncodeSetDelete(t, encodeSetID)
}

func testEncodeSetCreate(t *testing.T, mediaFormatID int64, mediaStreamID int64, mediaID int64, encodeLocationID int64) int64 {
	// create new encode set
	newEncodeSet := EncodeSet{
		MediaFormatID:           mediaFormatID,
		MediaStreamID:           mediaStreamID,
		MediaID:                 mediaID,
		StoredLibraryLocationID: encodeLocationID,
		MediaType:               EncodeSetMediaTypeMovie,
		Name:                    "avc_10",
		Version:                 1,
		Path:                    "some/path",
		FullPath:                "some/path/full",
		Codec:                   "avc",
		FullCodec:               "avc1.4d401e",
		Bandwidth:               6_200_000,
		AverageBandwidth:        6_000_000,
		TargetDuration: sql.NullInt64{
			Int64: 6,
		},
		IndependentSegments: true,
	}
	id, err := Instance.CreateEncodeSet(newEncodeSet)

	if err != nil {
		t.Error("create encode set failed", err)
	}

	// re-load encode set
	encodeSet, err := Instance.GetEncodeSet(id)
	if err != nil {
		t.Error("get encode set failed", err)
	}
	if encodeSet.ID != id {
		t.Error("get encode set failed (ID check)", encodeSet.ID)
	}

	// test find methods
	testEncodeSetFindByMediaID(t, id, encodeSet.MediaID)

	return id
}

func testEncodeSetFindByMediaID(t *testing.T, id int64, mediaID int64) {
	encodeSets, err := Instance.FindEncodeSetsByMediaID(mediaID)
	if err != nil {
		t.Error("find encode sets failed", err)
	}
	if len(encodeSets) != 1 {
		t.Error("find encode sets failed (invalid amount)", len(encodeSets))
	}
	if encodeSets[0].ID != id {
		t.Error("find encode sets failed (invalid ID)", encodeSets[0].ID)
	}
}

func testEncodeSetDelete(t *testing.T, id int64) {
	if err := Instance.DeleteEncodeSet(id); err != nil {
		t.Error("delete encode set failed", err)
	}
}
