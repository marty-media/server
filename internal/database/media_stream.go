/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

type MediaStream struct {
	ID                  int64           `db:"id,autoincrement"`
	MediaItemID         int64           `db:"media_item_id,requiredForInsert"`
	Latest              bool            `db:"latest"`
	Index               int64           `db:"index"` // no requiredForInsert flag, because 0 is a valid index
	CodecName           string          `db:"codec_name,requiredForInsert"`
	CodecLongName       sql.NullString  `db:"codec_long_name"`
	Profile             sql.NullString  `db:"profile"`
	CodecType           string          `db:"codec_type,requiredForInsert"`
	CodecTagString      sql.NullString  `db:"codec_tag_string"`
	CodecTag            sql.NullString  `db:"codec_tag"`
	Width               sql.NullInt64   `db:"width"`
	Height              sql.NullInt64   `db:"height"`
	CodedWidth          sql.NullInt64   `db:"coded_width"`
	CodedHeight         sql.NullInt64   `db:"coded_height"`
	ClosedCaptions      sql.NullBool    `db:"closed_captions"`
	FilmGrain           sql.NullBool    `db:"film_grain"`
	HasBFrames          sql.NullInt64   `db:"has_b_frames"`
	SampleAspectRatio   sql.NullString  `db:"sample_aspect_ratio"`
	DisplayAspectRatio  sql.NullString  `db:"display_aspect_ratio"`
	PixelFormat         sql.NullString  `db:"pixel_format"`
	Level               sql.NullInt64   `db:"level"`
	ColorRange          sql.NullString  `db:"color_range"`
	ColorSpace          sql.NullString  `db:"color_space"`
	ColorTransferMatrix sql.NullString  `db:"color_transfer_matrix"`
	ColorPrimaries      sql.NullString  `db:"color_primaries"`
	ChromaLocation      sql.NullString  `db:"chroma_location"`
	FieldOrder          sql.NullString  `db:"field_order"`
	Refs                sql.NullInt64   `db:"refs"`
	SampleFormat        sql.NullString  `db:"sample_format"`
	SampleRate          sql.NullInt64   `db:"sample_rate"`
	Channels            sql.NullInt64   `db:"channels"`
	ChannelLayout       sql.NullString  `db:"channel_layout"`
	BitsPerSample       sql.NullInt64   `db:"bits_per_sample"`
	InitialPadding      sql.NullInt64   `db:"initial_padding"`
	StreamID            sql.NullString  `db:"stream_id"`
	RealFrameRate       sql.NullString  `db:"real_frame_rate"`
	AverageFrameRate    sql.NullString  `db:"average_frame_rate"`
	TimeBase            sql.NullString  `db:"time_base"`
	StartPTS            sql.NullInt64   `db:"start_pts"`
	StartTime           sql.NullFloat64 `db:"start_time"`
	DurationTS          sql.NullInt64   `db:"duration_ts"`
	Duration            sql.NullFloat64 `db:"duration"`
	BitRate             sql.NullInt64   `db:"bit_rate"`
	MaxBitRate          sql.NullInt64   `db:"max_bit_rate"`
	BitsPerRawSample    sql.NullInt64   `db:"bits_per_raw_sample"`
	CreatedAt           time.Time       `db:"created_at,nowForInsert"`
	ModifiedAt          sql.NullTime    `db:"modified_at"`
}

const (
	MediaStreamCodecNameVideoH264   = "h264"
	MediaStreamCodecNameAudioAAC    = "aac"
	MediaStreamCodecNameAudioAC3    = "ac3"
	MediaStreamCodecNameAudioTrueHD = "truehd"
)

const (
	MediaStreamProfileAACLC = "LC"
)

const (
	MediaStreamCodecTypeVideo    = "video"
	MediaStreamCodecTypeAudio    = "audio"
	MediaStreamCodecTypeSubtitle = "subtitle"
)

func (mediaStream MediaStream) TableName() string {
	return "media_stream"
}

func (transaction *Transaction) CreateMediaStream(mediaStream MediaStream) (int64, error) {
	entries := []DatabaseTable{mediaStream}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: mediaStream.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("create new media stream failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreateMediaStream(mediaStream MediaStream) (int64, error) {
	return singleInsert(db, func(tx *Transaction) (*genericInsertResult, error) {
		id, err := tx.CreateMediaStream(mediaStream)
		return &genericInsertResult{
			id: id,
		}, err
	})
}

func (transaction *Transaction) GetLMediaStream(id int64) (*MediaStream, error) {
	// load rows from database
	return selectRow[MediaStream](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) GetMediaStream(id int64) (*MediaStream, error) {
	return singleExecute(db, func(tx *Transaction) (*MediaStream, error) {
		return tx.GetLMediaStream(id)
	})
}

func (transaction *Transaction) FindMediaStreamsByMediaItem(mediaItemID int64) ([]*MediaStream, error) {
	return selectRows[MediaStream](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "media_item_id",
			operator:   filterOperatorEqual,
			value:      mediaItemID,
		}},
	})
}

func (db *Database) FindMediaStreamsByMediaItem(mediaItemID int64) ([]*MediaStream, error) {
	result, err := singleExecute(db, func(tx *Transaction) (*genericSelectListResult[MediaStream], error) {
		streams, err := tx.FindMediaStreamsByMediaItem(mediaItemID)
		return &genericSelectListResult[MediaStream]{
			data: streams,
		}, err
	})
	return result.data, err
}

// TODO: create update request struct
func (transaction *Transaction) UpdateMediaStream(id int64, latest bool, updateLatest bool) error {
	// build update request
	request := updateRowsRequest{
		tableName: MediaStream{}.TableName(),
		fields: []updateField{{
			columnName:        "modified_at",
			nowTimestampValue: true,
		}},
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	}
	if updateLatest {
		request.fields = append(request.fields, updateField{
			columnName: "latest",
			value:      latest,
		})
	}

	// execute update
	err := transaction.updateRows(request)
	if err != nil {
		log.WithField("inherit", err).Error("update of media stream failed")
		return err
	}

	return nil
}

func (db *Database) UpdateMediaStream(mediaItemID int64, latest bool, updateLatest bool) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.UpdateMediaStream(mediaItemID, latest, updateLatest)
	})
	return err
}

func (transaction *Transaction) DeleteMediaStream(id int64) error {
	return transaction.deleteRows(deleteRowsRequest{
		tableName: MediaStream{}.TableName(),
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) DeleteMediaStream(id int64) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.DeleteMediaStream(id)
	})
	return err
}

func (mediaStream *MediaStream) parseFPS(frameRate string) (float64, bool) {
	// try to parse as float
	fps, err := strconv.ParseFloat(frameRate, 64)
	if err == nil {
		return fps, true
	}

	// try to parse format 24000/1001
	parts := strings.Split(frameRate, "/")
	if len(parts) == 2 {
		a, err := strconv.ParseInt(parts[0], 10, 64)
		if err == nil {
			b, err := strconv.ParseInt(parts[1], 10, 64)
			if err == nil {
				return float64(a) / float64(b), true
			}
		}
	}

	return 0, false
}

func (mediaStream *MediaStream) GetRealFrameRate() (float64, bool) {
	if mediaStream.RealFrameRate.Valid {
		return mediaStream.parseFPS(mediaStream.RealFrameRate.String)
	}
	return 0, false
}
