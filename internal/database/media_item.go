/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"time"

	log "github.com/sirupsen/logrus"
)

type MediaItemAnalyzedStatus int64
type MediaItemEncodedStatus int64

const (
	MediaItemAnalyzedStatusInitial   MediaItemAnalyzedStatus = 0
	MediaItemAnalyzedStatusStarted   MediaItemAnalyzedStatus = 1
	MediaItemAnalyzedStatusCompleted MediaItemAnalyzedStatus = 2
	MediaItemAnalyzedStatusFailed    MediaItemAnalyzedStatus = 3
	MediaItemEncodedStatusInitial    MediaItemEncodedStatus  = 0
	MediaItemEncodedStatusStarted    MediaItemEncodedStatus  = 1
	MediaItemEncodedStatusCompleted  MediaItemEncodedStatus  = 2
	MediaItemEncodedStatusFailed     MediaItemEncodedStatus  = 3
	MediaItemEncodedStatusRetry      MediaItemEncodedStatus  = 4
)

type MediaItem struct {
	ID                int64                   `db:"id,autoincrement"`
	MediaID           int64                   `db:"media_id,requiredForInsert"`
	LibraryLocationID int64                   `db:"library_location_id,requiredForInsert"`
	File              string                  `db:"file,requiredForInsert"`
	Latest            bool                    `db:"latest"`
	Size              int64                   `db:"size,requiredForInsert"`
	ChecksumSha256    sql.NullString          `db:"checksum_sha256"`
	AnalyzedStatus    MediaItemAnalyzedStatus `db:"analyzed_status"`
	AnalyzerVersion   sql.NullInt64           `db:"analyzer_version"`
	LastAnalyzedAt    sql.NullTime            `db:"last_analyzed_at"`
	EncodedStatus     MediaItemEncodedStatus  `db:"encoded_status"`
	EncoderVersion    sql.NullInt64           `db:"encoder_version"`
	LastEncodedAt     sql.NullTime            `db:"last_encoded_at"`
	CreatedAt         time.Time               `db:"created_at,nowForInsert"`
	ModifiedAt        sql.NullTime            `db:"modified_at"`
}

func (mediaItem MediaItem) TableName() string {
	return "media_item"
}

func (transaction *Transaction) CreateMediaItem(mediaItem MediaItem) (int64, error) {
	entries := []DatabaseTable{mediaItem}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: mediaItem.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("create new media item failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreateMediaItem(mediaItem MediaItem) (int64, error) {
	return singleInsert(db, func(tx *Transaction) (*genericInsertResult, error) {
		id, err := tx.CreateMediaItem(mediaItem)
		return &genericInsertResult{
			id: id,
		}, err
	})
}

func (transaction *Transaction) FindMediaItemsByMedia(mediaID int64) ([]*MediaItem, error) {
	return selectRows[MediaItem](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "media_id",
			operator:   filterOperatorEqual,
			value:      mediaID,
		}},
	})
}

func (db *Database) FindMediaItemsByMedia(mediaID int64) ([]*MediaItem, error) {
	data, err := singleExecute(db, func(tx *Transaction) (*genericSelectListResult[MediaItem], error) {
		data, err := tx.FindMediaItemsByMedia(mediaID)
		return &genericSelectListResult[MediaItem]{
			data: data,
		}, err
	})
	return data.data, err
}

func (transaction *Transaction) FindLatestMediaItemByFile(mediaID int64, file string) (*MediaItem, error) {
	return selectRow[MediaItem](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "latest",
			operator:   filterOperatorEqual,
			value:      true,
		}, {
			columnName: "media_id",
			operator:   filterOperatorEqual,
			value:      mediaID,
		}, {
			columnName: "file",
			operator:   filterOperatorEqual,
			value:      file,
		}},
	})
}

func (db *Database) FindLatestMediaItemByFile(mediaID int64, file string) (*MediaItem, error) {
	return singleExecute(db, func(tx *Transaction) (*MediaItem, error) {
		return tx.FindLatestMediaItemByFile(mediaID, file)
	})
}

func (transaction *Transaction) FindMediaItemToAnalyze(analyzerVersion int64) (*MediaItem, error) {
	// configurations:
	// 1. find un-analyzed entries
	// 2. find failed entries
	// 3. find outdated entries (only, if encoding is currently not running)
	selectConfigurations := []selectRowsRequest{{
		filter: []filterRow{{
			columnName: "latest",
			operator:   filterOperatorEqual,
			value:      true,
		}, {
			columnName: "analyzed_status",
			operator:   filterOperatorEqual,
			value:      MediaItemAnalyzedStatusInitial,
		}},
		limit:      1,
		forUpdate:  true,
		skipLocked: true,
	}, {
		filter: []filterRow{{
			columnName: "latest",
			operator:   filterOperatorEqual,
			value:      true,
		}, {
			columnName: "analyzed_status",
			operator:   filterOperatorEqual,
			value:      MediaItemAnalyzedStatusFailed,
		}, {
			columnName: "last_analyzed_at",
			operator:   filterOperatorLT,
			value:      time.Now().Add(-1 * time.Hour),
		}},
		limit:      1,
		forUpdate:  true,
		skipLocked: true,
	}, {
		filter: []filterRow{{
			columnName: "latest",
			operator:   filterOperatorEqual,
			value:      true,
		}, {
			columnName: "analyzer_version",
			operator:   filterOperatorLT,
			value:      analyzerVersion,
		}, {
			columnName: "encoded_status",
			operator:   filterOperatorNotEqual,
			value:      MediaItemEncodedStatusStarted,
		}},
		limit:      1,
		forUpdate:  true,
		skipLocked: true,
	}}

	for _, config := range selectConfigurations {
		// execute query
		row, err := selectRow[MediaItem](transaction, config)

		// check for errors
		if err != nil {
			return nil, err
		}

		// check for result
		if row != nil {
			return row, nil
		}
	}

	// no row found
	return nil, nil
}

func (transaction *Transaction) FindMediaItemToEncode(analyzerVersion int64, encoderVersion int64) (*MediaItem, error) {
	// 1. find not encoded entries
	// 2. check for retry entries
	// 3. find outdated encoded entries
	selectConfigurations := []selectRowsRequest{{
		filter: []filterRow{{
			columnName: "analyzed_status",
			operator:   filterOperatorEqual,
			value:      MediaItemAnalyzedStatusCompleted,
		}, {
			columnName: "analyzer_version",
			operator:   filterOperatorEqual,
			value:      analyzerVersion,
		}, {
			columnName: "encoded_status",
			operator:   filterOperatorEqual,
			value:      MediaItemEncodedStatusInitial,
		}},
		limit:      1,
		forUpdate:  true,
		skipLocked: true,
	}, {
		filter: []filterRow{{
			columnName: "analyzed_status",
			operator:   filterOperatorEqual,
			value:      MediaItemAnalyzedStatusCompleted,
		}, {
			columnName: "analyzer_version",
			operator:   filterOperatorEqual,
			value:      analyzerVersion,
		}, {
			columnName: "encoded_status",
			operator:   filterOperatorEqual,
			value:      MediaItemEncodedStatusRetry,
		}, {
			columnName: "last_encoded_at",
			operator:   filterOperatorLT,
			value:      time.Now().Add(-1 * time.Hour),
		}},
		limit:      1,
		forUpdate:  true,
		skipLocked: true,
	}, {
		filter: []filterRow{{
			columnName: "analyzed_status",
			operator:   filterOperatorEqual,
			value:      MediaItemAnalyzedStatusCompleted,
		}, {
			columnName: "analyzer_version",
			operator:   filterOperatorEqual,
			value:      analyzerVersion,
		}, {
			columnName: "encoder_version",
			operator:   filterOperatorLT,
			value:      encoderVersion,
		}, {
			columnName: "encoded_status",
			operator:   filterOperatorNotEqual,
			value:      MediaItemEncodedStatusStarted,
		}},
		limit:      1,
		forUpdate:  true,
		skipLocked: true,
	}}

	for _, config := range selectConfigurations {
		row, err := selectRow[MediaItem](transaction, config)
		if err != nil {
			return nil, err
		}
		if row != nil {
			return row, nil
		}
	}

	// no entries for encoding found
	return nil, nil
}

type UpdateMediaItemRequest struct {
	ID                    int64
	Latest                bool
	UpdateLatest          bool
	ChecksumSha256        string
	UpdateChecksumSha256  bool
	AnalyzedStatus        MediaItemAnalyzedStatus
	UpdateAnalyzedStatus  bool
	AnalyzerVersion       int64
	UpdateAnalyzerVersion bool
	UpdateLastAnalyzedAt  bool
	EncodedStatus         MediaItemEncodedStatus
	UpdateEncodedStatus   bool
	EncoderVersion        int64
	UpdateEncoderVersion  bool
	UpdateLastEncodedAt   bool
}

func (transaction *Transaction) UpdateMediaItem(request UpdateMediaItemRequest) error {
	// build update request
	updateRequest := updateRowsRequest{
		tableName: MediaItem{}.TableName(),
		fields: []updateField{{
			columnName:        "modified_at",
			nowTimestampValue: true,
		}},
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      request.ID,
		}},
	}
	if request.UpdateLatest {
		updateRequest.fields = append(updateRequest.fields, updateField{
			columnName: "latest",
			value:      request.Latest,
		})
	}
	if request.UpdateChecksumSha256 {
		updateRequest.fields = append(updateRequest.fields, updateField{
			columnName: "checksum_sha256",
			value:      request.ChecksumSha256,
		})
	}
	if request.UpdateAnalyzedStatus {
		updateRequest.fields = append(updateRequest.fields, updateField{
			columnName: "analyzed_status",
			value:      request.AnalyzedStatus,
		})
	}
	if request.UpdateAnalyzerVersion {
		updateRequest.fields = append(updateRequest.fields, updateField{
			columnName: "analyzer_version",
			value:      request.AnalyzerVersion,
		})
	}
	if request.UpdateLastAnalyzedAt {
		updateRequest.fields = append(updateRequest.fields, updateField{
			columnName:        "last_analyzed_at",
			nowTimestampValue: true,
		})
	}
	if request.UpdateEncodedStatus {
		updateRequest.fields = append(updateRequest.fields, updateField{
			columnName: "encoded_status",
			value:      request.EncodedStatus,
		})
	}
	if request.UpdateEncoderVersion {
		updateRequest.fields = append(updateRequest.fields, updateField{
			columnName: "encoder_version",
			value:      request.EncoderVersion,
		})
	}
	if request.UpdateLastEncodedAt {
		updateRequest.fields = append(updateRequest.fields, updateField{
			columnName:        "last_encoded_at",
			nowTimestampValue: true,
		})
	}

	// execute update
	err := transaction.updateRows(updateRequest)
	if err != nil {
		log.WithField("inherit", err).Error("update of media item failed")
		return err
	}

	return nil
}

func (db *Database) UpdateMediaItem(request UpdateMediaItemRequest) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.UpdateMediaItem(request)
	})
	return err
}

func (transaction *Transaction) DeleteMediaItem(id int64) error {
	return transaction.deleteRows(deleteRowsRequest{
		tableName: MediaItem{}.TableName(),
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) DeleteMediaItem(id int64) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.DeleteMediaItem(id)
	})
	return err
}
