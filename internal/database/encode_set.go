/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	log "github.com/sirupsen/logrus"
	"time"
)

type EncodeSetMediaType int64

const (
	EncodeSetMediaTypeMovie EncodeSetMediaType = 1
	EncodeSetMediaTypeAudio EncodeSetMediaType = 2
)

type EncodeSetFrameRate int64

const (
	EncodeSetFrameRateMovie     EncodeSetFrameRate = 1 // 24fps
	EncodeSetFrameRatePAL       EncodeSetFrameRate = 2 // 25fps
	EncodeSetFrameRateMovieNTSC EncodeSetFrameRate = 3 // 24000/1001
)

type EncodeSetVideoRange int64

const (
	EncodeSetVideoRangeSDR EncodeSetVideoRange = 1
	EncodeSetVideoRangeHLG EncodeSetVideoRange = 2
	EncodeSetVideoRangePQ  EncodeSetVideoRange = 3
)

type EncodeSet struct {
	ID                      int64              `db:"id,autoincrement"`
	MediaFormatID           int64              `db:"media_format_id,requiredForInsert"`
	MediaStreamID           int64              `db:"media_stream_id,requiredForInsert"`
	MediaID                 int64              `db:"media_id,requiredForInsert"`
	StoredLibraryLocationID int64              `db:"stored_library_location_id,requiredForInsert"`
	MediaType               EncodeSetMediaType `db:"media_type,requiredForInsert"`
	Name                    string             `db:"name,requiredForInsert"`
	Version                 int64              `db:"version,requiredForInsert"`
	Path                    string             `db:"path,requiredForInsert"`
	FullPath                string             `db:"full_path,requiredForInsert"`
	Codec                   string             `db:"codec,requiredForInsert"`
	FullCodec               string             `db:"full_codec,requiredForInsert"`
	Bandwidth               int64              `db:"bandwidth,requiredForInsert"`
	AverageBandwidth        int64              `db:"average_bandwidth,requiredForInsert"`
	TargetDuration          sql.NullInt64      `db:"target_duration"`
	IndependentSegments     bool               `db:"independent_segments"`
	FrameRate               sql.NullInt64      `db:"frame_rate"`
	VideoRange              sql.NullInt64      `db:"video_range"`
	Width                   sql.NullInt64      `db:"width"`
	Height                  sql.NullInt64      `db:"height"`
	Channels                sql.NullInt64      `db:"channels"`
	Downmix                 sql.NullBool       `db:"downmix"`
	Downscale               sql.NullBool       `db:"downscale"`
	Language                sql.NullString     `db:"language"` // ISO639_3
	CreatedAt               time.Time          `db:"created_at,nowForInsert"`
	ModifiedAt              sql.NullTime       `db:"modified_at"`
}

func (encodeSet EncodeSet) TableName() string {
	return "encode_set"
}

func (transaction *Transaction) CreateEncodeSet(encodeSet EncodeSet) (int64, error) {
	entries := []DatabaseTable{encodeSet}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: encodeSet.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("encode set creation failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreateEncodeSet(encodeSet EncodeSet) (int64, error) {
	return singleInsert(db, func(tx *Transaction) (*genericInsertResult, error) {
		response, err := tx.CreateEncodeSet(encodeSet)
		return &genericInsertResult{
			id: response,
		}, err
	})
}

func (transaction *Transaction) GetEncodeSet(id int64) (*EncodeSet, error) {
	// load rows from database
	return selectRow[EncodeSet](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) GetEncodeSet(id int64) (*EncodeSet, error) {
	return singleExecute(db, func(tx *Transaction) (*EncodeSet, error) {
		return tx.GetEncodeSet(id)
	})
}

func (transaction *Transaction) FindEncodeSetsByMediaID(mediaID int64) ([]*EncodeSet, error) {
	return selectRows[EncodeSet](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "media_id",
			operator:   filterOperatorEqual,
			value:      mediaID,
		}},
	})
}

func (db *Database) FindEncodeSetsByMediaID(mediaID int64) ([]*EncodeSet, error) {
	result, err := singleExecute(db, func(tx *Transaction) (*genericSelectListResult[EncodeSet], error) {
		result, err := tx.FindEncodeSetsByMediaID(mediaID)
		return &genericSelectListResult[EncodeSet]{
			data: result,
		}, err
	})
	var response []*EncodeSet
	if result != nil {
		response = result.data
	}
	return response, err
}

func (transaction *Transaction) FindEncodeSetsByMediaStream(mediaStreamID int64) ([]*EncodeSet, error) {
	return selectRows[EncodeSet](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "media_stream_id",
			operator:   filterOperatorEqual,
			value:      mediaStreamID,
		}},
	})
}

func (db *Database) FindEncodeSetsByMediaStream(mediaStreamID int64) ([]*EncodeSet, error) {
	result, err := singleExecute(db, func(tx *Transaction) (*genericSelectListResult[EncodeSet], error) {
		result, err := tx.FindEncodeSetsByMediaStream(mediaStreamID)
		return &genericSelectListResult[EncodeSet]{
			data: result,
		}, err
	})
	var response []*EncodeSet
	if result != nil {
		response = result.data
	}
	return response, err
}

func (transaction *Transaction) DeleteEncodeSet(id int64) error {
	return transaction.deleteRows(deleteRowsRequest{
		tableName: EncodeSet{}.TableName(),
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) DeleteEncodeSet(id int64) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.DeleteEncodeSet(id)
	})
	return err
}
