/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import "testing"

func testEncodeSegment(t *testing.T, encodeSetID int64) {
	id := testEncodeSegmentCreate(t, encodeSetID)
	testEncodeSegmentDelete(t, id)
}

func testEncodeSegmentCreate(t *testing.T, encodeSetID int64) int64 {
	// create new encode segment
	newSegment := EncodeSegment{
		EncodeSetID:    encodeSetID,
		Sequence:       1,
		Name:           "test",
		Size:           999,
		ChecksumSha256: "abc",
	}
	id, err := Instance.CreateEncodeSegment(newSegment)
	if err != nil {
		t.Error("create encode segment failed", err)
	}

	// re-load segment
	segment, err := Instance.GetEncodeSegment(id)
	if err != nil {
		t.Error("get encode segment failed", err)
	}
	if segment.ID != id {
		t.Error("get encode segment failed (invalid ID)", segment.ID)
	}

	// find segment by set ID
	segments, err := Instance.FindEncodeSegmentsByEncodeSetID(encodeSetID)
	if err != nil {
		t.Error("find encode segments failed", err)
	}
	if len(segments) != 1 {
		t.Error("find encode segments failed (invalid amount)", len(segments))
	}

	return id
}

func testEncodeSegmentDelete(t *testing.T, encodeSegmentID int64) {
	err := Instance.DeleteEncodeSegment(encodeSegmentID)
	if err != nil {
		t.Error("delete encode segment failed", err)
	}
}
