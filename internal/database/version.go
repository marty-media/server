/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"time"

	log "github.com/sirupsen/logrus"
)

type Version struct {
	ID        int64          `db:"id,autoincrement"`
	Version   int64          `db:"version,requiredForInsert"`
	Comment   sql.NullString `db:"comment"`
	CreatedAt time.Time      `db:"created_at,nowForInsert"`
}

func (version Version) TableName() string {
	return "version"
}

func (transaction *Transaction) CreateVersion(version Version) (int64, error) {
	entries := []DatabaseTable{version}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: version.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("database insert failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreateVersion(version Version) (int64, error) {
	result, err := singleExecute(db, func(tx *Transaction) (*genericInsertResult, error) {
		id, err := tx.CreateVersion(version)
		return &genericInsertResult{
			id: id,
		}, err
	})
	return result.id, err
}
