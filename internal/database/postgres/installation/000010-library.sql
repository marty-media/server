CREATE TABLE "mms"."library" (
    "id" BIGSERIAL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "type" SMALLINT NOT NULL,
    "language" TEXT NOT NULL,
    "created_at" TIMESTAMP NOT NULL,
    "modified_at" TIMESTAMP
)