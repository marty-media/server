CREATE TABLE "mms"."media_message" (
    "id" BIGSERIAL PRIMARY KEY,
    "message" INTEGER NOT NULL,
    "media_id" BIGINT NOT NULL REFERENCES "mms"."media" ON DELETE CASCADE,
    "media_item_id" BIGINT REFERENCES "mms"."media_item" ON DELETE CASCADE,
    "media_stream_id" BIGINT REFERENCES "mms"."media_stream" ON DELETE CASCADE,
    "encode_set_id" BIGINT REFERENCES "mms"."encode_set" ON DELETE CASCADE,
    "created_at" TIMESTAMP NOT NULL,
    "modified_at" TIMESTAMP
)