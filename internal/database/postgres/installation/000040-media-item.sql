CREATE TABLE "mms"."media_item" (
    "id" BIGSERIAL PRIMARY KEY,
    "media_id" BIGINT REFERENCES "mms"."media",
    "library_location_id" BIGINT REFERENCES "mms"."library_location",
    "file" TEXT NOT NULL,
    "latest" BOOLEAN NOT NULL,
    "size" BIGINT NOT NULL,
    "checksum_sha256" TEXT,
    "analyzed_status" INTEGER NOT NULL DEFAULT 0,
    "analyzer_version" INTEGER,
    "last_analyzed_at" TIMESTAMP,
    "encoded_status" INTEGER NOT NULL DEFAULT 0,
    "encoder_version" INTEGER,
    "last_encoded_at" TIMESTAMP,
    "created_at" TIMESTAMP NOT NULL,
    "modified_at" TIMESTAMP
)