CREATE TABLE "mms"."version" (
    "id" BIGSERIAL PRIMARY KEY,
    "version" INTEGER NOT NULL,
    "comment" TEXT,
    "created_at" TIMESTAMP NOT NULL
)