CREATE TABLE "mms"."library_location" (
    "id" BIGSERIAL PRIMARY KEY,
    "library_id" BIGINT REFERENCES "mms"."library" ON DELETE CASCADE,
    "type" SMALLINT NOT NULL,
    "usage_type" SMALLINT NOT NULL,
    "location" TEXT NOT NULL,
    "user" TEXT,
    "password" TEXT,
    "region" TEXT,
    "scanner_version" INTEGER,
    "last_scanned_at" TIMESTAMP,
    "created_at" TIMESTAMP NOT NULL,
    "modified_at" TIMESTAMP
)