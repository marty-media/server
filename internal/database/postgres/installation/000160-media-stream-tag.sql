CREATE TABLE "mms"."media_stream_tag" (
    "id" BIGSERIAL PRIMARY KEY,
    "media_stream_id" BIGINT REFERENCES "mms"."media_stream" ON DELETE CASCADE,
    "key" TEXT NOT NULL,
    "value" TEXT,
    "created_at" TIMESTAMP NOT NULL,
    "modified_at" TIMESTAMP
)