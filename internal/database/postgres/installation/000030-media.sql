CREATE TABLE "mms"."media" (
    "id" BIGSERIAL PRIMARY KEY,
    "library_id" BIGINT REFERENCES "mms"."library",
    "uid" TEXT NOT NULL UNIQUE,
    "path" TEXT NOT NULL,
    "created_at" TIMESTAMP NOT NULL,
    "modified_at" TIMESTAMP
)