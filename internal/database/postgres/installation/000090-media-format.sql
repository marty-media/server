CREATE TABLE "mms"."media_format" (
    "id" BIGSERIAL PRIMARY KEY,
    "media_item_id" BIGINT REFERENCES "mms"."media_item" ON DELETE CASCADE,
    "latest" BOOLEAN NOT NULL,
    "format_name" TEXT,
    "format_long_name" TEXT,
    "start_time" DOUBLE PRECISION,
    "duration" DOUBLE PRECISION,
    "created_at" TIMESTAMP NOT NULL,
    "modified_at" TIMESTAMP
)