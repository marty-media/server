CREATE TABLE "mms"."playback" (
    "id" BIGSERIAL PRIMARY KEY,
    "uid" TEXT NOT NULL UNIQUE,
    "media_id" BIGINT REFERENCES "mms"."media",
    "created_at" TIMESTAMP NOT NULL,
    "modified_at" TIMESTAMP
)