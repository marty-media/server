CREATE TABLE "mms"."encode_segment" (
    "id" BIGSERIAL PRIMARY KEY,
    "encode_set_id" BIGINT REFERENCES "mms"."encode_set" ON DELETE CASCADE,
    "sequence" INTEGER NOT NULL,
    "name" TEXT NOT NULL,
    "size" BIGINT NOT NULL,
    "checksum_sha256" TEXT NOT NULL,
    "duration" DOUBLE PRECISION,
    "map" BOOLEAN NOT NULL,
    "created_at" TIMESTAMP NOT NULL,
    "modified_at" TIMESTAMP
)