ALTER TABLE "mms"."media_item"
ALTER COLUMN "analyzed_status" SET DEFAULT 0,
ADD COLUMN "encoded_status" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN "encoder_version" INTEGER,
ADD COLUMN "last_encoded_at" TIMESTAMP
