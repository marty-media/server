/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"time"
)

const (
	settingKeyDebugLog = "debug_log"
)

type Setting struct {
	ID         int64        `db:"id,autoincrement"`
	Key        string       `db:"key,requiredForInsert"`
	Value      string       `db:"value,requiredForInsert"`
	CreatedAt  time.Time    `db:"created_at,nowForInsert"`
	ModifiedAt sql.NullTime `db:"modified_at"`
}

func (setting Setting) TableName() string {
	return "setting"
}

func (transaction *Transaction) getSettings(key string) ([]*Setting, error) {
	return selectRows[Setting](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "key",
			operator:   filterOperatorEqual,
			value:      key,
		}},
	})
}

func (transaction *Transaction) getSetting(key string) (*Setting, error) {
	data, err := transaction.getSettings(key)
	if err != nil {
		return nil, err
	}

	if len(data) == 0 {
		return nil, nil
	}

	return data[0], nil
}

func (transaction *Transaction) GetSettingDebugLog() (*Setting, error) {
	return transaction.getSetting(settingKeyDebugLog)
}

func (db *Database) GetSettingDebugLog() (*Setting, error) {
	return singleExecute(db, func(tx *Transaction) (*Setting, error) {
		return tx.GetSettingDebugLog()
	})
}
