/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"time"

	log "github.com/sirupsen/logrus"
)

type Media struct {
	ID         int64        `db:"id,autoincrement"`
	LibraryID  int64        `db:"library_id,requiredForInsert"`
	UID        string       `db:"uid,requiredForInsert"`
	Path       string       `db:"path,requiredForInsert"`
	CreatedAt  time.Time    `db:"created_at,nowForInsert"`
	ModifiedAt sql.NullTime `db:"modified_at"`
}

func (media Media) TableName() string {
	return "media"
}

func (transaction *Transaction) CreateMedia(media Media) (int64, error) {
	entries := []DatabaseTable{media}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: media.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("create new media failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreateMedia(media Media) (int64, error) {
	return singleInsert(db, func(tx *Transaction) (*genericInsertResult, error) {
		id, err := tx.CreateMedia(media)
		return &genericInsertResult{
			id: id,
		}, err
	})
}

func (transaction *Transaction) GetMedia(id int64) (*Media, error) {
	return selectRow[Media](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) GetMedia(id int64) (*Media, error) {
	return singleExecute(db, func(tx *Transaction) (*Media, error) {
		return tx.GetMedia(id)
	})
}

func (transaction *Transaction) FindMediaEntriesByLibrary(libraryID int64) ([]*Media, error) {
	return selectRows[Media](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "library_id",
			operator:   filterOperatorEqual,
			value:      libraryID,
		}},
	})
}

func (db *Database) FindMediaEntriesByLibrary(libraryID int64) ([]*Media, error) {
	result, err := singleExecute(db, func(tx *Transaction) (*genericSelectListResult[Media], error) {
		data, err := tx.FindMediaEntriesByLibrary(libraryID)
		return &genericSelectListResult[Media]{
			data: data,
		}, err
	})
	return result.data, err
}

func (transaction *Transaction) FindMediaByPath(libraryID int64, path string) (*Media, error) {
	return selectRow[Media](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "library_id",
			operator:   filterOperatorEqual,
			value:      libraryID,
		}, {
			columnName: "path",
			operator:   filterOperatorEqual,
			value:      path,
		}},
	})
}

func (db *Database) FindMediaByPath(libraryID int64, path string) (*Media, error) {
	return singleExecute(db, func(tx *Transaction) (*Media, error) {
		return tx.FindMediaByPath(libraryID, path)
	})
}

func (transaction *Transaction) DeleteMedia(id int64) error {
	return transaction.deleteRows(deleteRowsRequest{
		tableName: Media{}.TableName(),
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) DeleteMedia(id int64) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.DeleteMedia(id)
	})
	return err
}
