/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"os"
	"testing"
)

func TestDatabase(t *testing.T) {
	if err := InitDatabase(); err != nil {
		t.Error("initialization of database failed", err)
	}

	// clean database
	if err := Instance.Reset(); err != nil {
		t.Error("unable to clear database", err)
	}

	// create tables
	if err := Instance.Upgrade(); err != nil {
		t.Error("unable to upgrade database schema", err)
	}

	// run tests
	testLibrary(t)
	testSettings(t)
}

func TestInvalidDatabase(t *testing.T) {
	// store environment
	originalDBType := os.Getenv("MMS_DATABASE_TYPE")
	defer os.Setenv("MMS_DATABASE_TYPE", originalDBType)

	// test invalid DB type
	os.Setenv("MMS_DATABASE_TYPE", "asdf")
	if err := InitDatabase(); err == nil {
		t.Error("missing error for invalid database type")
	}
}
