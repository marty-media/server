/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	log "github.com/sirupsen/logrus"
	"time"
)

type Playback struct {
	ID         int64        `db:"id,autoincrement"`
	UID        string       `db:"uid,requiredForInsert"`
	MediaID    int64        `db:"media_id,requiredForInsert"`
	CreatedAt  time.Time    `db:"created_at,nowForInsert"`
	ModifiedAt sql.NullTime `db:"modified_at"`
}

func (playback Playback) TableName() string {
	return "playback"
}

func (transaction *Transaction) CreatePlayback(playback Playback) (int64, error) {
	entries := []DatabaseTable{playback}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: playback.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("create new playback failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreatePlayback(playback Playback) (int64, error) {
	return singleInsert(db, func(tx *Transaction) (*genericInsertResult, error) {
		id, err := tx.CreatePlayback(playback)
		return &genericInsertResult{
			id: id,
		}, err
	})
}

func (transaction *Transaction) FindPlaybackByUID(uid string) (*Playback, error) {
	return selectRow[Playback](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "uid",
			operator:   filterOperatorEqual,
			value:      uid,
		}},
	})
}

func (db *Database) FindPlaybackByUID(uid string) (*Playback, error) {
	return singleExecute(db, func(tx *Transaction) (*Playback, error) {
		return tx.FindPlaybackByUID(uid)
	})
}
