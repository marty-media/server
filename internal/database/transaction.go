/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"fmt"
	"strconv"
)

type Transaction struct {
	db          *Database
	tx          *sql.Tx
	isCommitted bool
}

func (transaction *Transaction) Commit() error {
	err := transaction.tx.Commit()
	if err == nil {
		transaction.isCommitted = true
	}
	return err
}

func (transaction *Transaction) Rollback() error {
	// transaction is already committed
	if transaction.isCommitted {
		return nil
	}
	return transaction.tx.Rollback()
}

type filterRow struct {
	columnName string
	value      any
	operator   filterOperator
}

type filterOperator int

const (
	filterOperatorEqual    filterOperator = 1
	filterOperatorGT       filterOperator = 2
	filterOperatorLT       filterOperator = 3
	filterOperatorNotEqual filterOperator = 4
)

func singleExecute[Resp any](db *Database, fn func(tx *Transaction) (*Resp, error)) (*Resp, error) {
	// create transaction
	tx, err := db.Transaction()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	// execute command
	response, err := fn(tx)
	if err != nil {
		return nil, err
	}

	// commit changes
	if err := tx.Commit(); err != nil {
		return nil, err
	}
	return response, nil
}

func buildWhereClause(filter []filterRow, parameterOffset int) (string, []any) {
	query := ""
	parameters := []any{}
	if len(filter) > 0 {
		for i, filter := range filter {
			if i == 0 {
				query += " WHERE "
			} else {
				query += " AND "
			}
			query += "\"" + filter.columnName + "\" "
			switch filter.operator {
			case filterOperatorEqual:
				query += "="
			case filterOperatorGT:
				query += ">"
			case filterOperatorLT:
				query += "<"
			case filterOperatorNotEqual:
				query += "!="
			default:
				panic(fmt.Sprintf("invalid filter operator %d", filter.operator))
			}
			query += " $" + strconv.Itoa(len(parameters)+1+parameterOffset)
			parameters = append(parameters, filter.value)
		}
	}

	return query, parameters
}
