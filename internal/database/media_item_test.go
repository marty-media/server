/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"testing"
)

func testMediaItem(t *testing.T, mediaID int64, libraryLocationID int64) {
	testFindMediaItems(t)
	id := testCreateMediaItem(t, mediaID, libraryLocationID)
	testMediaFormat(t, id)
	testMediaStream(t, id, mediaID, libraryLocationID)
	testUpdateMediaItem(t, id)
	testDeleteMediaItem(t, id)
}

func testFindMediaItems(t *testing.T) {
	// check media items to analyze (no entries should be found)
	trx, err := Instance.Transaction()
	if err != nil {
		t.Error("unable to open new transaction", err)
	}
	mediaItem, err := trx.FindMediaItemToAnalyze(0)
	if err != nil {
		t.Error("unable to find media item to analyze", err)
	}
	if mediaItem != nil {
		t.Error("media item to analyze found, but none expected")
	}
}

func testCreateMediaItem(t *testing.T, mediaID int64, libraryLocationID int64) int64 {
	fileName := "test1"
	id, err := Instance.CreateMediaItem(MediaItem{
		MediaID:           mediaID,
		LibraryLocationID: libraryLocationID,
		File:              fileName,
		Latest:            true,
		Size:              123,
		ChecksumSha256:    sql.NullString{String: "asdf", Valid: true},
	})
	if err != nil {
		t.Error("unable to create new media item", err)
	}

	// find by media
	mediaItems, err := Instance.FindMediaItemsByMedia(mediaID)
	if err != nil {
		t.Error("unable to find media items by media ID")
	} else if len(mediaItems) != 1 || mediaItems[0].ID != id {
		t.Error("invalid response from loading media items by mediaID")
	}

	// find by file name
	mediaItem, err := Instance.FindLatestMediaItemByFile(mediaID, fileName)
	if err != nil {
		t.Error(err)
	} else if mediaItem.ID != id {
		t.Error("invalid media item found by file name")
	}

	// try to create (inavlid) media item
	if _, err := Instance.CreateMediaItem(MediaItem{}); err == nil {
		t.Error("expected error for createing media item with missing fields")
	}

	return id
}

func testUpdateMediaItem(t *testing.T, mediaItemID int64) {
	err := Instance.UpdateMediaItem(UpdateMediaItemRequest{
		ID:                    mediaItemID,
		Latest:                true,
		UpdateLatest:          true,
		ChecksumSha256:        "asdff",
		UpdateChecksumSha256:  true,
		AnalyzedStatus:        MediaItemAnalyzedStatusCompleted,
		UpdateAnalyzedStatus:  true,
		AnalyzerVersion:       1,
		UpdateAnalyzerVersion: true,
		UpdateLastAnalyzedAt:  true,
	})
	if err != nil {
		t.Error("update of media item failed", err)
	}
}

func testDeleteMediaItem(t *testing.T, mediaItemID int64) {
	if err := Instance.DeleteMediaItem(mediaItemID); err != nil {
		t.Error("unable to delete media item", err)
	}
}
