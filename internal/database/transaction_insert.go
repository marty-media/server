/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
)

func (transaction *Transaction) insertRows(request insertRowsRequest) ([]int64, error) {
	// check values for insert
	if err := transaction.insertRowsChecks(request); err != nil {
		log.WithField("inherit", err).Error("checks before insert execution failed")
		return nil, err
	}

	// prepare insert statement
	sql := transaction.db.instance.BuildInsertQuery(request)
	statement, err := transaction.tx.Prepare(sql)
	if err != nil {
		log.WithFields(log.Fields{
			"query":   sql,
			"inherit": err,
		}).Error("failed to prepare new statement")
		return nil, err
	}

	// execute inserts
	columns, _ := request.getInsertColumns()
	ids := []int64{}
	for _, entry := range request.entries {
		// build parameters
		entryValue := reflect.ValueOf(entry)
		var values []interface{}
		for _, column := range columns {
			if column.tag.hasAttribute(tagAttributeNowForInsert) {
				// this is automatically filled by database
				continue
			}
			value := entryValue.FieldByName(column.structField.Name)
			values = append(values, value.Interface())
		}

		// execute statement
		row := statement.QueryRow(values...)
		if row.Err() != nil {
			log.WithFields(log.Fields{
				"sql":     sql,
				"inherit": row.Err(),
			}).Error("statement execution failed")
			return nil, row.Err()
		}

		// get new ID
		var newID int64
		if err := row.Scan(&newID); err != nil {
			log.WithFields(log.Fields{
				"sql":     sql,
				"inherit": err,
			}).Error("unable to retrieve last insert ID")
			return nil, err
		}
		ids = append(ids, newID)
	}

	// return IDs
	return ids, nil
}

func (transaction *Transaction) insertRowsChecks(request insertRowsRequest) error {
	// check required fields
	columns := request.getColumns()
	for _, entry := range request.entries {
		entryValue := reflect.ValueOf(entry)
		for _, column := range columns {
			if !column.tag.hasAttribute(tagAttributeRequiredForInsert) {
				// check only required fields
				continue
			}

			// check, if field has value
			field := entryValue.FieldByName(column.structField.Name)
			if field.IsZero() {
				return fmt.Errorf("missing value for field " + column.structField.Name)
			}
		}
	}

	return nil
}

type insertRowsRequest struct {
	tableName string
	entries   []DatabaseTable
}

func (request insertRowsRequest) getColumns() []insertColumn {
	t := reflect.ValueOf(request.entries[0]).Type()

	// parse tags of fields of struct
	columns := []insertColumn{}
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		// parse tag
		tagString, _ := field.Tag.Lookup(tagName)
		if tagString == "" {
			continue
		}
		tag := tagByString(tagString)

		// store tag
		columns = append(columns, insertColumn{
			structField: field,
			tag:         tag,
		})
	}

	return columns
}

func (request insertRowsRequest) getInsertColumns() (insertColumns []insertColumn, autoIncrementColumn insertColumn) {
	columns := request.getColumns()

	for _, column := range columns {
		if column.tag.hasAttribute("autoincrement") {
			autoIncrementColumn = column
			continue
		}

		insertColumns = append(insertColumns, column)
	}

	return
}

func singleInsert(db *Database, fn func(tx *Transaction) (*genericInsertResult, error)) (int64, error) {
	response, err := singleExecute(db, fn)
	var id int64 = -1
	if response != nil {
		id = response.id
	}
	return id, err
}

type insertColumn struct {
	structField reflect.StructField
	tag         tag
}

type genericInsertResult struct {
	id int64
}
