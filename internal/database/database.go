/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"embed"
	"encoding/json"
	"fmt"
	"gitlab.com/marty-media/server/internal/common"
	"io/fs"
	"os"
	"path"
	"reflect"
	"sort"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/marty-media/server/internal/settings"
)

type Database struct {
	instance DatabaseInstance
}

type DatabaseInstance interface {
	Open() error
	GetDB() *sql.DB
	Upgrade() error
	BuildInsertQuery(insertRowsRequest) string
	BuildUpdateQuery(updateRowsRequest) (string, []any)
	BuildDeleteQuery(deleteRowsRequest) (string, []any)
}

type DatabaseTable interface {
	TableName() string
	// ValidateInsert() error
	// TODO: generic insert validation method
}

var Instance *Database

//go:embed postgres/*/*.sql postgres/*/*.go.json
var scriptsFS embed.FS

func InitDatabase() error {
	// create new database instance object
	var db DatabaseInstance
	databaseType := settings.Instance.GetDatabaseType()
	switch databaseType {
	case "postgres":
		address := settings.Instance.GetDatabasePostgresAddress()
		port := settings.Instance.GetDatabasePostgresPort()
		user := settings.Instance.GetDatabasePostgresUser()
		password := settings.Instance.GetDatabasePostgresPassword()
		database := settings.Instance.GetDatabasePostgresDatabase()
		maxOpenConnections := settings.Instance.GetDatabasePostgresMaxOpenConnections()
		db = NewPostgres(address, port, user, password, database, maxOpenConnections)
	default:
		log.WithField("database type", databaseType).Error("unknown database type")
		return fmt.Errorf("invalid database type %s", databaseType)
	}

	// open new database connection pool
	if err := db.Open(); err != nil {
		log.WithField("inherit", err).Error("unable to open database connection")
		return err
	}

	// create single database instance
	Instance = &Database{
		instance: db,
	}
	return nil
}

// Reset is only used for internal testing
func (db *Database) Reset() error {
	_, err := db.instance.GetDB().Exec("DROP SCHEMA IF EXISTS \"mms\" CASCADE")
	return err
}

func (db *Database) Upgrade() error {
	return db.instance.Upgrade()
}

func (db *Database) Transaction() (*Transaction, error) {
	tx, err := db.instance.GetDB().Begin()
	if err != nil {
		log.WithField("inherit", err).Error("create new database transaction failed")
		return nil, err
	}

	return &Transaction{
		db: db,
		tx: tx,
	}, nil
}

func filesForUpgrade(databaseType string, subFolder string, currentVersion int) ([]fs.DirEntry, error) {
	entries, err := scriptsFS.ReadDir(path.Join(databaseType, subFolder))
	if err != nil {
		if os.IsNotExist(err) {
			log.WithField("type", subFolder).Warning("no upgrade scripts available")
			// the folder doesn't exist, if no files exists
			return nil, nil
		}

		log.Fatal(err)
		return nil, err
	}

	// filter (sql files only; new files only)
	var files []fs.DirEntry
	for _, entry := range entries {
		if entry.IsDir() {
			// there should be no directories
			continue
		}

		version := versionByName(entry.Name())
		if version <= currentVersion {
			// ignore older versions and current version; skip also initial version creation
			continue
		}

		files = append(files, entry)
	}

	// sort files by name
	sort.Slice(files, func(i, j int) bool {
		version1 := versionByName(files[i].Name())
		version2 := versionByName(files[j].Name())
		return version1 < version2
	})

	return files, nil
}

func executeUpgrade(db DatabaseInstance, databaseType string, subFolder string, entries []fs.DirEntry, transaction *sql.Tx) (int, error) {
	lastVersion := -1
	for _, entry := range entries {
		// read file
		data, err := scriptsFS.ReadFile(path.Join(databaseType, subFolder, entry.Name()))
		if err != nil {
			log.WithFields(log.Fields{
				"sql":     entry.Name(),
				"inherit": err,
			}).Error("unable to read file")
			return lastVersion, err
		}

		if strings.HasSuffix(entry.Name(), ".sql") {
			// execute sql
			_, err = transaction.Exec(string(data))
			if err != nil {
				log.WithFields(log.Fields{
					"sql":     entry.Name(),
					"query":   string(data),
					"inherit": err,
				}).Error("unable to execute script")
				return lastVersion, err
			}
		} else if strings.HasSuffix(entry.Name(), ".go.json") {
			// execute upgrade logic
			err = executeUpgradeLogic(db, databaseType, entry, data, transaction)
			if err != nil {
				log.WithFields(log.Fields{
					"sql":     entry.Name(),
					"query":   string(data),
					"inherit": err,
				}).Error("unable to execute script")
				return lastVersion, err
			}
		} else {
			return lastVersion, fmt.Errorf("unsupported file type: %s", entry.Name())
		}

		// update version
		lastVersion = versionByName(entry.Name())
		log.WithFields(log.Fields{
			"version": lastVersion,
			"sql":     entry.Name(),
		}).Info("upgrade script executed")
	}

	return lastVersion, nil
}

type upgradeInfo struct {
	Version int
	Name    string
}

func executeUpgradeLogic(db DatabaseInstance, databaseType string, entry fs.DirEntry, data []byte, transaction *sql.Tx) error {
	// parse upgrade information
	var info upgradeInfo
	if err := json.Unmarshal(data, &info); err != nil {
		return err
	}

	// execute method
	upgrade := &upgrade{
		db:          db,
		transaction: transaction,
	}
	method := reflect.ValueOf(upgrade).MethodByName(info.Name)
	result := method.Call([]reflect.Value{})
	if ret := result[0].Interface(); ret != nil {
		return ret.(error)
	}
	return nil
}

func versionByName(fileName string) int {
	versionString := strings.Split(fileName, "-")[0]
	version, err := strconv.Atoi(versionString)
	if err != nil {
		log.WithFields(log.Fields{
			"sql":     fileName,
			"inherit": err,
		}).Fatal("unable to parse version from file name")
	}
	return version
}

type upgrade struct {
	db          DatabaseInstance
	transaction *sql.Tx
}

func (upgrade *upgrade) Upgrade15Media() error {
	// retrieve media entries without UID
	rows, err := upgrade.transaction.Query("SELECT id FROM mms.media WHERE uid IS NULL FOR UPDATE")
	if err != nil {
		log.WithFields(log.Fields{
			"inherit": err,
		}).Error("unable to find media for upgrade")
		return err
	}

	// collect IDs
	var ids []int64
	for rows.Next() {
		var id int64
		if err := rows.Scan(&id); err != nil {
			return err
		}
		ids = append(ids, id)
	}

	// update media entries
	for _, id := range ids {
		// generate new UID
		uid, err := common.RandomHex(common.RandomLengthMedia)
		if err != nil {
			log.WithFields(log.Fields{
				"inherit": err,
			}).Error("unable to generate uid for upgrade")
			return err
		}

		// update entry with new UID
		updateSql, params := upgrade.db.BuildUpdateQuery(updateRowsRequest{
			tableName: "media",
			filter: []filterRow{
				{columnName: "id", operator: filterOperatorEqual, value: id},
			},
			fields: []updateField{
				{columnName: "uid", value: uid},
			},
		})

		if _, err := upgrade.transaction.Exec(updateSql, params...); err != nil {
			log.WithFields(log.Fields{
				"inherit": err,
			}).Error("unable to update uid for media")
			return err
		}
	}

	return nil
}
