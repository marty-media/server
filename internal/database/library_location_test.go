/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import "testing"

func testLibraryLocation(t *testing.T, libraryID int64) {
	id := testLibraryLocationCreate(t, libraryID)
	testMedia(t, libraryID, id)
	testLibraryLocationUpdate(t, id)
	testLibraryLocationInvalid(t, libraryID)
	testLibraryLocationDelete(t, id)
}

func testLibraryLocationCreate(t *testing.T, libraryID int64) int64 {
	libraryLocation := LibraryLocation{
		LibraryID: libraryID,
		Type:      LibraryLocationTypeLocalFolder,
		UsageType: LibraryLocationUsageTypeSourceMedia,
		Location:  "folder",
	}

	// validate new location
	if err := libraryLocation.Validate(); err != nil {
		t.Error("validation of library location failed")
	}

	// create new library
	id, err := Instance.CreateLibraryLocation(libraryLocation)
	if err != nil {
		t.Error("library location creation failed", err)
	}

	// check locations
	locations, err := Instance.GetLibraryLocations(libraryID)
	if err != nil {
		t.Error("unable to load library locations", err)
	}
	if len(locations) != 1 {
		t.Error("retrieved invalid amount of locations")
	}

	// check location
	location, err := Instance.GetLibraryLocation(id)
	if err != nil {
		t.Error("loading library location failed", err)
	}
	if location.ID != id {
		t.Error("invalid library location retrieved")
	}

	return id
}

func testLibraryLocationUpdate(t *testing.T, id int64) {
	if err := Instance.UpdateLibraryLocation(id, 1, true, true); err != nil {
		t.Error("library location update failed", err)
	}
}

func testLibraryLocationInvalid(t *testing.T, libraryID int64) {
	// invalid location type
	invalidType := LibraryLocation{
		LibraryID: libraryID,
		Type:      12345,
		Location:  "/some/path",
	}
	if err := invalidType.Validate(); err == nil {
		t.Error("missing error for invalid location type")
	}

	// missing S3 credentials
	missingS3credentials := LibraryLocation{
		LibraryID: libraryID,
		Type:      LibraryLocationTypeS3,
		Location:  "/some/path",
	}
	if err := missingS3credentials.Validate(); err == nil {
		t.Error("missing error for missing s3 credentials")
	}
}

func testLibraryLocationDelete(t *testing.T, id int64) {
	if err := Instance.DeleteLibraryLocation(id); err != nil {
		t.Error("deletion of library location failed")
	}
}
