/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"fmt"
	"slices"
	"time"

	log "github.com/sirupsen/logrus"
)

type LibraryLocationType int64

const (
	LibraryLocationTypeLocalFolder LibraryLocationType = 1
	LibraryLocationTypeS3          LibraryLocationType = 2
)

var (
	allLibraryLocationTypes = []LibraryLocationType{
		LibraryLocationTypeLocalFolder,
		LibraryLocationTypeS3,
	}
)

type LibraryLocationUsageType int64

const (
	LibraryLocationUsageTypeSourceMedia  LibraryLocationUsageType = 1
	LibraryLocationUsageTypeEncodedMedia LibraryLocationUsageType = 2
)

var (
	allLibraryLocationUsageTypes = []LibraryLocationUsageType{
		LibraryLocationUsageTypeSourceMedia,
		LibraryLocationUsageTypeEncodedMedia,
	}
)

type LibraryLocation struct {
	ID             int64                    `db:"id,autoincrement"`
	LibraryID      int64                    `db:"library_id,requiredForInsert"`
	Type           LibraryLocationType      `db:"type,requiredForInsert"`
	UsageType      LibraryLocationUsageType `db:"usage_type,requiredForInsert"`
	Location       string                   `db:"location,requiredForInsert"`
	User           sql.NullString           `db:"user"`
	Password       sql.NullString           `db:"password"`
	Region         sql.NullString           `db:"region"`
	ScannerVersion int64                    `db:"scanner_version"`
	LastScannedAt  sql.NullTime             `db:"last_scanned_at"`
	CreatedAt      time.Time                `db:"created_at,nowForInsert"`
	ModifiedAt     sql.NullTime             `db:"modified_at"`
}

func (location LibraryLocation) TableName() string {
	return "library_location"
}

func (location LibraryLocation) Validate() error {
	// validate type
	if !slices.Contains(allLibraryLocationTypes, location.Type) {
		return fmt.Errorf("invalid location type")
	}

	// validate usage type
	if !slices.Contains(allLibraryLocationUsageTypes, location.UsageType) {
		return fmt.Errorf("invalid usage type")
	}

	// validate location string
	if location.Location == "" {
		return fmt.Errorf("missing location")
	} else if len(location.Location) > 255 {
		return fmt.Errorf("location too long")
	}

	// additional validate by type
	switch location.Type {
	case LibraryLocationTypeLocalFolder:
		if location.User.Valid || location.Password.Valid || location.Region.Valid {
			return fmt.Errorf("user/password/region only valid for S3")
		}
	case LibraryLocationTypeS3:
		if !location.User.Valid || location.User.String == "" || !location.Password.Valid || location.Password.String == "" {
			return fmt.Errorf("missing user/password")
		}
	}

	return nil
}

func (transaction *Transaction) GetLibraryLocations(libraryID int64) ([]*LibraryLocation, error) {
	return selectRows[LibraryLocation](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "library_id",
			operator:   filterOperatorEqual,
			value:      libraryID,
		}},
	})
}

func (db *Database) GetLibraryLocations(libraryID int64) ([]*LibraryLocation, error) {
	result, err := singleExecute(db, func(tx *Transaction) (*genericSelectListResult[LibraryLocation], error) {
		result, err := tx.GetLibraryLocations(libraryID)
		return &genericSelectListResult[LibraryLocation]{
			data: result,
		}, err
	})
	var response []*LibraryLocation
	if result != nil {
		response = result.data
	}
	return response, err
}

func (transaction *Transaction) CreateLibraryLocation(location LibraryLocation) (int64, error) {
	entries := []DatabaseTable{location}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: location.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("library location creation failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreateLibraryLocation(location LibraryLocation) (int64, error) {
	return singleInsert(db, func(tx *Transaction) (*genericInsertResult, error) {
		response, err := tx.CreateLibraryLocation(location)
		return &genericInsertResult{
			id: response,
		}, err
	})
}

func (transaction *Transaction) GetLibraryLocation(locationID int64) (*LibraryLocation, error) {
	return selectRow[LibraryLocation](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      locationID,
		}},
	})
}

func (db *Database) GetLibraryLocation(locationID int64) (*LibraryLocation, error) {
	return singleExecute(db, func(tx *Transaction) (*LibraryLocation, error) {
		return tx.GetLibraryLocation(locationID)
	})
}

// TODO: create update request struct
func (transaction *Transaction) UpdateLibraryLocation(id int64, scannerVersion int64, updateScannerVersion bool, updateLastScannedAt bool) error {
	// build request
	request := updateRowsRequest{
		tableName: LibraryLocation{}.TableName(),
		fields: []updateField{{
			columnName:        "modified_at",
			nowTimestampValue: true,
		}},
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	}
	if updateScannerVersion {
		request.fields = append(request.fields, updateField{
			columnName: "scanner_version",
			value:      scannerVersion,
		})
	}
	if updateLastScannedAt {
		request.fields = append(request.fields, updateField{
			columnName:        "last_scanned_at",
			nowTimestampValue: true,
		})
	}

	// execute update
	err := transaction.updateRows(request)
	if err != nil {
		log.WithField("inherit", err).Error("update of library location failed")
		return err
	}

	return nil
}

func (db *Database) UpdateLibraryLocation(id int64, scannerVersion int64, updateScannerVersion bool, updateLastScannedAt bool) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.UpdateLibraryLocation(id, scannerVersion, updateScannerVersion, updateLastScannedAt)
	})
	return err
}

func (transaction *Transaction) DeleteLibraryLocation(id int64) error {
	return transaction.deleteRows(deleteRowsRequest{
		tableName: LibraryLocation{}.TableName(),
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) DeleteLibraryLocation(id int64) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.DeleteLibraryLocation(id)
	})
	return err
}
