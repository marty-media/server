/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"testing"
)

func testMediaStream(t *testing.T, mediaItemID int64, mediaID int64, encodeLocationID int64) {
	id := testCreateMediaStream(t, mediaItemID)
	formatID := testCreateMediaFormat(t, mediaItemID)
	testEncodeSet(t, formatID, id, mediaID, encodeLocationID)
	testUpdateMediaStream(t, id)
	// TODO: delete media format
	testDeleteMediaStream(t, id)
}

func testCreateMediaStream(t *testing.T, mediaItemID int64) int64 {
	// create new media stream
	newMediaStream := MediaStream{
		MediaItemID:   mediaItemID,
		Latest:        true,
		Index:         0,
		CodecName:     "test",
		CodecType:     "test",
		RealFrameRate: sql.NullString{String: "24000/1001", Valid: true},
	}
	id, err := Instance.CreateMediaStream(newMediaStream)
	if err != nil {
		t.Error("unable to create new media stream")
	}

	// find by media item
	mediaStreams, err := Instance.FindMediaStreamsByMediaItem(mediaItemID)
	if err != nil {
		t.Error("unable to find media streams by media item", err)
	} else if len(mediaStreams) != 1 || mediaStreams[0].ID != id {
		t.Error("invalid response from find media streams by media item")
	}

	// try to create new (invalid) media stream
	if _, err := Instance.CreateMediaStream(MediaStream{}); err == nil {
		t.Error("expected error for missing required fields")
	}

	// check frame rate
	rate, success := mediaStreams[0].GetRealFrameRate()
	if !success {
		t.Error("unable to get real frame rate", err)
	}
	expectedRate := float64(24000) / float64(1001)
	if rate != expectedRate {
		t.Error("invalid real-frame-rate", rate, expectedRate)
	}

	return id
}

func testUpdateMediaStream(t *testing.T, mediaStreamID int64) {
	err := Instance.UpdateMediaStream(mediaStreamID, true, true)
	if err != nil {
		t.Error("update of media stream failed", err)
	}
}

func testDeleteMediaStream(t *testing.T, id int64) {
	if err := Instance.DeleteMediaStream(id); err != nil {
		t.Error("unable to delete media stream", err)
	}
}
