# Database
## For Developers
### Naming Schema of SQL files
`xxxxxy-name.sql` is the typical file name:
- `xxxxx` is the version number (incremented by 1 for each change)
- `y` typically `0`. This number is only used, if for a single database change needs multiple queries (the amount of queries might be different based on database type).
- `name` is the table name

### Version handling
Every change increases the version number. The latest version is stored into the database table called `version`. There are special rules to follow for each new version:
- create a new file into the upgrade folders for each change
- modify the installation scripts
- rename the version of the `-version.sql` file of the installation folders
