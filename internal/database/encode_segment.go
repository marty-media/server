/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	log "github.com/sirupsen/logrus"
	"time"
)

type EncodeSegment struct {
	ID             int64           `db:"id,autoincrement"`
	EncodeSetID    int64           `db:"encode_set_id,requiredForInsert"`
	Sequence       int64           `db:"sequence"`
	Name           string          `db:"name,requiredForInsert"`
	Size           int64           `db:"size,requiredForInsert"`
	ChecksumSha256 string          `db:"checksum_sha256,requiredForInsert"`
	Duration       sql.NullFloat64 `db:"duration"`
	Map            bool            `db:"map"`
	CreatedAt      time.Time       `db:"created_at,nowForInsert"`
	ModifiedAt     sql.NullTime    `db:"modified_at"`
}

func (encodeSegment EncodeSegment) TableName() string {
	return "encode_segment"
}

func (transaction *Transaction) CreateEncodeSegment(encodeSegment EncodeSegment) (int64, error) {
	entries := []DatabaseTable{encodeSegment}
	ids, err := transaction.insertRows(insertRowsRequest{
		tableName: encodeSegment.TableName(),
		entries:   entries,
	})
	if err != nil {
		log.WithField("inherit", err).Error("encode segment creation failed")
		return -1, err
	}

	// return IDs
	return ids[0], nil
}

func (db *Database) CreateEncodeSegment(encodeSegment EncodeSegment) (int64, error) {
	return singleInsert(db, func(tx *Transaction) (*genericInsertResult, error) {
		response, err := tx.CreateEncodeSegment(encodeSegment)
		return &genericInsertResult{
			id: response,
		}, err
	})
}

func (transaction *Transaction) GetEncodeSegment(id int64) (*EncodeSegment, error) {
	return selectRow[EncodeSegment](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) GetEncodeSegment(id int64) (*EncodeSegment, error) {
	return singleExecute(db, func(tx *Transaction) (*EncodeSegment, error) {
		return tx.GetEncodeSegment(id)
	})
}

func (transaction *Transaction) FindEncodeSegmentsByEncodeSetID(encodeSetID int64) ([]*EncodeSegment, error) {
	return selectRows[EncodeSegment](transaction, selectRowsRequest{
		filter: []filterRow{{
			columnName: "encode_set_id",
			operator:   filterOperatorEqual,
			value:      encodeSetID,
		}},
	})
}

func (db *Database) FindEncodeSegmentsByEncodeSetID(encodeSetID int64) ([]*EncodeSegment, error) {
	result, err := singleExecute(db, func(tx *Transaction) (*genericSelectListResult[EncodeSegment], error) {
		result, err := tx.FindEncodeSegmentsByEncodeSetID(encodeSetID)
		return &genericSelectListResult[EncodeSegment]{
			data: result,
		}, err
	})
	var response []*EncodeSegment
	if result != nil {
		response = result.data
	}
	return response, err
}

func (transaction *Transaction) DeleteEncodeSegment(id int64) error {
	return transaction.deleteRows(deleteRowsRequest{
		tableName: EncodeSegment{}.TableName(),
		filter: []filterRow{{
			columnName: "id",
			operator:   filterOperatorEqual,
			value:      id,
		}},
	})
}

func (db *Database) DeleteEncodeSegment(id int64) error {
	_, err := singleExecute(db, func(tx *Transaction) (*any, error) {
		return nil, tx.DeleteEncodeSegment(id)
	})
	return err
}
