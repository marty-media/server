/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package database

import (
	"database/sql"
	"fmt"
	"strconv"

	_ "github.com/jackc/pgx/v5/stdlib"
	log "github.com/sirupsen/logrus"
)

const (
	postgresScriptsFolder = "postgres"
)

type PostgresDB struct {
	dataSourceName     string
	maxOpenConnections int
	db                 *sql.DB
}

func NewPostgres(address string, port int, user string, password string, database string, maxOpenConnections int) *PostgresDB {
	return &PostgresDB{
		dataSourceName:     fmt.Sprintf("postgres://%s:%s@%s:%d/%s", user, password, address, port, database),
		maxOpenConnections: maxOpenConnections,
	}
}

func (db *PostgresDB) Open() error {
	// open database connection
	sqldb, err := sql.Open("pgx", db.dataSourceName)
	if err != nil {
		log.WithField("inherit", err).Error("unable to open database connection")
		return err
	}
	db.db = sqldb

	// limit connections
	db.db.SetMaxOpenConns(db.maxOpenConnections)

	return nil
}

func (db *PostgresDB) GetDB() *sql.DB {
	return db.db
}

func (db *PostgresDB) Upgrade() error {
	// load current database version
	currentVersion := -1
	scriptsFolder := "upgrade"
	rows, err := db.db.Query("SELECT MAX(\"version\") FROM \"mms\".\"version\"")
	if err != nil {
		log.WithField("inherit", err).Warn("detecting current database version failed; initialize new database")
		scriptsFolder = "installation"
	} else {
		// retrieve version
		for rows.Next() {
			rows.Scan(&currentVersion)
		}
	}
	log.WithField("version", currentVersion).Debug("detected database version")

	// load database scripts
	files, err := filesForUpgrade(postgresScriptsFolder, scriptsFolder, currentVersion)
	if err != nil {
		log.WithField("inherit", err).Error("unable to load database scripts")
		return err
	}

	// check for upgrade files
	if len(files) == 0 {
		log.Info("database is up to date")
		return nil
	}

	// begin new transaction
	transaction, err := db.db.Begin()
	if err != nil {
		log.WithField("inherit", err).Error("unable to begin new transaction")
		return err
	}

	// execute scripts
	log.WithField("files", len(files)).Info("execute database upgrade")
	newVersion, err := executeUpgrade(db, postgresScriptsFolder, scriptsFolder, files, transaction)
	if err != nil {
		log.WithField("inherit", err).Error("database upgrade failed")
		db.rollback(transaction)
		return err
	}

	// update versions database
	// TODO: refactor this into new version structure for more generic handling
	newVersionStatement, err := transaction.Prepare("INSERT INTO \"mms\".\"version\" (\"version\", \"created_at\") VALUES ($1,CURRENT_TIMESTAMP)")
	if err != nil {
		log.WithField("inherit", err).Error("prepare statement for new version insert failed")
		db.rollback(transaction)
		return err
	}
	_, err = newVersionStatement.Exec(newVersion)
	if err != nil {
		log.WithField("inherit", err).Error("insert new version failed")
		db.rollback(transaction)
		return err
	}

	// everything went fine; commit changes
	err = transaction.Commit()
	if err != nil {
		log.WithField("inherit", err).Error("commit of database upgrade failed")
		return err
	}

	return nil
}

func (db *PostgresDB) CurrentTimestampPlaceholder() string {
	return "CURRENT_TIMESTAMP"
}

func (db *PostgresDB) rollback(transaction *sql.Tx) {
	if rollbackErr := transaction.Rollback(); rollbackErr != nil {
		log.WithField("inherit", rollbackErr).Error("rollback failed")
	}
}

func (db *PostgresDB) BuildInsertQuery(request insertRowsRequest) string {
	// load all columns
	columns, autoIncrementColumn := request.getInsertColumns()

	// build column and value string parts
	columnString := ""
	valuesString := ""
	valuesCounter := 0
	for _, column := range columns {
		// build column string
		if columnString != "" {
			columnString += ","
		}
		columnString += "\"" + column.tag.Name + "\""

		// build values string
		if valuesString != "" {
			valuesString += ","
		}
		if column.tag.hasAttribute(tagAttributeNowForInsert) {
			valuesString += db.CurrentTimestampPlaceholder()
		} else {
			valuesString += "$" + strconv.Itoa(valuesCounter+1)
			valuesCounter++
		}
	}

	// build query
	query := "INSERT INTO \"mms\".\"" + request.tableName + "\" "
	query += "(" + columnString + ") VALUES (" + valuesString + ")"
	if autoIncrementColumn.tag.Name != "" {
		query += " RETURNING " + autoIncrementColumn.tag.Name
	}

	return query
}

func (db *PostgresDB) BuildUpdateQuery(request updateRowsRequest) (string, []any) {
	query := "UPDATE \"mms\".\"" + request.tableName + "\""
	var parameters []any

	// set new values
	for i, field := range request.fields {
		if i == 0 {
			query += " SET"
		} else {
			query += ","
		}
		query += " \"" + field.columnName + "\" = "

		if field.nowTimestampValue {
			query += db.CurrentTimestampPlaceholder()
		} else {
			query += "$" + strconv.Itoa(len(parameters)+1)
			parameters = append(parameters, field.value)
		}
	}

	// build where clause
	whereClause, whereClauseParameters := buildWhereClause(request.filter, len(parameters))
	query += whereClause
	parameters = append(parameters, whereClauseParameters...)

	return query, parameters
}

func (db *PostgresDB) BuildDeleteQuery(request deleteRowsRequest) (string, []any) {
	query := "DELETE FROM \"mms\".\"" + request.tableName + "\""

	// build where clause
	whereClause, parameters := buildWhereClause(request.filter, 0)
	query += whereClause

	return query, parameters
}
