/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package ffmpeg

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"gitlab.com/marty-media/server/internal/settings"
)

type FFprobe struct {
	Path string
}

func NewFFprobe(filePath string) *FFprobe {
	return &FFprobe{
		Path: filePath,
	}
}

func (ffprobe *FFprobe) AnalyzeVideo() (*FileInfo, error) {
	ffprobePath := filepath.Join(settings.Instance.GetFFmpegLocation(), "ffprobe")
	cmd := exec.Command(ffprobePath, "-print_format", "json",
		"-show_format", "-show_streams", "-show_chapters", "-hide_banner", "-v", "quiet", ffprobe.Path)
	var stdOut bytes.Buffer
	var stdError bytes.Buffer
	cmd.Stdout = &stdOut
	cmd.Stderr = &stdError
	if err := cmd.Run(); err != nil {
		log.WithFields(log.Fields{
			"filePath":           ffprobe.Path,
			"inherit":            err,
			"ffprobePath":        ffprobePath,
			"ffprobeErrorOutput": stdError.String(),
		}).Error("ffprobe execution failed")
		return nil, err
	}

	// check for errors
	if len(stdError.Bytes()) > 0 {
		log.WithFields(log.Fields{
			"filePath":           ffprobe.Path,
			"ffprobeErrorOutput": stdError.String(),
		}).Error("ffprobe return error text")
		return nil, fmt.Errorf("error during ffprobe execution: %s", stdError.String())
	}

	// ffprobe result
	data := stdOut.Bytes()
	log.WithFields(log.Fields{
		"filePath":           ffprobe.Path,
		"ffprobeOutput":      string(data),
		"ffprobeErrorOutput": stdError.String(),
	}).Debug("ffprobe successfully executed")

	// parse json data
	var info FileInfo
	if err := json.Unmarshal(data, &info); err != nil {
		log.WithFields(log.Fields{
			"filePath":      ffprobe.Path,
			"ffprobeOutput": string(data),
			"inherit":       err,
		}).Error("unmarshalling ffprobe response failed")
		return nil, err
	}

	return &info, nil
}

type FileInfo struct {
	Format   Format    `json:"format"`
	Streams  []Stream  `json:"streams"`
	Chapters []Chapter `json:"chapters"`
}

type Format struct {
	FormatName     string            `json:"format_name"`      // e.g. "mpegts", "mov,mp4,m4a,3gp,3g2,mj2"
	FormatLongName string            `json:"format_long_name"` // e.g. "Matroska / WebM"
	StartTime      string            `json:"start_time"`       // e.g. "0.000000"
	Duration       string            `json:"duration"`         // e.g. "6600.640000"
	Tags           map[string]string `json:"tags"`             // e.g. "encoder": "Lavf59.17.100"
}

type Stream struct {
	Index               int               `json:"index"`
	CodecName           string            `json:"codec_name"`           // e.g. "h264", "ac3", "dvb_subtitle", "epg"
	CodecNameLong       *string           `json:"codec_long_name"`      // e.g. "H.265 / HEVC (High Efficiency Video Coding)"
	Profile             *string           `json:"profile"`              // e.g. "Main 10"
	CodecType           string            `json:"codec_type"`           // e.g. "video", "audio", "data"
	CodecTagString      *string           `json:"codec_tag_string"`     // e.g. "hev1"
	CodecTag            *string           `json:"codec_tag"`            // e.g. "0x31766568"
	Width               *int              `json:"width"`                // e.g. 1280, 3840
	Height              *int              `json:"height"`               // e.g. 720, 2160
	CodedWidth          *int              `json:"coded_width"`          // e.g. 1280, 3840
	CodedHeight         *int              `json:"coded_height"`         // e.g. 720, 2160
	ClosedCaptions      *int              `json:"closed_captions"`      // e.g. 0 or 1
	FilmGrain           *int              `json:"film_grain"`           // e.g. 0 or 1
	HasBFrames          *int              `json:"has_b_frames"`         // e.g. 2
	SampleAspectRatio   *string           `json:"sample_aspect_ratio"`  // e.g. "1:1"
	DisplayAspectRatio  *string           `json:"display_aspect_ratio"` // e.g. "16:9"
	PixelFormat         *string           `json:"pix_fmt"`              // e.g. "yuv420p10le"
	Level               *int              `json:"level"`                // e.g. 153
	ColorRange          *string           `json:"color_range"`          // e.g. "tv"
	ColorSpace          *string           `json:"color_space"`          // e.g. "bt2020nc"
	ColorTransferMatrix *string           `json:"color_transfer"`       // e.g. "smpte2084"
	ColorPrimaries      *string           `json:"color_primaries"`      // e.g. "bt2020"
	ChromaLocation      *string           `json:"chroma_location"`      // e.g. "topleft"
	FieldOrder          *string           `json:"field_order"`          // e.g. "progressive", "tt"
	Refs                *int              `json:"refs"`                 // e.g. 1
	SampleFormat        *string           `json:"sample_fmt"`           // e.g. "fltp"
	SampleRate          *string           `json:"sample_rate"`          // e.g. "48000"
	Channels            *int              `json:"channels"`             // e.g. 6
	ChannelLayout       *string           `json:"channel_layout"`       // e.g. "stereo"
	BitsPerSample       *int              `json:"bits_per_sample"`      // e.g. 0
	InitialPadding      *int              `json:"initial_padding"`      // e.g. 0
	ID                  *string           `json:"id"`                   // e.g. "0x1"
	RealFrameRate       *string           `json:"r_frame_rate"`         // e.g. "25/1"
	AverageFrameRate    *string           `json:"avg_frame_rate"`       // e.g. "25/1"
	TimeBase            *string           `json:"time_base"`            // e.g. "1/12800"
	StartPTS            *int              `json:"start_pts"`            // e.g. 0
	StartTime           *string           `json:"start_time"`           // e.g. "0.000000", "N/A"
	DurationTS          *int              `json:"duration_ts"`          // e.g. 7681024
	Duration            *string           `json:"duration"`             // e.g. "600.080000", "N/A"
	BitRate             *string           `json:"bit_rate"`             // e.g. "3707588", "N/A"
	MaxBitRate          *string           `json:"max_bit_rate"`         // e.g. "3707588", "N/A"
	BitsPerRawSample    *string           `json:"bits_per_raw_sample"`  // e.g. "8", "N/A"
	NBFrames            *string           `json:"nb_frames"`            // e.g. "15002", "N/A"
	NBReadFrames        *string           `json:"nb_read_frames"`       // e.g. "N/A"
	NBReadPackets       *string           `json:"nb_read_packets"`      // e.g. "N/A"
	Disposition         Disposition       `json:"disposition"`          // e.g. "forced": 0
	Tags                map[string]string `json:"tags"`                 // e.g. "language": "deu"
}

type Chapter struct {
	ID        int               `json:"id"`
	StartTime string            `json:"start_time"`
	EndTime   string            `json:"end_time"`
	Tags      map[string]string `json:"tags"`
}

type Disposition struct {
	Default int `json:"default"`
	Forced  int `json:"forced"`
}
