/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package ffmpeg

import "testing"

func TestFFprobe(t *testing.T) {
	// execute analyzation
	probe := NewFFprobe("test/Trailer.mp4")
	fileInfo, err := probe.AnalyzeVideo()
	if err != nil {
		t.Error(err)
	}

	// validate probe data
	if len(fileInfo.Streams) != 2 {
		t.Error("received invalid amount of streams")
	}

	// TODO: validate audio stream (index 0)
	validateVideo(t, fileInfo.Streams[1])
}

func validateVideo(t *testing.T, stream Stream) {
	if stream.Index != 1 {
		t.Error("received invalid stream index")
	}
	if stream.CodecName != "h264" {
		t.Error("received invalid codec name")
	}
	if *stream.CodecNameLong != "H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10" {
		t.Error("received invalid codec long name")
	}
	if *stream.Profile != "High" {
		t.Error("received invalid profile")
	}
	if stream.CodecType != "video" {
		t.Error("received invalid stream type")
	}
	if *stream.CodecTagString != "avc1" {
		t.Error("received invalid codec tag string")
	}
	if *stream.CodecTag != "0x31637661" {
		t.Error("received invalid codec tag")
	}
	if *stream.Width != 1280 {
		t.Error("received invalid width")
	}
	if *stream.Height != 720 {
		t.Error("received invalid height")
	}
	if *stream.CodedWidth != 1280 {
		t.Error("received invalid coded width")
	}
	if *stream.CodedHeight != 720 {
		t.Error("received invalid coded height")
	}
	if *stream.ClosedCaptions != 0 {
		t.Error("received invalid closed captions")
	}
	if *stream.FilmGrain != 0 {
		t.Error("received invalid film grain")
	}
	if *stream.HasBFrames != 0 {
		t.Error("received invalid has b frames")
	}
	if *stream.SampleAspectRatio != "1:1" {
		t.Error("received invalid sample aspect ratio")
	}
	if *stream.DisplayAspectRatio != "16:9" {
		t.Error("received invalid display aspect ratio")
	}
	if *stream.PixelFormat != "yuv420p" {
		t.Error("received invalid pixel format")
	}
	if *stream.Level != 31 {
		t.Error("received invalid level")
	}
	if *stream.ColorRange != "tv" {
		t.Error("received invalid color range")
	}
	if *stream.ColorSpace != "bt709" {
		t.Error("received invalid color space")
	}
	if *stream.ColorTransferMatrix != "bt709" {
		t.Error("received invalid color transfer")
	}
	if *stream.ColorPrimaries != "bt709" {
		t.Error("received invalid color primaries")
	}
	if *stream.ChromaLocation != "left" {
		t.Error("received invalid chroma location")
	}
	if *stream.FieldOrder != "progressive" {
		t.Error("received invalid field order")
	}
	if *stream.Refs != 1 {
		t.Error("received invalid refs")
	}

	// video has no sample format, sample rate, channels, channel layout, bits per sample, initial padding

	// TODO: is_avc, nal_length_size missing

	if *stream.ID != "0x2" {
		t.Error("received invalid id")
	}
	if *stream.RealFrameRate != "30000/1001" {
		t.Error("received invalid real frame rate")
	}
	if *stream.AverageFrameRate != "30000/1001" {
		t.Error("received invalid average frame rate")
	}
	if *stream.TimeBase != "1/30000" {
		t.Error("received invalid time base")
	}
	if *stream.StartPTS != 0 {
		t.Error("received invalid start pts")
	}
	if *stream.StartTime != "0.000000" {
		t.Error("received invalid start time")
	}
	if *stream.DurationTS != 158158 {
		t.Error("received invalid duration ts")
	}
	if *stream.Duration != "5.271933" {
		t.Error("received invalid duration")
	}
	if *stream.BitRate != "3239582" {
		t.Error("received invalid bit rate")
	}

	// TODO: check, why there is no max bit rate

	if *stream.BitsPerRawSample != "8" {
		t.Error("received invalid bits per raw sample")
	}

	// TODO: other fields
}

func TestFFprobeInvalidFile(t *testing.T) {
	probe := NewFFprobe("test/invalid.mp4")
	_, err := probe.AnalyzeVideo()
	if err == nil {
		t.Error("expected error for invalid file")
	}
}
