/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package ffmpeg

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/marty-media/server/internal/settings"
)

type FFmpeg struct {
	cmd       *exec.Cmd
	Directory string
	logFile   *os.File
}

func Encode(args []string, dir string) (*FFmpeg, error) {
	// base directory for execution
	if dir == "" {
		baseDirectory := filepath.Join(settings.Instance.GetStorageTempDirectory(), "encode", fmt.Sprintf("hls_%d", time.Now().UnixNano()))
		if err := os.MkdirAll(baseDirectory, 0700); err != nil {
			log.WithField("inherit", err).Error("unable to create temporary directory for encoding")
			return nil, err
		}
		dir = baseDirectory
	}

	// create log file
	logFilePath := filepath.Join(dir, "ffmpeg.log")
	logFile, err := os.Create(logFilePath)
	if err != nil {
		log.WithField("inherit", err).Error("unable to create log file for ffmpeg")
		return nil, nil
	}

	// build FFmpeg command
	logFile.WriteString(strings.Join(args, " ") + "\n\n")
	ffmpegBinary := filepath.Join(settings.Instance.GetFFmpegLocation(), "ffmpeg")
	cmd := exec.Command(ffmpegBinary, args...)
	cmd.Dir = dir
	cmd.Stderr = logFile
	return &FFmpeg{
		cmd:       cmd,
		Directory: dir,
		logFile:   logFile,
	}, nil
}

func (ffmpeg *FFmpeg) Run() error {
	return ffmpeg.cmd.Run()
}

func (ffmpeg *FFmpeg) RunAsync() error {
	return ffmpeg.cmd.Start()
}

func (ffmpeg *FFmpeg) Finalize() {
	if err := ffmpeg.logFile.Close(); err != nil {
		log.WithField("inherit", err).Error("unable to close log file")
	}
}

func (ffmpeg *FFmpeg) CleanUp() {
	if err := os.RemoveAll(ffmpeg.Directory); err != nil {
		log.WithField("inherit", err).Error("unable to clean up ffmpeg")
	}
}
