/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package settings

import (
	"os"
	"testing"
)

const testConfigFile = "test.config.json"

func TestMain(m *testing.M) {
	// clean previous config files
	testClean()

	// store original environment variables
	dbType := os.Getenv("MMS_DATABASE_TYPE")
	os.Unsetenv("MMS_DATABASE_TYPE")
	dbPgAddress := os.Getenv("MMS_DATABASE_POSTGRES_ADDRESS")
	os.Unsetenv("MMS_DATABASE_POSTGRES_ADDRESS")
	dbPgPort := os.Getenv("MMS_DATABASE_POSTGRES_PORT")
	os.Unsetenv("MMS_DATABASE_POSTGRES_PORT")
	dbPgUser := os.Getenv("MMS_DATABASE_POSTGRES_USER")
	os.Unsetenv("MMS_DATABASE_POSTGRES_USER")
	dbPgPassword := os.Getenv("MMS_DATABASE_POSTGRES_PASSWORD")
	os.Unsetenv("MMS_DATABASE_POSTGRES_PASSWORD")
	dbPgDb := os.Getenv("MMS_DATABASE_POSTGRES_DATABASE")
	os.Unsetenv("MMS_DATABASE_POSTGRES_DATABASE")

	// execute tests
	response := m.Run()

	// clean temporary config files
	testClean()

	// restore original environment variables
	os.Setenv("MMS_DATABASE_TYPE", dbType)
	os.Setenv("MMS_DATABASE_POSTGRES_ADDRESS", dbPgAddress)
	os.Setenv("MMS_DATABASE_POSTGRES_PORT", dbPgPort)
	os.Setenv("MMS_DATABASE_POSTGRES_USER", dbPgUser)
	os.Setenv("MMS_DATABASE_POSTGRES_PASSWORD", dbPgPassword)
	os.Setenv("MMS_DATABASE_POSTGRES_DATABASE", dbPgDb)

	// exit testing
	os.Exit(response)
}

func testClean() {
	// create old settings file from previous test
	os.Remove(testConfigFile)
	Instance = &Settings{}
}

func createValidConfig() {
	os.WriteFile(testConfigFile, []byte(`{"webServer": {"listen": ":1234"}, "database": {"type": "asdf", "postgres": {"address": "strage-host-name", "port": 54545, "user": "martin", "password": "supersecure", "database": "db1", "maxOpenConnections": 10}}, "ffmpeg": {"location": "/path"}}`), 0644)
}

func TestNoConfig(t *testing.T) {
	testClean()
	if err := Instance.Load(""); err != nil {
		t.Error("invalid error on config loading", err)
	}

	// check default values
	if Instance.GetWebServerListen() != defaultWebServerListen {
		t.Error("invalid default web server listen address")
	}
	if Instance.GetDatabaseType() != defaultDatabaseType {
		t.Error("invalid default database type")
	}
	if Instance.GetDatabasePostgresAddress() != "" {
		t.Error("invalid default database postgres address")
	}
	if Instance.GetDatabasePostgresPort() != defaultDatabasePostgresPort {
		t.Error("invalid default database postgres port")
	}
	if Instance.GetDatabasePostgresUser() != "" {
		t.Error("invalid default database postgres user")
	}
	if Instance.GetDatabasePostgresPassword() != "" {
		t.Error("invalid default database postgres password")
	}
	if Instance.GetDatabasePostgresDatabase() != "" {
		t.Error("invalid default database postgres database")
	}
	if Instance.GetDatabasePostgresMaxOpenConnections() != defaultDatabasePostgresMaxOpenConnections {
		t.Error("invalid default database postgres max open connections")
	}
	if Instance.GetFFmpegLocation() != "" {
		t.Error("invalid default ff binary location")
	}
}

func TestConfig(t *testing.T) {
	testClean()
	createValidConfig()

	if err := Instance.Load(testConfigFile); err != nil {
		t.Error("invalid error on config loading", err)
	}

	// check values
	if Instance.GetWebServerListen() != ":1234" {
		t.Error("invalid listen address from config")
	}
	os.Setenv("MMS_WEBSERVER_LISTEN", ":1919")
	defer os.Unsetenv("MMS_WEBSERVER_LISTEN")
	if Instance.GetWebServerListen() != ":1919" {
		t.Error("invalid listen address from environment")
	}

	if Instance.GetDatabaseType() != "asdf" {
		t.Error("invalid database type from config")
	}
	os.Setenv("MMS_DATABASE_TYPE", "abcd")
	defer os.Unsetenv("MMS_DATABASE_TYPE")
	if Instance.GetDatabaseType() != "abcd" {
		t.Error("invalid database type from environment")
	}

	if Instance.GetDatabasePostgresAddress() != "strage-host-name" {
		t.Error("invalid database postgres address from config")
	}
	os.Setenv("MMS_DATABASE_POSTGRES_ADDRESS", "stranger-host-name")
	defer os.Unsetenv("MMS_DATABASE_POSTGRES_ADDRESS")
	if Instance.GetDatabasePostgresAddress() != "stranger-host-name" {
		t.Error("invalid database postgres address from environment")
	}

	if Instance.GetDatabasePostgresPort() != 54545 {
		t.Error("invalid database postgres port from config")
	}
	os.Setenv("MMS_DATABASE_POSTGRES_PORT", "55555")
	defer os.Unsetenv("MMS_DATABASE_POSTGRES_PORT")
	if Instance.GetDatabasePostgresPort() != 55555 {
		t.Error("invalid database postgres port from environment")
	}

	if Instance.GetDatabasePostgresUser() != "martin" {
		t.Error("invalid database postgres user from config")
	}
	os.Setenv("MMS_DATABASE_POSTGRES_USER", "awesomeuser")
	defer os.Unsetenv("MMS_DATABASE_POSTGRES_USER")
	if Instance.GetDatabasePostgresUser() != "awesomeuser" {
		t.Error("invalid database postgres user from environment")
	}

	if Instance.GetDatabasePostgresPassword() != "supersecure" {
		t.Error("invalid database postgres password from config")
	}
	os.Setenv("MMS_DATABASE_POSTGRES_PASSWORD", "supersecure2")
	defer os.Unsetenv("MMS_DATABASE_POSTGRES_PASSWORD")
	if Instance.GetDatabasePostgresPassword() != "supersecure2" {
		t.Error("invalid database postgres password from environment")
	}

	if Instance.GetDatabasePostgresDatabase() != "db1" {
		t.Error("invalid database postgres database from config")
	}
	os.Setenv("MMS_DATABASE_POSTGRES_DATABASE", "db2")
	defer os.Unsetenv("MMS_DATABASE_POSTGRES_DATABASE")
	if Instance.GetDatabasePostgresDatabase() != "db2" {
		t.Error("invalid database postgres database from environment")
	}

	if Instance.GetDatabasePostgresMaxOpenConnections() != 10 {
		t.Error("invalid database postgres max open connections from config")
	}
	os.Setenv("MMS_DATABASE_POSTGRES_MAX_OPEN_CONNECTIONS", "12")
	defer os.Unsetenv("MMS_DATABASE_POSTGRES_MAX_OPEN_CONNECTIONS")
	if Instance.GetDatabasePostgresMaxOpenConnections() != 12 {
		t.Error("invalid database postgres max open connections", 12)
	}

	if Instance.GetFFmpegLocation() != "/path" {
		t.Error("invalid ff binary location from config")
	}
	os.Setenv("MMS_FFMPEG_LOCATION", "/anotherPath")
	defer os.Unsetenv("MMS_FFMPEG_LOCATION")
	if Instance.GetFFmpegLocation() != "/anotherPath" {
		t.Error("invalid ff binary location from environment")
	}
}

func TestEnvironmentConfig(t *testing.T) {
	testClean()
	createValidConfig()
	os.Setenv("MMS_CONFIG", testConfigFile)
	defer os.Unsetenv("MMS_CONFIG")

	if err := Instance.Load(""); err != nil {
		t.Error("invalid error on config loading", err)
	}
}

func TestInvalidEnvironment(t *testing.T) {
	testClean()
	os.Setenv("MMS_DATABASE_POSTGRES_PORT", "invalid-number")
	defer os.Unsetenv("MMS_DATABASE_POSTGRES_PORT")

	// environment is invalid (not a number) --> expected result is the default value
	if Instance.GetDatabasePostgresPort() != defaultDatabasePostgresPort {
		t.Error("check for invalid environment failed", Instance.GetDatabasePostgresPort())
	}
}

func TestInvalidConfig(t *testing.T) {
	testClean()
	os.WriteFile(testConfigFile, []byte("asdf"), 0644)

	if err := Instance.Load(testConfigFile); err == nil {
		t.Error("missing error for invalid config file", err)
	}
}

func TestMissingConfig(t *testing.T) {
	testClean()
	if err := Instance.Load("not.exists.config.json"); err == nil {
		t.Error("missing error for missing config file", err)
	}
}
