/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package settings

import (
	"encoding/json"
	"os"
	"path/filepath"
	"strconv"

	log "github.com/sirupsen/logrus"
)

const (
	defaultWebServerListen                           = ":43200"
	defaultDatabaseType                              = "postgres"
	defaultDatabasePostgresPort                      = 5432
	defaultDatabasePostgresMaxOpenConnections        = 8
	defaultStorageCacheSize                   uint64 = 200_000_000_000
)

type Settings struct {
	data data
}

type data struct {
	WebServer webServer `json:"webServer"`
	Database  database  `json:"database"`
	FFmpeg    ffmpeg    `json:"ffmpeg"`
	Storage   storage   `json:"storage"`
}

var Instance = &Settings{}

func (settings *Settings) Load(filePath string) error {
	// check, if file path was provided
	filePath = settings.getConfigFilePath(filePath)
	if filePath == "" {
		log.Debug("no config file provided")
		return nil
	}

	// read settings file
	data, err := os.ReadFile(filePath)
	if err != nil {
		log.WithFields(log.Fields{
			"filePath": filePath,
			"inherit":  err,
		}).Error("unable to read settings file")
		return err
	}

	// parse json
	err = json.Unmarshal(data, &settings.data)
	if err != nil {
		log.WithField("inherit", err).Error("unable to unmarshal config file")
		return err
	}

	return nil
}

func (settings *Settings) getConfigFilePath(filePath string) string {
	// check environment variable
	config := os.Getenv("MMS_CONFIG")
	if config != "" {
		return config
	}

	// return command line flag
	return filePath
}

func (settings *Settings) getStringValue(environmentName string, jsonValue string, defaultValue string) string {
	// check environment variable
	val := os.Getenv(environmentName)
	if val != "" {
		return val
	}

	// check for value in config file
	if jsonValue != "" {
		return jsonValue
	}

	// return default value
	return defaultValue
}

func (settings *Settings) getIntValue(environmentName string, jsonValue int, defaultValue int) int {
	// check environment variable
	val := os.Getenv(environmentName)
	if val != "" {
		intVal, err := strconv.Atoi(val)
		if err == nil {
			return intVal
		}

		// show error and continue with next source
		log.WithFields(log.Fields{
			"name":  environmentName,
			"value": val,
		}).Error("invalid environment value")
	}

	// check for value in config file
	if jsonValue != 0 {
		return jsonValue
	}

	// return default value
	return defaultValue
}

func (settings *Settings) getUInt64Value(environmentName string, jsonValue *uint64, defaultValue uint64) uint64 {
	// check environment variable
	val := os.Getenv(environmentName)
	if val != "" {
		intVal, err := strconv.ParseUint(val, 10, 64)
		if err == nil {
			return intVal
		}

		// show error and continue with next source
		log.WithFields(log.Fields{
			"name":  environmentName,
			"value": val,
		}).Error("invalid environment value")
	}

	// check for value in config file
	if jsonValue != nil {
		return *jsonValue
	}

	// return default value
	return defaultValue
}

type webServer struct {
	Listen string `json:"listen"`
}

func (settings *Settings) GetWebServerListen() string {
	return settings.getStringValue("MMS_WEBSERVER_LISTEN", settings.data.WebServer.Listen, defaultWebServerListen)
}

type database struct {
	Type     string   `json:"type"`
	Postgres postgres `json:"postgres"`
}

func (settings *Settings) GetDatabaseType() string {
	return settings.getStringValue("MMS_DATABASE_TYPE", settings.data.Database.Type, defaultDatabaseType)
}

type postgres struct {
	Address            string `json:"address"`
	Port               int    `json:"port"`
	User               string `json:"user"`
	Password           string `json:"password"`
	Database           string `json:"database"`
	MaxOpenConnections int    `json:"maxOpenConnections"`
}

func (settings *Settings) GetDatabasePostgresAddress() string {
	return settings.getStringValue("MMS_DATABASE_POSTGRES_ADDRESS", settings.data.Database.Postgres.Address, "")
}

func (settings *Settings) GetDatabasePostgresPort() int {
	return settings.getIntValue("MMS_DATABASE_POSTGRES_PORT", settings.data.Database.Postgres.Port, defaultDatabasePostgresPort)
}

func (settings *Settings) GetDatabasePostgresUser() string {
	return settings.getStringValue("MMS_DATABASE_POSTGRES_USER", settings.data.Database.Postgres.User, "")
}

func (settings *Settings) GetDatabasePostgresPassword() string {
	return settings.getStringValue("MMS_DATABASE_POSTGRES_PASSWORD", settings.data.Database.Postgres.Password, "")
}

func (settings *Settings) GetDatabasePostgresDatabase() string {
	return settings.getStringValue("MMS_DATABASE_POSTGRES_DATABASE", settings.data.Database.Postgres.Database, "")
}

func (settings *Settings) GetDatabasePostgresMaxOpenConnections() int {
	return settings.getIntValue("MMS_DATABASE_POSTGRES_MAX_OPEN_CONNECTIONS", settings.data.Database.Postgres.MaxOpenConnections, defaultDatabasePostgresMaxOpenConnections)
}

type ffmpeg struct {
	Location string `json:"location"`
}

func (settings *Settings) GetFFmpegLocation() string {
	return settings.getStringValue("MMS_FFMPEG_LOCATION", settings.data.FFmpeg.Location, "")
}

type storage struct {
	Cache   cache  `json:"cache"`
	TempDir string `json:"tempDir"`
}

func (settings *Settings) GetStorageTempDirectory() string {
	directory := filepath.Join(os.TempDir(), "marty-media-server")
	return settings.getStringValue("MMS_STORAGE_TEMP_DIR", settings.data.Storage.TempDir, directory)
}

type cache struct {
	Size *uint64 `json:"size"`
}

func (settings *Settings) GetStorageCacheSize() uint64 {
	return settings.getUInt64Value("MMS_STORAGE_CACHE_SIZE", settings.data.Storage.Cache.Size, defaultStorageCacheSize)
}
