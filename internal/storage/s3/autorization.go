/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package s3

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"fmt"
	"io"
	"net/url"
	"sort"
	"strings"
	"time"
)

// Authorization based on AWS Signature v4
// https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-auth-using-authorization-header.html

const (
	headerAmazonPrefix = "x-amz-"
	headerDate         = "x-amz-date"
	headerContentHash  = "x-amz-content-sha256"
)

const (
	autorizationAlgorithm = "AWS4-HMAC-SHA256"
	serviceS3             = "s3"
	aws4Request           = "aws4_request"
)

func (s3 *S3) authorize(call *s3call) error {
	// add current date/time
	now := time.Now()
	formattedTime := now.UTC().Format("20060102T150405Z")
	call.request.Header.Add(headerDate, formattedTime)

	// calculate payload hash, if exists
	payloadHash := sha256.New().Sum(nil)
	if call.request.Body != nil {
		// load data
		data, err := io.ReadAll(call.request.Body)
		if err != nil {
			return err
		}

		// calculate sha256 hash
		hasher := sha256.New()
		_, err = hasher.Write(data)
		if err != nil {
			return err
		}
		payloadHash = hasher.Sum(nil)

		// set new reader for http call with same data
		call.request.Body = io.NopCloser(bytes.NewReader(data))
	}
	payloadHashHex := fmt.Sprintf("%x", payloadHash)
	call.request.Header.Add(headerContentHash, payloadHashHex)

	// calculate signature
	signedHeaders, signature := s3.calculateSignature(call, formattedTime, payloadHashHex)

	// add authorization header
	credentials := fmt.Sprintf("%s/%s/%s/%s/%s", s3.accessKey, formattedTime[0:8], s3.region, serviceS3, aws4Request)
	authorization := fmt.Sprintf("%s Credential=%s,SignedHeaders=%s,Signature=%s", autorizationAlgorithm, credentials, signedHeaders, signature)
	call.request.Header.Add("Authorization", authorization)
	return nil
}

type kv struct {
	key   string
	value string
}

func (s3 *S3) calculateSignature(call *s3call, formattedTime string, payloadHashHex string) (string, string) {
	httpMethod := call.request.Method
	canonicalURL := call.request.URL.EscapedPath()

	// build canonical query string
	queryParams := []kv{}
	for k, values := range call.request.URL.Query() {
		for _, v := range values {
			queryParams = append(queryParams, kv{
				key:   url.QueryEscape(k),
				value: url.QueryEscape(v),
			})
		}
	}
	sort.Slice(queryParams, func(i, j int) bool {
		return queryParams[i].key < queryParams[j].key
	})
	canonicalQueryString := ""
	for _, kv := range queryParams {
		if canonicalQueryString != "" {
			canonicalQueryString += "&"
		}
		canonicalQueryString += kv.key + "=" + kv.value
	}

	// build canonical headers
	canonicalHeaders, canonicalHeaderString := s3.buildCanonicalHeader(call)

	// build signed headers string
	signedHeadersString := ""
	for _, header := range canonicalHeaders {
		if signedHeadersString != "" {
			signedHeadersString += ";"
		}
		signedHeadersString += header.key
	}

	// build canonical request
	canonicalRequest := fmt.Sprintf("%s\n%s\n%s\n%s\n%s\n%s", httpMethod, canonicalURL, canonicalQueryString, canonicalHeaderString, signedHeadersString, payloadHashHex)
	canonicalRequestHash := sha256.New()
	canonicalRequestHash.Write([]byte(canonicalRequest))
	canonicalRequestSha256 := canonicalRequestHash.Sum(nil)

	// build scope
	scope := formattedTime[0:8] + "/" + s3.region + "/" + serviceS3 + "/" + aws4Request

	// build signing request
	signingRequest := fmt.Sprintf("%s\n%s\n%s\n%x", autorizationAlgorithm, formattedTime, scope, canonicalRequestSha256)

	// calculate signing key
	dateKey := hmacSha256([]byte("AWS4"+s3.secretKey), []byte(formattedTime[0:8]))
	dateRegionKey := hmacSha256(dateKey, []byte(s3.region))
	dateRegionServiceKey := hmacSha256(dateRegionKey, []byte(serviceS3))
	signingKey := hmacSha256(dateRegionServiceKey, []byte(aws4Request))

	// execute signing
	signagure := hmacSha256(signingKey, []byte(signingRequest))
	signatureHex := fmt.Sprintf("%x", signagure)
	return signedHeadersString, signatureHex
}

func (s3 *S3) buildCanonicalHeader(call *s3call) ([]kv, string) {
	canonicalHeaders := []kv{}
	canonicalHeaders = append(canonicalHeaders, kv{key: "host", value: call.request.Host})
	if contentType := call.request.Header.Get("content-type"); contentType != "" {
		canonicalHeaders = append(canonicalHeaders, kv{key: "content-type", value: contentType})
	}

	// add other hheaders
	for key, values := range call.request.Header {
		// only headers with amz prefix are relevant
		if !strings.HasPrefix(strings.ToLower(key), headerAmazonPrefix) {
			continue
		}

		for _, value := range values {
			canonicalHeaders = append(canonicalHeaders, kv{
				key:   strings.ToLower(key),
				value: value,
			})
		}
	}

	// sort by name (key)
	sort.Slice(canonicalHeaders, func(i, j int) bool {
		return canonicalHeaders[i].key < canonicalHeaders[j].key
	})

	// build canonical header string
	canonicalHeaderString := ""
	for _, header := range canonicalHeaders {
		canonicalHeaderString += header.key + ":" + header.value + "\n"
	}

	return canonicalHeaders, canonicalHeaderString
}

func hmacSha256(key, val []byte) []byte {
	hash := hmac.New(sha256.New, key)
	hash.Write(val)
	return hash.Sum(nil)
}
