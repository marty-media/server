/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package s3

import (
	"fmt"
	"testing"
	"time"
)

func testPutObjects(t *testing.T, s3 *S3) {
	// call list endpoint
	_, err := s3.PutObject(PutObjectRequest{
		Name:        fmt.Sprintf("tmp/put_%d.txt", time.Now().UnixNano()),
		Data:        []byte("everything is awesome!"),
		ContentType: "text/plain",
	})
	if err != nil {
		t.Fatal("unable to put new objects", err)
	}
}
