/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package s3

import (
	"bytes"
	"fmt"
	"math"
	"net/http"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/marty-media/server/internal/settings"
)

type GetObjectRequest struct {
	Name           string
	RangeStart     int
	SendRangeStart bool
	RangeEnd       int
	SendRangeEnd   bool
}

type GetObjectResponse struct {
	Data []byte
}

func (s3 *S3) GetObject(request GetObjectRequest) (*GetObjectResponse, error) {
	// prepare API call
	call, err := s3.newS3GetCall("/"+request.Name, http.MethodGet)
	if err != nil {
		return nil, err
	}

	// add header
	rangeHeader := ""
	if request.SendRangeStart && request.SendRangeEnd {
		rangeHeader = fmt.Sprintf("bytes=%d-%d", request.RangeStart, request.RangeEnd)
	} else if request.SendRangeStart {
		rangeHeader = fmt.Sprintf("bytes=%d-", request.RangeStart)
	}
	if rangeHeader != "" {
		call.request.Header.Add("Range", rangeHeader)
	}

	// execute API call
	dataBuffer := bytes.NewBuffer([]byte{})
	_, err = s3.Run(call, dataBuffer)
	if err != nil {
		return nil, err
	}

	// return response
	response := &GetObjectResponse{
		Data: dataBuffer.Bytes(),
	}
	return response, nil
}

type DownloadObjectRequest struct {
	Name string
}

type DownloadObjectResponse struct {
	FileName string
}

const (
	downloadChunkSize = 1024 * 1024 * 64 // 64 MB
)

func (s3 *S3) DownloadObject(request DownloadObjectRequest) (*DownloadObjectResponse, error) {
	// check for file (exists and size)
	headResponse, err := s3.HeadObject(HeadObjectRequest(request))
	if err != nil {
		return nil, err
	}

	// extract file size
	fileSize := headResponse.ContentLength

	// create temp file
	tmpFile := createTmpFile()
	deleteFile := false
	defer func() {
		if err := tmpFile.Close(); err != nil {
			log.WithError(err).Error("failed to close tmp file")
		}

		// if download failed, remove file
		if deleteFile {
			if err := os.Remove(tmpFile.Name()); err != nil {
				log.WithError(err).Error("failed to delete file")
			}
		}
	}()

	// download chunk
	chunks := int(math.Ceil(float64(fileSize) / float64(downloadChunkSize)))
	for i := 0; i < chunks; i++ {
		// download next chunk
		dl, err := s3.GetObject(GetObjectRequest{
			Name:           request.Name,
			RangeStart:     i * downloadChunkSize,
			SendRangeStart: true,
			RangeEnd:       (i+1)*downloadChunkSize - 1,
			SendRangeEnd:   true,
		})
		if err != nil {
			deleteFile = true
			return nil, err
		}

		// add file content
		if _, err := tmpFile.Write(dl.Data); err != nil {
			deleteFile = true
			log.WithFields(log.Fields{
				"inherit":  err,
				"fileName": tmpFile.Name(),
			}).Error("unable to write temporary file content")
			return nil, err
		}
	}

	// build response
	response := &DownloadObjectResponse{
		FileName: tmpFile.Name(),
	}
	return response, nil
}

func createTmpFile() *os.File {
	// find temporary folder
	outputDirectory := settings.Instance.GetStorageTempDirectory()

	// build output directory
	outputDirectory = filepath.Join(outputDirectory, "s3")

	// create new directory, if not already exists
	if err := os.MkdirAll(outputDirectory, 0700); err != nil {
		log.WithField("inherit", err).Error("unable to create temporary directory for s3")
		return nil
	}

	// create new file
	fileName := fmt.Sprintf("%d.data", time.Now().UnixMilli())
	filePath := filepath.Join(outputDirectory, fileName)
	file, err := os.OpenFile(filePath, os.O_CREATE|os.O_EXCL|os.O_WRONLY, 0600)
	if err != nil {
		log.WithField("file", filePath).Error("unable to create temporary file for s3")
	}

	return file
}
