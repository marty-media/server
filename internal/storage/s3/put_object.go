/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package s3

import (
	"crypto/md5"
	"encoding/base64"
	"net/http"
	"strconv"
)

// This implementation is based on the documentation of AWS
// https://docs.aws.amazon.com/AmazonS3/latest/API/API_PutObject.html

type PutObjectRequest struct {
	Name        string
	Data        []byte
	ContentType string
}

type PutObjectResult struct {
}

func (s3 *S3) PutObject(request PutObjectRequest) (*PutObjectResult, error) {
	// prepare API call
	call, err := s3.newS3PutCall("/"+request.Name, request.Data, http.MethodPut)
	if err != nil {
		return nil, err
	}

	// prepare http headers
	call.request.Header.Add("Content-Type", request.ContentType)
	call.request.Header.Add("Content-Length", strconv.Itoa(len(request.Data)))

	// calculate MD5
	md5 := md5.Sum(request.Data)
	call.request.Header.Add("Content-MD5", base64.StdEncoding.EncodeToString(md5[:]))

	// execute API call
	var response PutObjectResult
	if _, err = s3.Run(call, nil); err != nil {
		return nil, err
	}

	return &response, err
}
