/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package s3

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"reflect"

	log "github.com/sirupsen/logrus"
)

const (
	DefaultDelimiter = "/"
)

type S3 struct {
	address   string
	accessKey string
	secretKey string
	region    string
}

type S3Request struct {
	Address   string
	AccessKey string
	SecretKey string
	Region    string
}

func NewS3(request S3Request) *S3 {
	return &S3{
		address:   request.Address,
		accessKey: request.AccessKey,
		secretKey: request.SecretKey,
		region:    request.Region,
	}
}

type s3call struct {
	request *http.Request
}

func (s3 *S3) newS3GetCall(url string, method string) (*s3call, error) {
	fullURL := s3.address + url
	request, err := http.NewRequest(method, fullURL, nil)
	return &s3call{
		request: request,
	}, err
}

func (s3 *S3) newS3PutCall(url string, body []byte, method string) (*s3call, error) {
	fullURL := s3.address + url
	bodyReader := bytes.NewReader(body)
	request, err := http.NewRequest(method, fullURL, bodyReader)
	return &s3call{
		request: request,
	}, err
}

func (s3 *S3) Run(call *s3call, response any) (http.Header, error) {
	// authorize API call
	if err := s3.authorize(call); err != nil {
		log.WithField("inherit", err).Error("error during S3 authorization calculation")
		return nil, err
	}

	// execute API call
	httpResponse, err := http.DefaultClient.Do(call.request)
	if err != nil {
		log.WithField("inherit", err).Error("error during S3 API call")
		return nil, err
	}

	// parse response
	responseData, err := io.ReadAll(httpResponse.Body)
	if err != nil {
		log.WithField("inherit", err).Error("error during response of S3 API call")
		return nil, err
	}

	// check HTTP response code
	if httpResponse.StatusCode < 200 || httpResponse.StatusCode > 299 {
		log.WithFields(log.Fields{
			"statusCode":     httpResponse.StatusCode,
			"response":       string(responseData),
			"responseHeader": httpResponse.Header,
			"requestHeader":  httpResponse.Request.Header,
			"requestURL":     httpResponse.Request.URL,
		}).Error("invalid http response status from S3 API")
		return nil, fmt.Errorf("invalid http response status %d", httpResponse.StatusCode)
	}

	// parse response
	if response != nil && reflect.TypeOf(response) == reflect.TypeOf(&bytes.Buffer{}) {
		response.(*bytes.Buffer).Write(responseData)
	} else if response != nil {
		if err = xml.Unmarshal(responseData, response); err != nil {
			log.WithField("inherit", err).Error("unable to unmarshal XML response from S3")
			return nil, err
		}
	}

	return httpResponse.Header, nil
}
