/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package s3

import (
	"os"
	"testing"
)

func testGetObject(t *testing.T, s3 *S3) {
	testGetFullObject(t, s3)
	testGetPartialObject(t, s3)
	testGetPartialObject2(t, s3)
	testDownloadObject(t, s3)
}

func testGetFullObject(t *testing.T, s3 *S3) {
	// call get object endpoint
	result, err := s3.GetObject(GetObjectRequest{
		Name: "static/static-test.txt",
	})
	if err != nil {
		t.Fatal("unable to get object", err)
	}

	// check result
	if string(result.Data) != "static123" {
		t.Fatal("received invalid data from GetObject", string(result.Data))
	}
}

func testGetPartialObject(t *testing.T, s3 *S3) {
	// call get object endpoint
	result, err := s3.GetObject(GetObjectRequest{
		Name:           "static/static-test.txt",
		RangeStart:     0,
		SendRangeStart: true,
		RangeEnd:       5,
		SendRangeEnd:   true,
	})
	if err != nil {
		t.Fatal("unable to get object", err)
	}

	// check result
	if string(result.Data) != "static" {
		t.Fatal("received invalid data from GetObject", string(result.Data))
	}
}

func testGetPartialObject2(t *testing.T, s3 *S3) {
	// call get object endpoint
	result, err := s3.GetObject(GetObjectRequest{
		Name:           "static/static-test.txt",
		RangeStart:     6,
		SendRangeStart: true,
	})
	if err != nil {
		t.Fatal("unable to get object", err)
	}

	// check result
	if string(result.Data) != "123" {
		t.Fatal("received invalid data from GetObject", string(result.Data))
	}
}

func testDownloadObject(t *testing.T, s3 *S3) {
	// call download object endpoint
	result, err := s3.DownloadObject(DownloadObjectRequest{
		Name: "static/static test.txt",
	})
	if err != nil {
		t.Fatal("unable to download object", err)
	}
	defer os.Remove(result.FileName)

	// check result
	fileContent, err := os.ReadFile(result.FileName)
	if err != nil {
		t.Fatal(err)
	} else if string(fileContent) != "static123" {
		t.Fatal("received invalid data from GetObject", string(fileContent))
	}
}
