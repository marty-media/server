/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package s3

import (
	"net/http"
	"strconv"
)

type HeadObjectRequest struct {
	Name string
}

type HeadObjectResponse struct {
	ContentLength int
}

func (s3 *S3) HeadObject(request HeadObjectRequest) (*HeadObjectResponse, error) {
	// prepare API call
	call, err := s3.newS3GetCall("/"+request.Name, http.MethodHead)
	if err != nil {
		return nil, err
	}

	// execute API call
	header, err := s3.Run(call, nil)
	if err != nil {
		return nil, err
	}

	// parse header
	response := &HeadObjectResponse{}
	contentTypeString := header.Get("Content-Length")
	contentType, err := strconv.Atoi(contentTypeString)
	if err == nil {
		response.ContentLength = contentType
	}

	// return response
	return response, nil
}
