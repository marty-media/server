/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package s3

import (
	"net/http"
	"time"
)

// This implementation is based on the documentation of AWS
// https://docs.aws.amazon.com/AmazonS3/latest/API/API_ListObjectsV2.html

type ListBucketV2Request struct {
	Prefix            string
	Delimiter         string
	ContinuationToken string
}

type ListBucketResult struct {
	Name                  string             `xml:"Name"`
	Prefix                string             `xml:"Prefix"`
	IsTruncated           bool               `xml:"IsTruncated"`
	KeyCount              int                `xml:"KeyCount"`
	MaxKeys               int                `xml:"MaxKeys"`
	ContinuationToken     string             `xml:"ContinuationToken"`
	NextContinuationToken string             `xml:"NextContinuationToken"`
	Content               []ListBucketObject `xml:"Contents"`
}

type ListBucketObject struct {
	ETag         string    `xml:"ETag"`
	Key          string    `xml:"Key"`
	LastModified time.Time `xml:"LastModified"`
	Size         int       `xml:"Size"`
}

func (s3 *S3) ListObjectsV2(request ListBucketV2Request) (*ListBucketResult, error) {
	// prepare API call
	call, err := s3.newS3GetCall("/?list-type=2", http.MethodGet)
	if err != nil {
		return nil, err
	}

	// add parameters
	params := call.request.URL.Query()
	if request.Prefix != "" {
		params.Add("prefix", request.Prefix)
	}
	if request.Delimiter != "" {
		params.Add("delimiter", request.Delimiter)
	}
	if request.ContinuationToken != "" {
		params.Add("continuation-token", request.ContinuationToken)
	}
	call.request.URL.RawQuery = params.Encode()

	// execute API call
	var response ListBucketResult
	if _, err = s3.Run(call, &response); err != nil {
		return nil, err
	}

	return &response, err
}

type ListAllObjectsRequest struct {
	Prefix    string
	Delimiter string
}

type ListAllObjectsResult struct {
	Content []ListBucketObject
}

func (s3 *S3) ListAllObjects(request ListAllObjectsRequest) (*ListAllObjectsResult, error) {
	listRequest := ListBucketV2Request{
		Prefix:    request.Prefix,
		Delimiter: request.Delimiter,
	}

	response := &ListAllObjectsResult{}
	for {
		// load S3 objects
		listResult, err := s3.ListObjectsV2(listRequest)
		if err != nil {
			return nil, err
		}

		// store result
		response.Content = append(response.Content, listResult.Content...)

		// check for more results
		if listResult.ContinuationToken != "" {
			listRequest.ContinuationToken = listResult.ContinuationToken
		} else {
			break
		}
	}

	return response, nil
}
