/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package s3

import "testing"

func testListObjects(t *testing.T, s3 *S3) {
	testListObjectsV2(t, s3)
	testListAllObjects(t, s3)
}

func testListObjectsV2(t *testing.T, s3 *S3) {
	// call list endpoint
	result, err := s3.ListObjectsV2(ListBucketV2Request{
		Prefix: "static",
	})
	if err != nil {
		t.Fatal("unable to list objects", err)
	}

	// check result
	if len(result.Content) < 1 {
		t.Fatal("invalid content in list objects")
	}
}

func testListAllObjects(t *testing.T, s3 *S3) {
	result, err := s3.ListAllObjects(ListAllObjectsRequest{
		Prefix: "static",
	})
	if err != nil {
		t.Fatal("unable to list all objects", err)
	}

	// check result
	if len(result.Content) < 1 {
		t.Fatal("invalid content in list all objects")
	}
}
