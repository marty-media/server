/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package s3

import (
	"log"
	"os"
	"testing"

	"gitlab.com/marty-media/server/internal/database"
)

func TestMain(m *testing.M) {
	// init backend
	if err := database.InitDatabase(); err != nil {
		log.Println(err)
		os.Exit(-1)
	}
	if err := database.Instance.Upgrade(); err != nil {
		log.Println(err)
		os.Exit(-1)
	}

	// run tests
	response := m.Run()
	os.Exit(response)
}

func TestS3(t *testing.T) {
	s3 := NewS3(S3Request{
		Address:   os.Getenv("MMS__S3_ADDRESS"),
		AccessKey: os.Getenv("MMS__S3_ACCESS_KEY"),
		SecretKey: os.Getenv("MMS__S3_SECRET_KEY"),
		Region:    os.Getenv("MMS__S3_REGION"),
	})
	testListObjects(t, s3)
	testHeadObject(t, s3)
	testGetObject(t, s3)
	testPutObjects(t, s3)
}

func TestS3Errors(t *testing.T) {
	s3 := NewS3(S3Request{
		Address:   os.Getenv("MMS__S3_ADDRESS"),
		AccessKey: os.Getenv("MMS__S3_ACCESS_KEY"),
		SecretKey: "invalid-key",
	})
	if _, err := s3.ListObjectsV2(ListBucketV2Request{}); err == nil {
		t.Error("expected error for invalid secret key")
	}
}
