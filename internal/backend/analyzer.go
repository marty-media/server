/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package backend

import (
	"database/sql"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/ffmpeg"
)

const (
	AnalyzerVersion = 3
)

type Analyzer struct {
	timer *time.Ticker
}

var AnalyzerInstance = newAnalyzerInstance()

type mediaAnalyzeJob struct {
	mediaItem       *database.MediaItem
	transaction     *database.Transaction
	media           *database.Media
	libraryLocation *database.LibraryLocation
	cacheItem       *CacheItem
}

func newAnalyzerInstance() *Analyzer {
	return &Analyzer{
		timer: time.NewTicker(time.Minute),
	}
}

func (analyzer *Analyzer) Start() error {
	// run background threads
	go analyzer.runMediaScanJobs()
	return nil
}

func (analyzer *Analyzer) runMediaScanJobs() {
	for {
		// check for next media item
		mediaItem := analyzer.findNextMediaItem()
		if mediaItem != nil {
			analyzer.runMediaScanJob(&mediaAnalyzeJob{
				mediaItem: mediaItem,
			})
			continue
		}

		// wait until next timer tick
		<-analyzer.timer.C
	}
}

func (analyzer *Analyzer) findNextMediaItem() *database.MediaItem {
	// create new transaction (for select lock)
	transaction, err := database.Instance.Transaction()
	if err != nil {
		log.WithField("inherit", err).Error("unable to create new transaction")
		return nil
	}
	defer transaction.Rollback()

	// find next entry
	mediaItem, err := transaction.FindMediaItemToAnalyze(AnalyzerVersion)
	if err != nil {
		log.WithField("inherit", err).Error("unable to find next media item for analyzation")
		return nil
	}

	// new item found?
	if mediaItem == nil {
		return nil
	}

	// update analyzation status
	err = transaction.UpdateMediaItem(database.UpdateMediaItemRequest{
		ID:                   mediaItem.ID,
		AnalyzedStatus:       database.MediaItemAnalyzedStatusStarted,
		UpdateAnalyzedStatus: true,
	})
	if err != nil {
		log.WithField("inherit", err).Error("unable to update media item for analyzation start")
		return nil
	}

	// commit changes
	if err := transaction.Commit(); err != nil {
		log.WithField("inherit", err).Error("unable to commit transaction")
		return nil
	}

	return mediaItem
}

func (analyzer *Analyzer) runMediaScanJob(job *mediaAnalyzeJob) {
	// process clean up
	defer job.cleanUp()

	// create new transaction
	transaction, err := database.Instance.Transaction()
	if err != nil {
		return
	}
	job.transaction = transaction

	// run execution
	jobFields := log.Fields{
		"mediaItemID":   job.mediaItem.ID,
		"mediaItemFile": job.mediaItem.File,
	}
	log.WithFields(jobFields).Info("start analyzation of media item")
	defer log.WithFields(jobFields).Info("analyzation of media item completed")
	changeDetected, success := analyzer.runMediaScan(job)

	// processing failed?
	if !success {
		// rollback all actions
		transaction.Rollback()

		// update status
		if err := database.Instance.UpdateMediaItem(database.UpdateMediaItemRequest{
			ID:                    job.mediaItem.ID,
			AnalyzedStatus:        database.MediaItemAnalyzedStatusFailed,
			UpdateAnalyzedStatus:  true,
			AnalyzerVersion:       AnalyzerVersion,
			UpdateAnalyzerVersion: true,
			UpdateLastAnalyzedAt:  true,
		}); err != nil {
			log.WithFields(log.Fields{
				"mediaID": job.mediaItem.ID,
				"inherit": err,
			}).Error("unable to set new media status 'failed'")
		}
		return
	}

	// job completed
	dbUpdate := database.UpdateMediaItemRequest{
		ID:                    job.mediaItem.ID,
		AnalyzedStatus:        database.MediaItemAnalyzedStatusCompleted,
		UpdateAnalyzedStatus:  true,
		AnalyzerVersion:       AnalyzerVersion,
		UpdateAnalyzerVersion: true,
		UpdateLastAnalyzedAt:  true,
	}
	if changeDetected {
		// required, to trigger encoder job (keep initial status, if this is the first scan)
		if job.mediaItem.EncodedStatus != database.MediaItemEncodedStatusInitial {
			dbUpdate.EncodedStatus = database.MediaItemEncodedStatusRetry
			dbUpdate.UpdateEncodedStatus = true
		}
	}
	if err := transaction.UpdateMediaItem(dbUpdate); err != nil {
		log.WithFields(log.Fields{
			"mediaID": job.mediaItem.ID,
			"inherit": err,
		}).Error("unable to set new media status 'completed'")
		return
	}
	if err := job.transaction.Commit(); err != nil {
		log.WithField("inherit", err).Error("unable to commit transaction")
		return
	}
}

func (analyzer *Analyzer) runMediaScan(job *mediaAnalyzeJob) (bool, bool) {
	// load media
	media, err := job.transaction.GetMedia(job.mediaItem.MediaID)
	if err != nil {
		log.WithField("inherit", err).Error("unable to load media by ID")
		return false, false
	}
	job.media = media

	// load library location
	libraryLocation, err := job.transaction.GetLibraryLocation(job.mediaItem.LibraryLocationID)
	if err != nil {
		log.WithField("inhert", err).Error("unable to load library location by ID")
		return false, false
	}
	job.libraryLocation = libraryLocation

	// get media path
	mediaPathResponse, success := GetMediaItemFilePath(job.mediaItem, media, libraryLocation)
	if !success {
		return false, false
	}
	job.cacheItem = mediaPathResponse.CacheItem

	// start analyzation
	ffprobeInfo := analyzer.analyzeMovieFile(job, mediaPathResponse.Path)
	if ffprobeInfo == nil {
		log.Error("ffprobe analyzation failed")
		return false, false
	}

	// store media format
	formatChangeDetected, ok := analyzer.processMediaFormat(job, ffprobeInfo)
	if !ok {
		log.Error("media format processing failed")
		return false, false
	}

	// store media streams
	streamChangeDetected, ok := analyzer.processMediaStreams(job, ffprobeInfo)
	if !ok {
		log.Error("stream processing of media item failed")
		return false, false
	}

	return formatChangeDetected || streamChangeDetected, true
}

func (analyzer *Analyzer) analyzeMovieFile(job *mediaAnalyzeJob, filePath string) *ffmpeg.FileInfo {
	// run FFprobe
	ffprobe := ffmpeg.NewFFprobe(filePath)
	ffprobeInfo, err := ffprobe.AnalyzeVideo()
	if err != nil {
		log.WithFields(log.Fields{
			"mediaItemID": job.mediaItem.ID,
			"filePath":    filePath,
			"inherit":     err,
		}).Error("unable to detect file with ffprobe")
		return nil
	}
	return ffprobeInfo
}

func (analyzer *Analyzer) processMediaFormat(job *mediaAnalyzeJob, ffprobeInfo *ffmpeg.FileInfo) (bool, bool) {
	// load current media format
	mediaFormats, err := job.transaction.FindMediaFormatsByMediaItem(job.mediaItem.ID)
	if err != nil {
		log.WithFields(log.Fields{
			"mediaItemID": job.mediaItem.ID,
			"inherit":     err,
		}).Error("unable to find media formats by media item")
		return false, false
	}

	var latestMediaFormat *database.MediaFormat
	for _, mediaFormat := range mediaFormats {
		if mediaFormat.Latest {
			latestMediaFormat = mediaFormat
			break
		}
	}

	// parse new media format
	mediaFormat := analyzer.parseMediaFormat(ffprobeInfo.Format, job.mediaItem.ID)

	// check, if media format is unchanged
	if latestMediaFormat != nil && analyzer.isMediaFormatEqual(mediaFormat, latestMediaFormat) {
		return false, true
	}

	// create new media format
	mediaFormat.Latest = true
	if _, err := job.transaction.CreateMediaFormat(*mediaFormat); err != nil {
		log.WithFields(log.Fields{
			"mediaItemID": job.mediaItem.ID,
			"inherit":     err,
		}).Error("unable to create new media format")
		return false, false
	}

	// expire old media formats
	for _, mediaFormat := range mediaFormats {
		if !mediaFormat.Latest {
			continue
		}

		// mark media format as outdated
		if err := job.transaction.UpdateMediaFormat(database.UpdateMediaFormatRequest{
			ID:           mediaFormat.ID,
			Latest:       false,
			UpdateLatest: true,
		}); err != nil {
			log.WithFields(log.Fields{
				"mediaItemID":   job.mediaItem.ID,
				"mediaFormatID": mediaFormat.ID,
				"inherit":       err,
			}).Error("unable to update media format")
			return false, false
		}
	}

	return true, true
}

func (analyzer *Analyzer) processMediaStreams(job *mediaAnalyzeJob, ffprobeInfo *ffmpeg.FileInfo) (bool, bool) {
	// load currently stored streams
	mediaStreams, err := job.transaction.FindMediaStreamsByMediaItem(job.mediaItem.ID)
	if err != nil {
		log.WithFields(log.Fields{
			"mediaItemID": job.mediaItem.ID,
			"inherit":     err,
		}).Error("unable to load media streams for media item")
		return false, false
	}

	// check media streams
	unchangedMediaStreamIDs := []int64{}
	changeDetected := false
	for _, stream := range ffprobeInfo.Streams {
		// find current database media stream
		var mediaStream *database.MediaStream
		for _, currentMediaStream := range mediaStreams {
			if currentMediaStream.Index == int64(stream.Index) && currentMediaStream.Latest {
				mediaStream = currentMediaStream
				break
			}
		}

		// build new stream
		newMediaStream := analyzer.parseMediaStream(stream, job.mediaItem.ID)
		if newMediaStream == nil {
			return false, false
		}

		// build new stream tag list
		newMediaStreamTags := analyzer.parseMediaStreamTags(stream)

		// check, if media stream is unchanged
		if mediaStream != nil && analyzer.isMediaStreamEqual(mediaStream, newMediaStream) &&
			analyzer.areMediaStreamTagsEqual(job, newMediaStreamTags, mediaStream.ID) {
			unchangedMediaStreamIDs = append(unchangedMediaStreamIDs, mediaStream.ID)
			continue
		}

		// create new media stream
		newMediaStream.Latest = true
		changeDetected = true
		newMediaStreamID, err := job.transaction.CreateMediaStream(*newMediaStream)
		if err != nil {
			log.WithField("inherit", err).Error("unable to create new media stream")
			return false, false
		}

		// create new media stream tags
		for _, newMediaStreamTag := range newMediaStreamTags {
			newMediaStreamTag.MediaStream = newMediaStreamID
			if _, err := job.transaction.CreateMediaStreamTag(*newMediaStreamTag); err != nil {
				log.WithField("inherit", err).Error("unable to create new media stream tag")
				return false, false
			}
		}
	}

	// mark other streams as outdated
	deletedChangeDetected, success := analyzer.processOutdatedMediaStreams(job, mediaStreams, unchangedMediaStreamIDs)
	if !success {
		return false, false
	}
	return changeDetected || deletedChangeDetected, true
}

func (analyzer *Analyzer) processOutdatedMediaStreams(job *mediaAnalyzeJob, mediaStreams []*database.MediaStream, unchangedMediaStreamIDs []int64) (bool, bool) {
	changeDetected := false
	for _, mediaStream := range mediaStreams {
		// check, if stream is already outdated
		if !mediaStream.Latest {
			continue
		}

		// check, if stream is still relevant
		isProcessed := false
		for _, processedMediaStreamID := range unchangedMediaStreamIDs {
			if mediaStream.ID == processedMediaStreamID {
				isProcessed = true
				break
			}
		}

		// processed records are fine
		if isProcessed {
			continue
		}

		// update media stream as outdated
		changeDetected = true
		if err := job.transaction.UpdateMediaStream(mediaStream.ID, false, true); err != nil {
			log.WithField("inherit", err).Error("unable to update media stream as outdated")
			return false, false
		}
	}

	return changeDetected, true
}

func (analyzer *Analyzer) parseMediaFormat(ffmpegFormat ffmpeg.Format, mediaItemID int64) *database.MediaFormat {
	startTime, err := parseNullFloatByString(&ffmpegFormat.StartTime)
	if err != nil {
		return nil
	}
	duration, err := parseNullFloatByString(&ffmpegFormat.Duration)
	if err != nil {
		return nil
	}

	return &database.MediaFormat{
		MediaItemID:    mediaItemID,
		FormatName:     ffmpegFormat.FormatName,
		FormatLongName: ffmpegFormat.FormatLongName,
		StartTime:      startTime,
		Duration:       duration,
	}
}

func (analyzer *Analyzer) isMediaFormatEqual(f1, f2 *database.MediaFormat) bool {
	return f1.FormatName == f2.FormatName &&
		f1.FormatLongName == f2.FormatLongName &&
		equalNullFloat(f1.Duration, f2.Duration) &&
		equalNullFloat(f1.StartTime, f2.StartTime)
}

func (analyzer *Analyzer) parseMediaStream(ffmpegStream ffmpeg.Stream, mediaItemID int64) *database.MediaStream {
	sampleRate, err := parseNullIntByString(ffmpegStream.SampleRate)
	if err != nil {
		return nil
	}
	startTime, err := parseNullFloatByString(ffmpegStream.StartTime)
	if err != nil {
		return nil
	}
	duration, err := parseNullFloatByString(ffmpegStream.Duration)
	if err != nil {
		return nil
	}
	bitRate, err := parseNullIntByString(ffmpegStream.BitRate)
	if err != nil {
		return nil
	}
	maxBitRate, err := parseNullIntByString(ffmpegStream.MaxBitRate)
	if err != nil {
		return nil
	}
	bitsPerRawSample, err := parseNullIntByString(ffmpegStream.BitsPerRawSample)
	if err != nil {
		return nil
	}

	stream := database.MediaStream{
		MediaItemID:         mediaItemID,
		Index:               int64(ffmpegStream.Index),
		CodecName:           ffmpegStream.CodecName,
		CodecLongName:       parseNullString(ffmpegStream.CodecNameLong),
		Profile:             parseNullString(ffmpegStream.Profile),
		CodecType:           ffmpegStream.CodecType,
		CodecTagString:      parseNullString(ffmpegStream.CodecTagString),
		CodecTag:            parseNullString(ffmpegStream.CodecTag),
		Width:               parseNullInt(ffmpegStream.Width),
		Height:              parseNullInt(ffmpegStream.Height),
		CodedWidth:          parseNullInt(ffmpegStream.CodedWidth),
		CodedHeight:         parseNullInt(ffmpegStream.CodedHeight),
		ClosedCaptions:      parseNullBoolByInt(ffmpegStream.ClosedCaptions),
		FilmGrain:           parseNullBoolByInt(ffmpegStream.FilmGrain),
		HasBFrames:          parseNullInt(ffmpegStream.HasBFrames),
		SampleAspectRatio:   parseNullString(ffmpegStream.SampleAspectRatio),
		DisplayAspectRatio:  parseNullString(ffmpegStream.DisplayAspectRatio),
		PixelFormat:         parseNullString(ffmpegStream.PixelFormat),
		Level:               parseNullInt(ffmpegStream.Level),
		ColorRange:          parseNullString(ffmpegStream.ColorRange),
		ColorSpace:          parseNullString(ffmpegStream.ColorSpace),
		ColorTransferMatrix: parseNullString(ffmpegStream.ColorTransferMatrix),
		ColorPrimaries:      parseNullString(ffmpegStream.ColorPrimaries),
		ChromaLocation:      parseNullString(ffmpegStream.ChromaLocation),
		FieldOrder:          parseNullString(ffmpegStream.FieldOrder),
		Refs:                parseNullInt(ffmpegStream.Refs),
		SampleFormat:        parseNullString(ffmpegStream.SampleFormat),
		SampleRate:          sampleRate,
		Channels:            parseNullInt(ffmpegStream.Channels),
		ChannelLayout:       parseNullString(ffmpegStream.ChannelLayout),
		BitsPerSample:       parseNullInt(ffmpegStream.BitsPerSample),
		InitialPadding:      parseNullInt(ffmpegStream.InitialPadding),
		StreamID:            parseNullString(ffmpegStream.ID),
		RealFrameRate:       parseNullString(ffmpegStream.RealFrameRate),
		AverageFrameRate:    parseNullString(ffmpegStream.AverageFrameRate),
		TimeBase:            parseNullString(ffmpegStream.TimeBase),
		StartPTS:            parseNullInt(ffmpegStream.StartPTS),
		StartTime:           startTime,
		DurationTS:          parseNullInt(ffmpegStream.DurationTS),
		Duration:            duration,
		BitRate:             bitRate,
		MaxBitRate:          maxBitRate,
		BitsPerRawSample:    bitsPerRawSample,
	}

	return &stream
}

func (analyzer *Analyzer) isMediaStreamEqual(s1, s2 *database.MediaStream) bool {
	return s1.Index == s2.Index &&
		s1.CodecName == s2.CodecName &&
		equalNullString(s1.CodecLongName, s2.CodecLongName) &&
		equalNullString(s1.Profile, s2.Profile) &&
		s1.CodecType == s2.CodecType &&
		equalNullString(s1.CodecTagString, s2.CodecTagString) &&
		equalNullString(s1.CodecTag, s2.CodecTag) &&
		equalNullInt(s1.Width, s2.Width) &&
		equalNullInt(s1.Height, s2.Height) &&
		equalNullInt(s1.CodedWidth, s2.CodedWidth) &&
		equalNullInt(s1.CodedHeight, s2.CodedHeight) &&
		equalNullBool(s1.ClosedCaptions, s2.ClosedCaptions) &&
		equalNullBool(s1.FilmGrain, s2.FilmGrain) &&
		equalNullInt(s1.HasBFrames, s2.HasBFrames) &&
		equalNullString(s1.SampleAspectRatio, s2.SampleAspectRatio) &&
		equalNullString(s1.DisplayAspectRatio, s2.DisplayAspectRatio) &&
		equalNullString(s1.PixelFormat, s2.PixelFormat) &&
		equalNullInt(s1.Level, s2.Level) &&
		equalNullString(s1.ColorRange, s2.ColorRange) &&
		equalNullString(s1.ColorSpace, s2.ColorSpace) &&
		equalNullString(s1.ColorTransferMatrix, s2.ColorTransferMatrix) &&
		equalNullString(s1.ColorPrimaries, s2.ColorPrimaries) &&
		equalNullString(s1.ChromaLocation, s2.ChromaLocation) &&
		equalNullString(s1.FieldOrder, s2.FieldOrder) &&
		equalNullInt(s1.Refs, s2.Refs) &&
		equalNullString(s1.SampleFormat, s2.SampleFormat) &&
		equalNullInt(s1.SampleRate, s2.SampleRate) &&
		equalNullInt(s1.Channels, s2.Channels) &&
		equalNullString(s1.ChannelLayout, s2.ChannelLayout) &&
		equalNullInt(s1.BitsPerSample, s2.BitsPerSample) &&
		equalNullInt(s1.InitialPadding, s2.InitialPadding) &&
		equalNullString(s1.StreamID, s2.StreamID) &&
		equalNullString(s1.RealFrameRate, s2.RealFrameRate) &&
		equalNullString(s1.AverageFrameRate, s2.AverageFrameRate) &&
		equalNullString(s1.TimeBase, s2.TimeBase) &&
		equalNullInt(s1.StartPTS, s2.StartPTS) &&
		equalNullFloat(s1.StartTime, s2.StartTime) &&
		equalNullInt(s1.DurationTS, s2.DurationTS) &&
		equalNullFloat(s1.Duration, s2.Duration) &&
		equalNullInt(s1.BitRate, s2.BitRate) &&
		equalNullInt(s1.MaxBitRate, s2.MaxBitRate) &&
		equalNullInt(s1.BitsPerRawSample, s2.BitsPerRawSample)
}

func (analyzer *Analyzer) parseMediaStreamTags(ffmpegStream ffmpeg.Stream) (response []*database.MediaStreamTag) {
	for key, val := range ffmpegStream.Tags {
		response = append(response, &database.MediaStreamTag{
			Key:   key,
			Value: sql.NullString{String: val, Valid: true},
		})
	}
	return
}

func (analyzer *Analyzer) areMediaStreamTagsEqual(job *mediaAnalyzeJob, newTags []*database.MediaStreamTag, streamID int64) bool {
	// load current tags
	tags, err := job.transaction.FindMediaStreamTagsByMediaStream(streamID)
	if err != nil {
		log.WithField("inherit", err).Error("error loading media stream tags")
		return false
	}

	// check, if all new tags exists already
	for _, newTag := range newTags {
		found := false
		for _, tag := range tags {
			if tag.Key == newTag.Key && equalNullString(tag.Value, newTag.Value) {
				found = true
			}
		}

		if !found {
			return false
		}
	}

	// check, if all old tags exists in new tag list
	for _, newTag := range tags {
		found := false
		for _, tag := range newTags {
			if tag.Key == newTag.Key && equalNullString(tag.Value, newTag.Value) {
				found = true
			}
		}

		if !found {
			return false
		}
	}

	// everything is equal
	return true
}

func (job *mediaAnalyzeJob) cleanUp() {
	// invalidate cache
	if job.cacheItem != nil {
		job.cacheItem.Unreference()
	}
}

func parseNullBoolByInt(val *int) sql.NullBool {
	if val == nil {
		return sql.NullBool{}
	}
	return sql.NullBool{
		Bool:  *val == 1,
		Valid: true,
	}
}

func parseNullFloatByString(val *string) (sql.NullFloat64, error) {
	if val == nil || *val == "N/A" {
		return sql.NullFloat64{}, nil
	}
	floatVal, err := strconv.ParseFloat(*val, 64)
	if err != nil {
		log.WithFields(log.Fields{
			"value":   val,
			"inherit": err,
		}).Error("unable to parse int value")
		return sql.NullFloat64{}, err
	}
	return sql.NullFloat64{
		Float64: floatVal,
		Valid:   true,
	}, nil
}

func parseNullInt(val *int) sql.NullInt64 {
	if val == nil {
		return sql.NullInt64{}
	}
	return sql.NullInt64{
		Int64: int64(*val),
		Valid: true,
	}
}

func parseNullIntByString(val *string) (sql.NullInt64, error) {
	if val == nil || *val == "N/A" {
		return sql.NullInt64{}, nil
	}
	intVal, err := strconv.Atoi(*val)
	if err != nil {
		log.WithFields(log.Fields{
			"value":   *val,
			"inherit": err,
		}).Error("unable to parse int value")
		return sql.NullInt64{}, err
	}
	return sql.NullInt64{
		Int64: int64(intVal),
		Valid: true,
	}, nil
}

func parseNullString(val *string) sql.NullString {
	if val == nil {
		return sql.NullString{}
	}
	return sql.NullString{
		String: *val,
		Valid:  true,
	}
}

func equalNullBool(b1, b2 sql.NullBool) bool {
	if !b1.Valid && !b2.Valid {
		return true
	} else if b1.Valid && b2.Valid && b1.Bool == b2.Bool {
		return true
	}
	return false
}

func equalNullFloat(i1, i2 sql.NullFloat64) bool {
	if !i1.Valid && !i2.Valid {
		return true
	} else if i1.Valid && i2.Valid && i1.Float64 == i2.Float64 {
		return true
	}
	return false
}

func equalNullInt(i1, i2 sql.NullInt64) bool {
	if !i1.Valid && !i2.Valid {
		return true
	} else if i1.Valid && i2.Valid && i1.Int64 == i2.Int64 {
		return true
	}
	return false
}

func equalNullString(s1, s2 sql.NullString) bool {
	if !s1.Valid && !s2.Valid {
		return true
	} else if s1.Valid && s2.Valid && s1.String == s2.String {
		return true
	}
	return false
}
