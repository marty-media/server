/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package backend

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/settings"
	"gitlab.com/marty-media/server/internal/storage/s3"
)

type Cache struct {
	directory string
	maxSize   uint64
	items     map[string]*CacheItem
}

type CacheItem struct {
	referenceCounter int
	LocalPath        string
	Size             int64
}

var CacheInstance = newCache()

func newCache() *Cache {
	return &Cache{
		items: make(map[string]*CacheItem),
	}
}

func (cache *Cache) Start() error {
	// find temporary folder
	outputDirectory := settings.Instance.GetStorageTempDirectory()

	// build output directory
	cache.directory = filepath.Join(outputDirectory, "cache")
	log.WithField("folder", cache.directory).Debug("use cache directory")

	// run clean up (always start empty)
	cache.cleanUp(true)

	// create new directory, if not already exists
	if err := os.MkdirAll(cache.directory, 0700); err != nil {
		log.WithField("inherit", err).Error("unable to create temporary directory for cache")
		return err
	}

	// load max cache size
	cache.maxSize = settings.Instance.GetStorageCacheSize()

	return nil
}

func (cache *Cache) Stop() {
	// run clean up
	cache.cleanUp(false)
}

func (cache *Cache) cleanUp(force bool) {
	// clean folder
	if force {
		err := os.RemoveAll(cache.directory)
		if err != nil {
			log.WithField("inherit", err).Error("unable to clean up cache folder")
			return
		}
		return
	}

	// remove unused files
	for key, cacheItem := range cache.items {
		if cacheItem.referenceCounter != 0 {
			continue
		}

		// remove file
		if err := os.Remove(cacheItem.LocalPath); err != nil {
			log.WithField("inherit", err).Error("unable to remove cache item")
			continue
		}

		delete(cache.items, key)
	}
}

func (cache *Cache) Size() int64 {
	var size int64 = 0
	for _, cacheItem := range cache.items {
		size += int64(cacheItem.Size)
	}

	return size
}

func (cache *Cache) GetS3MediaItem(mediaItem *database.MediaItem, media *database.Media, libraryLocation *database.LibraryLocation) (*CacheItem, error) {
	// load info from S3
	s3Instance := GetS3ByLocation(libraryLocation)
	path := filepath.Join(media.Path, mediaItem.File)
	s3HeadResponse, err := s3Instance.HeadObject(s3.HeadObjectRequest{
		Name: path,
	})
	if err != nil {
		return nil, err
	}

	// build cache ID
	cacheID := fmt.Sprintf("s3#%d#%s#%d", libraryLocation.ID, path, s3HeadResponse.ContentLength)

	// check, if file is already in cache
	if cacheItem, found := cache.items[cacheID]; found {
		cacheItem.referenceCounter++
		return cacheItem, nil
	}

	// check free cache size
	if cache.Size()+int64(s3HeadResponse.ContentLength) > int64(cache.maxSize) {
		cache.cleanUp(false)
	}

	// build cache file name
	cacheName := cache.createTmpFilePath("s3_media_item", mediaItem.File)

	// load file from S3 backend
	downloadResponse, err := s3Instance.DownloadObject(s3.DownloadObjectRequest{
		Name: path,
	})
	if err != nil {
		return nil, err
	}

	// move file to cache folder
	if err := os.Rename(downloadResponse.FileName, cacheName); err != nil {
		defer os.Remove(downloadResponse.FileName)
		log.WithField("inherit", err).Error("unable to move file to cache folder")
		return nil, err
	}

	// build cache item
	cacheItem := &CacheItem{
		referenceCounter: 1,
		LocalPath:        cacheName,
		Size:             int64(s3HeadResponse.ContentLength),
	}

	// add cache item
	cache.items[cacheID] = cacheItem

	// return result
	return cacheItem, nil
}

func (cache *Cache) createTmpFilePath(prefix string, fileName string) string {
	// get file extension from file name
	fileNameSplit := strings.Split(fileName, ".")
	fileExtension := fileNameSplit[len(fileNameSplit)-1]

	// build file path
	cacheFileName := fmt.Sprintf("%s_%d.%s", prefix, time.Now().UnixNano(), fileExtension)
	return filepath.Join(cache.directory, cacheFileName)
}

func (cacheItem *CacheItem) Unreference() {
	cacheItem.referenceCounter--
}
