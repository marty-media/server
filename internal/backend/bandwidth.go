/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package backend

import (
	"sync"
	"time"
)

type Bandwidth struct {
	statsLive *BandwidthInterval // store last 60 seconds
	statsHour *BandwidthInterval // store last 60 minutes
}

type BandwidthInterval struct {
	addOutgoing chan int64
	ticker      *time.Ticker
	stats       []int64
	statsLock   sync.Mutex
}

var BandwidthInstance = &Bandwidth{}

func (bw *Bandwidth) Start() error {
	bw.statsLive = NewBandwidthInterval(time.Second, 60)
	bw.statsHour = NewBandwidthInterval(time.Minute, 60)
	return nil
}

func (bw *Bandwidth) AddOutgoing(cnt int64) {
	bw.statsLive.addOutgoing <- cnt
	bw.statsHour.addOutgoing <- cnt
}

func (bw *Bandwidth) StatsLive() []int64 {
	return bw.statsLive.Stats()
}

func (bw *Bandwidth) StatsHour() []int64 {
	return bw.statsHour.Stats()
}

func NewBandwidthInterval(d time.Duration, size int) *BandwidthInterval {
	response := &BandwidthInterval{
		addOutgoing: make(chan int64, 100),
		ticker:      time.NewTicker(d),
		stats:       make([]int64, size),
	}
	go response.run()
	return response
}

func (bwInterval *BandwidthInterval) run() {
	for {
		select {
		case <-bwInterval.ticker.C:
			// rotate stats
			bwInterval.statsLock.Lock()
			bwInterval.stats = bwInterval.stats[1:]
			bwInterval.stats = append(bwInterval.stats, 0)
			bwInterval.statsLock.Unlock()
		case newBytes := <-bwInterval.addOutgoing:
			bwInterval.statsLock.Lock()
			bwInterval.stats[59] += newBytes
			bwInterval.statsLock.Unlock()
		}
	}
}

func (bwInterval *BandwidthInterval) Stats() []int64 {
	defer bwInterval.statsLock.Unlock()
	bwInterval.statsLock.Lock()
	return bwInterval.stats
}
