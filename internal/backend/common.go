/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package backend

import (
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/storage/s3"
)

func GetS3ByLocation(location *database.LibraryLocation) *s3.S3 {
	if !location.User.Valid || !location.Password.Valid {
		return nil
	}

	// s3 request
	request := s3.S3Request{
		Address:   location.Location,
		AccessKey: location.User.String,
		SecretKey: location.Password.String,
	}

	// check for a region
	if location.Region.Valid {
		request.Region = location.Region.String
	}

	return s3.NewS3(request)
}

type mediaFilePathResponse struct {
	Path      string
	CacheItem *CacheItem
}

func GetMediaItemFilePath(mediaItem *database.MediaItem, media *database.Media, libraryLocation *database.LibraryLocation) (*mediaFilePathResponse, bool) {
	switch libraryLocation.Type {
	case database.LibraryLocationTypeLocalFolder:
		return &mediaFilePathResponse{
			Path: filepath.Join(libraryLocation.Location, media.Path, mediaItem.File),
		}, true
	case database.LibraryLocationTypeS3:
		cacheItem, err := CacheInstance.GetS3MediaItem(mediaItem, media, libraryLocation)
		if err != nil {
			log.WithField("inherit", err).Error("unable to load media item from cache")
			return nil, false
		}
		return &mediaFilePathResponse{
			Path:      cacheItem.LocalPath,
			CacheItem: cacheItem,
		}, true
	default:
		log.WithField("libraryLocationType", libraryLocation.Type).Error("unsupported library location type")
		return nil, false
	}
}
