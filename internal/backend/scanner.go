/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package backend

import (
	"crypto/sha256"
	"database/sql"
	"fmt"
	"gitlab.com/marty-media/server/internal/common"
	"io"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/storage/s3"
)

const (
	ScannerVersion = 1
)

var Extensions = map[string]bool{
	"mkv": true,
	"mp4": true,
}

type Scanner struct {
	LocationScanJobs chan locationScan
}

var ScannerInstance = newScannerInstance()

type locationScan struct {
	libraryLocationID int64
	libraryLocation   *database.LibraryLocation
	library           *database.Library
}

func newScannerInstance() *Scanner {
	scanner := &Scanner{}

	// initialize channels
	scanner.LocationScanJobs = make(chan locationScan, 100)

	return scanner
}

func (scanner *Scanner) Start() error {
	// run background threads
	go scanner.runLocationScanJobs()
	return nil
}

func (scanner *Scanner) NewLocationScan(libraryLocationID int64) {
	scanner.LocationScanJobs <- locationScan{
		libraryLocationID: libraryLocationID,
	}
}

func (scanner *Scanner) runLocationScanJobs() {
	for {
		job := <-scanner.LocationScanJobs
		scanner.processLocationScanJob(&job)
	}
}

func (scanner *Scanner) processLocationScanJob(job *locationScan) {
	log.WithFields(log.Fields{
		"libraryLocationID": job.libraryLocationID,
	}).Info("start processing location scan")
	defer log.WithFields(log.Fields{
		"libraryLocationID": job.libraryLocationID,
	}).Info("finished processing location scan")

	// load library location
	libraryLocation, err := database.Instance.GetLibraryLocation(job.libraryLocationID)
	if err != nil {
		log.WithFields(log.Fields{
			"libraryLocationID": job.libraryLocationID,
			"inherit":           err,
		}).Error("library location not found")
		return
	}
	job.libraryLocation = libraryLocation

	// check, if library location was found
	if libraryLocation == nil {
		log.WithField("libraryLocationID", job.libraryLocationID).Warning("library location not found")
		return
	}

	// scan only source locations
	if libraryLocation.UsageType != database.LibraryLocationUsageTypeSourceMedia {
		log.WithField("libraryLocationID", job.libraryLocationID).Warning("skip scan of library location")
		return
	}

	// load library
	library, err := database.Instance.GetLibrary(libraryLocation.LibraryID)
	if err != nil {
		log.WithFields(log.Fields{
			"libraryID": libraryLocation.LibraryID,
			"inherit":   err,
		}).Error("library not found")
		return
	}
	job.library = library

	// scan location (folders and files)
	folders, success := scanner.scanLocation(job)
	if !success {
		return
	}

	// process files/folders
	switch job.library.Type {
	case database.LibraryTypeMovie:
		for _, folder := range folders {
			if !scanner.processMovieScan(job, folder) {
				return
			}
		}
	default:
		log.WithFields(log.Fields{
			"libraryID":   job.library.ID,
			"libraryType": job.library.Type,
		}).Error("invalid library type")
		return
	}

	// update database for last scanned entry
	if err := database.Instance.UpdateLibraryLocation(libraryLocation.ID, ScannerVersion, true, true); err != nil {
		log.WithFields(log.Fields{
			"libraryLocationID": libraryLocation.ID,
			"inherit":           err,
		}).Error("unable to update library location scanned timestamp")
		return
	}
}

type scanFolder struct {
	name  string
	files []scanFile
}

type scanFile interface {
	name() string
	size() int64
	checksumSha256() (sql.NullString, error)
}

type scanFileLocal struct {
	fileInfo fs.FileInfo
	folder   string
}

func (file scanFileLocal) name() string {
	return file.fileInfo.Name()
}

func (file scanFileLocal) size() int64 {
	return file.fileInfo.Size()
}

func (file scanFileLocal) checksumSha256() (sql.NullString, error) {
	fullFilePath := path.Join(file.folder, file.name())
	checksum, err := calculateChecksumSha256(fullFilePath)
	if err != nil {
		log.WithFields(log.Fields{
			"filePath": fullFilePath,
			"inherit":  err,
		}).Error("unable to calculate checksum")
		return sql.NullString{}, err
	}

	return sql.NullString{
		String: checksum,
		Valid:  true,
	}, nil
}

type scanFileS3 struct {
	_name    string
	fileInfo s3.ListBucketObject
}

func (file scanFileS3) name() string {
	return file._name
}

func (file scanFileS3) size() int64 {
	return int64(file.fileInfo.Size)
}

func (file scanFileS3) checksumSha256() (sql.NullString, error) {
	return sql.NullString{}, nil
}

func (scanner *Scanner) scanLocation(job *locationScan) ([]scanFolder, bool) {
	switch job.libraryLocation.Type {
	case database.LibraryLocationTypeLocalFolder:
		folders, _, success := scanner.scanLocationFolder(job, job.libraryLocation.Location)
		return folders, success
	case database.LibraryLocationTypeS3:
		folders, _, success := scanner.scanLocationS3(job)
		return folders, success
	default:
		log.WithFields(log.Fields{
			"libraryLocationID":   job.libraryLocation.ID,
			"libraryLocationType": job.libraryLocation.Type,
		}).Error("unknown library location type")
		return nil, false
	}
}

func (scanner *Scanner) scanLocationFolder(job *locationScan, folder string) ([]scanFolder, []scanFile, bool) {
	// read directory
	fileInfoList, err := os.ReadDir(folder)
	if err != nil {
		log.WithFields(log.Fields{
			"folder":  folder,
			"inherit": err,
		}).Error("unable to scan location")
		return nil, nil, false
	}

	// check each folder
	var responseFolders []scanFolder
	var responseFiles []scanFile
	for _, dirEntry := range fileInfoList {
		// check for folder
		if dirEntry.IsDir() {
			// build new folder object
			newFolder := scanFolder{
				name: dirEntry.Name(),
			}

			// parse files
			_, files, success := scanner.scanLocationFolder(job, filepath.Join(folder, dirEntry.Name()))
			if !success {
				return nil, nil, false
			}
			newFolder.files = files
			responseFolders = append(responseFolders, newFolder)
			continue
		}

		// retrieve file meta information
		fileInfo, err := dirEntry.Info()
		if err != nil {
			log.WithFields(log.Fields{
				"folder":   folder,
				"fileName": dirEntry.Name(),
				"inherit":  err,
			}).Error("unable to retrieve file information")
			return nil, nil, false
		}

		// process each file
		newFile := scanFileLocal{
			fileInfo: fileInfo,
			folder:   folder,
		}

		// add new folder to response
		responseFiles = append(responseFiles, newFile)
	}

	return responseFolders, responseFiles, true
}

func (scanner *Scanner) scanLocationS3(job *locationScan) ([]scanFolder, []scanFile, bool) {
	// get S3 connection
	s3Connection := GetS3ByLocation(job.libraryLocation)
	if s3Connection == nil {
		log.WithFields(log.Fields{
			"libraryLocationID": job.libraryLocation.ID,
		}).Error("unable to get S3 connection")
		return nil, nil, false
	}

	// load S3 bucket
	s3Result, err := s3Connection.ListAllObjects(s3.ListAllObjectsRequest{})
	if err != nil {
		log.WithFields(log.Fields{
			"libraryLocationID": job.libraryLocation.ID,
		}).Error("unable to list S3 bucket")
		return nil, nil, false
	}

	// build folders
	folders := map[string]scanFolder{}
	for _, s3Object := range s3Result.Content {
		// parse file parts
		fileParts := strings.Split(s3Object.Key, s3.DefaultDelimiter)
		if len(fileParts) != 2 {
			continue
		}

		// build structure
		folderName := fileParts[0]
		fileName := fileParts[1]
		if _, found := folders[folderName]; !found {
			// create folder, if not exists
			folders[folderName] = scanFolder{
				name: folderName,
			}
		}

		// add file to folder
		folder := folders[folderName]
		newFiles := append(folder.files, scanFileS3{
			_name:    fileName,
			fileInfo: s3Object,
		})
		folder.files = newFiles
		folders[folderName] = folder
	}

	// build response
	responseFolders := []scanFolder{}
	for _, folder := range folders {
		responseFolders = append(responseFolders, folder)
	}

	return responseFolders, nil, true
}

func (scanner *Scanner) processMovieScan(job *locationScan, folder scanFolder) bool {
	logFields := log.Fields{
		"libraryLocationID": job.libraryLocation.ID,
		"folder":            folder,
	}
	log.WithFields(logFields).Debug("start processing movie scan")
	defer log.WithFields(logFields).Debug("finished processing movie scan")

	// create new DB transaction
	transaction, err := database.Instance.Transaction()
	if err != nil {
		log.WithField("inherit", err).Error("unable to create new transaction")
		return false
	}
	defer transaction.Rollback()

	// load media (if exists)
	media, err := transaction.FindMediaByPath(job.libraryLocation.LibraryID, folder.name)
	if err != nil {
		log.WithFields(log.Fields{
			"libraryID": job.libraryLocation.LibraryID,
			"folder":    folder.name,
			"inherit":   err,
		}).Error("unable to find media by folder")
		return false
	}

	// create media, if not already exists
	var mediaID int64
	if media == nil {
		id, err := scanner.createNewMovie(job, folder.name, transaction)
		if err != nil {
			return false
		}
		mediaID = id
	} else {
		mediaID = media.ID
	}

	// scan each file
	fullFolderPath := filepath.Join(job.libraryLocation.Location, folder.name)
	processedItems := []int64{}
	for _, scanFile := range folder.files {
		// process each file
		mediaID, success := scanner.processMovieFile(job, mediaID, fullFolderPath, scanFile, transaction)
		if !success {
			// automatic rollback
			return false
		}

		processedItems = append(processedItems, mediaID)
	}

	// check for removed files
	if !scanner.checkForRemovedFiles(job, mediaID, processedItems, transaction) {
		return false
	}

	// everything went OK
	if err := transaction.Commit(); err != nil {
		log.WithField("inherit", err).Error("database commit failed")
		return false
	}

	return true
}

func (scanner *Scanner) createNewMovie(job *locationScan, folder string, transaction *database.Transaction) (int64, error) {
	uuid, err := common.RandomHex(common.RandomLengthMedia)
	if err != nil {
		return -1, err
	}
	id, err := transaction.CreateMedia(database.Media{
		LibraryID: job.libraryLocation.LibraryID,
		UID:       uuid,
		Path:      folder,
	})
	if err != nil {
		log.WithField("inherit", err).Error("unable to create new media entry")
		return -1, err
	}
	return id, nil
}

func (scanner *Scanner) processMovieFile(job *locationScan, mediaID int64, folder string, scanFile scanFile, transaction *database.Transaction) (int64, bool) {
	// check for supported file extension
	fileName := scanFile.name()
	ext := strings.ToLower(filepath.Ext(fileName))
	if ext == "" {
		log.WithFields(log.Fields{
			"scanFile": fileName,
		}).Info("skip file; missing file extension")
		return -1, true
	}
	ext = ext[1:]
	if _, found := Extensions[ext]; !found {
		log.WithFields(log.Fields{
			"ext":      ext,
			"scanFile": fileName,
		}).Info("skip file (unsupported file extension)")
		return -1, true
	}

	// load current media item
	mediaItem, err := transaction.FindLatestMediaItemByFile(mediaID, fileName)
	if err != nil {
		log.WithFields(log.Fields{
			"mediaID":  mediaID,
			"fileName": fileName,
			"inherit":  err,
		}).Error("unable to find media item")
		return -1, false
	}

	// check, if media item has changed
	fileSize := scanFile.size()
	if mediaItem != nil && mediaItem.Size == fileSize {
		// nothing changed
		return mediaItem.ID, true
	} else if mediaItem != nil {
		// update existing media entry (mark as outdated)
		if !scanner.disableMediaItem(mediaItem.ID, transaction) {
			return -1, false
		}
	}

	// calculate file checksum
	checksum, err := scanFile.checksumSha256()
	if err != nil {
		log.WithFields(log.Fields{
			"folder":   folder,
			"fileName": fileName,
			"inherit":  err,
		}).Error("unable to calculate checksum")
		return -1, false
	}

	// create new media item
	newMediaID, err := transaction.CreateMediaItem(database.MediaItem{
		MediaID:           mediaID,
		LibraryLocationID: job.libraryLocation.ID,
		File:              fileName,
		Latest:            true,
		Size:              fileSize,
		ChecksumSha256:    checksum,
	})
	if err != nil {
		log.WithFields(log.Fields{
			"folder":   folder,
			"fileName": fileName,
			"inherit":  err,
		}).Error("unable to create new media item")
		return -1, false
	}

	return newMediaID, true
}

func (scanner *Scanner) checkForRemovedFiles(job *locationScan, mediaID int64, processedMediaIDs []int64, transaction *database.Transaction) bool {
	// load media items for current media
	mediaItems, err := transaction.FindMediaItemsByMedia(mediaID)
	if err != nil {
		log.WithFields(log.Fields{
			"mediaID": mediaID,
			"inherit": err,
		}).Error("unable to find media items")
		return false
	}

	// build map for processed IDs
	processedIDs := make(map[int64]bool)
	for _, mediaID := range processedMediaIDs {
		processedIDs[mediaID] = true
	}

	for _, mediaItem := range mediaItems {
		if !mediaItem.Latest {
			// item is already disabled
			continue
		}

		if mediaItem.LibraryLocationID != job.libraryLocation.ID {
			// item is not for this location
			continue
		}

		// check, if media item has been processed
		_, itemProcessed := processedIDs[mediaItem.ID]

		// mark as outdated
		if !itemProcessed {
			if !scanner.disableMediaItem(mediaItem.ID, transaction) {
				return false
			}
		}
	}

	return true
}

func (scanner *Scanner) disableMediaItem(mediaID int64, transaction *database.Transaction) bool {
	if err := transaction.UpdateMediaItem(database.UpdateMediaItemRequest{
		ID:           mediaID,
		Latest:       false,
		UpdateLatest: true,
	}); err != nil {
		log.WithFields(log.Fields{
			"mediaID": mediaID,
			"inherit": err,
		})
		return false
	}
	return true
}

func calculateChecksumSha256(path string) (string, error) {
	// open file
	file, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer file.Close()

	// calculate hash
	hash := sha256.New()
	if _, err := io.Copy(hash, file); err != nil {
		return "", err
	}

	hashResult := hash.Sum(nil)
	return fmt.Sprintf("%x", hashResult), nil
}
