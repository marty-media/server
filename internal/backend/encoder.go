/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package backend

import (
	"bytes"
	"database/sql"
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/martinr92/gomp4"
	"gitlab.com/marty-media/server/internal/common"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/ffmpeg"
	"gitlab.com/marty-media/server/internal/hls"
	"gitlab.com/marty-media/server/internal/storage/s3"
	"math"
	"os"
	"path/filepath"
	"slices"
	"strconv"
	"strings"
	"time"
)

//go:embed encode_sets.json
var setsJSON string

var EncoderInstance = newEncoderInstance()

type Encoder struct {
	sets  EncodeSets
	timer *time.Ticker
}

type EncodeSets struct {
	Version int64            `json:"version"`
	Video   []EncodeSetVideo `json:"video"`
	Audio   []EncodeSetAudio `json:"audio"`
}

type EncodeSetVideoRange string

const (
	EncodeSetVideoRangeSDR EncodeSetVideoRange = "SDR"
	EncodeSetVideoRangeHLG EncodeSetVideoRange = "HLG"
	EncodeSetVideoRangePQ  EncodeSetVideoRange = "PQ"
)

const (
	EncodeSetAudioCodecAACLC = "aac_lc"
)

const (
	EncodeSetVideoCodecH264 = "avc1"
	EncodeSetVideoCodecH265 = "hevc"
)

const (
	hlsMainPlaylist              = "main.m3u8"
	hlsStreamPlaylist            = "stream.m3u8"
	hlsSegmentFolderPrefix       = "stream_"
	hlsInitName                  = "init.mp4"
	hlsTargetDuration      int64 = 6
)

type EncodeSetVideo struct {
	Version       int64                `json:"version"`
	Name          string               `json:"name"`
	Enabled       bool                 `json:"enabled"`
	Codec         string               `json:"codec"`
	MaxWidth      int                  `json:"max_width"`
	MaxHeight     int                  `json:"max_height"`
	TargetBitrate int                  `json:"target_bitrate"`
	StrictTarget  bool                 `json:"strict_target"`
	MaxFPS        *float64             `json:"max_fps"`
	Range         EncodeSetVideoRange  `json:"range"`
	ConditionSets []EncodeConditionSet `json:"condition_sets"`
}

type EncodeSetAudio struct {
	Version       int64                `json:"version"`
	Name          string               `json:"name"`
	Enabled       bool                 `json:"enabled"`
	Codec         string               `json:"codec"`
	TargetBitrate int                  `json:"target_bitrate"`
	Channels      int                  `json:"channels"`
	ConditionSets []EncodeConditionSet `json:"condition_sets"`
}

type EncodeConditionSet struct {
	Name       string            `json:"name"`
	Enabled    bool              `json:"enabled"`
	Conditions []EncodeCondition `json:"conditions"`
}

type EncodeCondition struct {
	Type   string `json:"type"`
	Value1 string `json:"value1"`
	Value2 string `json:"value2"`
}

type encodeJob struct {
	media                 *database.Media
	mediaItem             *database.MediaItem
	mediaFormat           *database.MediaFormat
	targetLibraryLocation *database.LibraryLocation
	cacheItem             *CacheItem
	filePath              string
	success               bool
	retry                 bool
}

type encodeSet struct {
	mediaStream      *database.MediaStream
	encodeSetVideo   *EncodeSetVideo
	encodeSetAudio   *EncodeSetAudio
	workingDirectory string
	dbEncodeSet      *database.EncodeSet
	dbEncodeSegments []*database.EncodeSegment
	dbMessages       []*database.MediaMessage
}

func newEncoderInstance() *Encoder {
	return &Encoder{
		timer: time.NewTicker(time.Minute),
	}
}

func (encoder *Encoder) Start() error {
	// parse encode sets
	var sets EncodeSets
	err := json.Unmarshal([]byte(setsJSON), &sets)
	if err != nil {
		log.WithField("inherit", err).Error("unable to parse encode sets")
		return err
	}
	encoder.sets = sets

	// run background threads
	go encoder.run()
	return nil
}

func (encoder *Encoder) run() {
	for {
		// run encode job
		job := encoder.findNext()
		if job != nil {
			encoder.runJob(job)
			continue
		}

		// wait until next timer tick
		<-encoder.timer.C
	}
}

func (encoder *Encoder) findNext() *encodeJob {
	// create new transaction (for select lock)
	transaction, err := database.Instance.Transaction()
	if err != nil {
		log.WithField("inherit", err).Error("unable to create new transaction")
		return nil
	}
	defer func() {
		if err := transaction.Rollback(); err != nil {
			log.WithField("inherit", err).Error("unable to rollback transaction")
		}
	}()

	// find next media item for encoding
	mediaItem, err := transaction.FindMediaItemToEncode(AnalyzerVersion, encoder.sets.Version)
	if err != nil {
		log.WithField("inherit", err).Error("unable to find next media item for encoding")
		return nil
	}

	// check for new encoding item
	if mediaItem == nil {
		return nil
	}
	log.WithFields(log.Fields{
		"mediaItemID": mediaItem.ID,
		"fileName":    mediaItem.File,
	}).Info("found media item for encoding")

	// update encoding status
	err = transaction.UpdateMediaItem(database.UpdateMediaItemRequest{
		ID:                  mediaItem.ID,
		EncodedStatus:       database.MediaItemEncodedStatusStarted,
		UpdateEncodedStatus: true,
		UpdateLastEncodedAt: true,
	})
	if err != nil {
		log.WithField("inherit", err).Error("unable to update media item")
		return nil
	}

	// commit changes
	if err := transaction.Commit(); err != nil {
		log.WithField("inherit", err).Error("unable to commit transaction")
		return nil
	}

	return &encodeJob{
		mediaItem: mediaItem,
	}
}

func (encoder *Encoder) runJob(job *encodeJob) {
	// clean up job
	defer encoder.cleanUpJob(job)

	// update encoding status (completed or failed)
	defer func() {
		updateRequest := database.UpdateMediaItemRequest{
			ID:                  job.mediaItem.ID,
			UpdateLastEncodedAt: true,
		}
		if job.retry {
			updateRequest.EncodedStatus = database.MediaItemEncodedStatusRetry
			updateRequest.UpdateEncodedStatus = true
		} else if job.success {
			updateRequest.EncodedStatus = database.MediaItemEncodedStatusCompleted
			updateRequest.UpdateEncodedStatus = true
			updateRequest.EncoderVersion = encoder.sets.Version
			updateRequest.UpdateEncoderVersion = true
		} else {
			updateRequest.EncodedStatus = database.MediaItemEncodedStatusFailed
			updateRequest.UpdateEncodedStatus = true
		}

		err := database.Instance.UpdateMediaItem(updateRequest)
		if err != nil {
			log.WithField("inherit", err).Error("unable to update media item")
		}
		log.WithFields(log.Fields{
			"mediaItemID":   job.mediaItem.ID,
			"fileName":      job.mediaItem.File,
			"encodedStatus": updateRequest.EncodedStatus,
		}).Info("finished media item encoding")
	}()

	// load media
	media, err := database.Instance.GetMedia(job.mediaItem.MediaID)
	if err != nil {
		log.WithField("inherit", err).Error("unable to load media")
		return
	}
	job.media = media

	// load media format
	mediaFormats, err := database.Instance.FindMediaFormatsByMediaItem(job.mediaItem.ID)
	if err != nil {
		log.WithField("inherit", err).Error("unable to load media format")
		return
	}
	for _, mediaFormat := range mediaFormats {
		if mediaFormat.Latest {
			job.mediaFormat = mediaFormat
			break
		}
	}
	if job.mediaFormat == nil {
		log.Error("unable to find current media format")
		return
	}

	// load location
	libraryLocation, err := database.Instance.GetLibraryLocation(job.mediaItem.LibraryLocationID)
	if err != nil {
		log.WithField("inherit", err).Error("unable to get library location")
		return
	}

	// find encoded location
	allLocations, err := database.Instance.GetLibraryLocations(libraryLocation.LibraryID)
	if err != nil {
		log.WithField("inherit", err).Error("unable to get library locations")
		return
	}
	encodedLocations := common.FilterSlice(allLocations, func(location *database.LibraryLocation) bool {
		return location.UsageType == database.LibraryLocationUsageTypeEncodedMedia
	})
	if len(encodedLocations) == 0 {
		log.Warn("no encoding location configured")
		job.retry = true
		return
	}
	job.targetLibraryLocation = encodedLocations[0]

	// load media item
	mediaItemPath, success := GetMediaItemFilePath(job.mediaItem, media, libraryLocation)
	if !success {
		return
	}
	job.cacheItem = mediaItemPath.CacheItem
	job.filePath = mediaItemPath.Path

	// load media streams
	mediaStreams, err := database.Instance.FindMediaStreamsByMediaItem(job.mediaItem.ID)
	if err != nil {
		log.WithField("inherit", err).Error("unable to load media streams")
		return
	}

	// run encode sets
	for _, mediaStream := range mediaStreams {
		// ignore outdated streams
		if !mediaStream.Latest {
			continue
		}
		if !encoder.processEncodeSets(job, mediaStream) {
			return
		}
	}

	job.success = true
}

func (encoder *Encoder) processEncodeSets(job *encodeJob, mediaStream *database.MediaStream) bool {
	// retrieve already processed encode sets by media stream
	encodeSets, err := database.Instance.FindEncodeSetsByMediaStream(mediaStream.ID)
	if err != nil {
		log.WithField("inherit", err).Error("unable to find processed encode sets")
		return false
	}
	processedEncodeSets := make(map[string]*database.EncodeSet)
	for _, encodeSet := range encodeSets {
		processedEncodeSets[encodeSet.Name] = encodeSet
	}

	// load media stream tags
	mediaStreamTags, err := database.Instance.FindMediaStreamTagsByMediaStream(mediaStream.ID)
	if err != nil {
		log.WithField("inherit", err).Error("unable to load media stream tags")
		return false
	}

	switch mediaStream.CodecType {
	case database.MediaStreamCodecTypeVideo:
		for _, encodeSetVideo := range encoder.sets.Video {
			if !encoder.processEncodeSetVideo(job, mediaStream, encodeSetVideo, processedEncodeSets[encodeSetVideo.Name]) {
				return false
			}
		}
	case database.MediaStreamCodecTypeAudio:
		for _, encodeSetAudio := range encoder.sets.Audio {
			if !encoder.processEncodeSetAudio(job, mediaStream, mediaStreamTags, encodeSetAudio, processedEncodeSets[encodeSetAudio.Name]) {
				return false
			}
		}
	}

	// everything is processed
	return true
}

func (encoder *Encoder) processEncodeSetVideo(job *encodeJob, mediaStream *database.MediaStream, encodeSetVideo EncodeSetVideo,
	processedEncodeSet *database.EncodeSet) bool {
	if encodeSetVideo.Skip(mediaStream) {
		log.WithFields(log.Fields{
			"mediaStreamID": mediaStream.ID,
			"name":          encodeSetVideo.Name,
		}).Debug("skip encode set")
		return true
	}

	// check, if this encode set is already processed
	if processedEncodeSet != nil && processedEncodeSet.Version == encodeSetVideo.Version &&
		processedEncodeSet.MediaFormatID == job.mediaFormat.ID {
		log.WithFields(log.Fields{
			"mediaStreamID": mediaStream.ID,
			"name":          encodeSetVideo.Name,
		}).Debug("encode set already processed")
		return true
	}

	// video range
	var videoRange database.EncodeSetVideoRange
	switch encodeSetVideo.Range {
	case EncodeSetVideoRangeSDR:
		videoRange = database.EncodeSetVideoRangeSDR
	case EncodeSetVideoRangeHLG:
		videoRange = database.EncodeSetVideoRangeHLG
	case EncodeSetVideoRangePQ:
		videoRange = database.EncodeSetVideoRangePQ
	default:
		log.WithField("range", encodeSetVideo.Range).Debug("unknown video range")
		return false
	}

	// start new set
	return encoder.runEncodeSet(job, &encodeSet{
		mediaStream:    mediaStream,
		encodeSetVideo: &encodeSetVideo,
		dbEncodeSet: &database.EncodeSet{
			MediaFormatID:           job.mediaFormat.ID,
			MediaStreamID:           mediaStream.ID,
			MediaID:                 job.mediaItem.MediaID,
			StoredLibraryLocationID: job.targetLibraryLocation.ID,
			MediaType:               database.EncodeSetMediaTypeMovie,
			Name:                    encodeSetVideo.Name,
			Version:                 encodeSetVideo.Version,
			Codec:                   encodeSetVideo.Codec,
			VideoRange:              sql.NullInt64{Int64: int64(videoRange), Valid: true},
			TargetDuration:          sql.NullInt64{Int64: hlsTargetDuration, Valid: true},
			IndependentSegments:     true,
		},
		dbEncodeSegments: []*database.EncodeSegment{},
		dbMessages:       []*database.MediaMessage{},
	})
}

func (encoder *Encoder) processEncodeSetAudio(job *encodeJob, mediaStream *database.MediaStream,
	mediaStreamTags []*database.MediaStreamTag, encodeSetAudio EncodeSetAudio, processedEncodeSet *database.EncodeSet) bool {
	if encodeSetAudio.Skip(mediaStream) {
		log.WithFields(log.Fields{
			"mediaStreamID": mediaStream.ID,
			"name":          encodeSetAudio.Name,
		}).Debug("skip encode set")
		return true
	}

	// check, if this encode set is already processed
	if processedEncodeSet != nil && processedEncodeSet.Version == encodeSetAudio.Version &&
		processedEncodeSet.MediaFormatID == job.mediaFormat.ID {
		log.WithFields(log.Fields{
			"mediaStreamID": mediaStream.ID,
			"name":          encodeSetAudio.Name,
		}).Debug("encode set already processed")
		return true
	}

	// calculate full codec
	fullCodec, err := encodeSetAudio.FullCodec()
	if err != nil {
		log.WithField("inherit", err).Error("unable to get full codec")
		return false
	}

	// detect language
	language, err := encoder.detectLanguage(mediaStreamTags)
	if err != nil {
		log.WithField("inherit", err).Error("unable to detect language")
		return false
	}

	// start new set
	return encoder.runEncodeSet(job, &encodeSet{
		mediaStream:    mediaStream,
		encodeSetAudio: &encodeSetAudio,
		dbEncodeSet: &database.EncodeSet{
			MediaFormatID:           job.mediaFormat.ID,
			MediaStreamID:           mediaStream.ID,
			MediaID:                 job.mediaItem.MediaID,
			StoredLibraryLocationID: job.targetLibraryLocation.ID,
			MediaType:               database.EncodeSetMediaTypeAudio,
			Name:                    encodeSetAudio.Name,
			Version:                 encodeSetAudio.Version,
			Codec:                   encodeSetAudio.Codec,
			FullCodec:               fullCodec,
			TargetDuration:          sql.NullInt64{Int64: hlsTargetDuration, Valid: true},
			Channels:                sql.NullInt64{Int64: int64(encodeSetAudio.Channels), Valid: true},
			Language:                sql.NullString{String: language, Valid: true},
		},
		dbEncodeSegments: []*database.EncodeSegment{},
		dbMessages:       []*database.MediaMessage{},
	})
}

func (encoder *Encoder) detectLanguage(mediaStreamTags []*database.MediaStreamTag) (string, error) {
	for _, tag := range mediaStreamTags {
		if strings.ToLower(tag.Key) != "language" {
			continue
		}

		// find language by key
		if !tag.Value.Valid {
			continue
		}

		// ffmpeg uses ISO639_2; so check for a matching ISO language
		isoCode, found := common.ISO639_2[strings.ToLower(tag.Value.String)]
		if !found {
			continue
		}

		return isoCode.ISO639_3, nil
	}

	return "", errors.New("unable to detect language")
}

func (encoder *Encoder) filterComplexToString(filterComplex []string, streamIndex int64) string {
	filterComplexString := ""
	for i, filter := range filterComplex {
		if i == 0 {
			filterComplexString += fmt.Sprintf("[0:%d]", streamIndex)
		} else {
			filterComplexString += fmt.Sprintf("[part_%d]", i-1)
		}
		filterComplexString += filter
		if i == len(filterComplex)-1 {
			filterComplexString += "[out]"
		} else {
			filterComplexString += fmt.Sprintf("[part_%d];", i)
		}
	}

	return filterComplexString
}

func (encoder *Encoder) runEncodeSet(job *encodeJob, encodeSet *encodeSet) bool {
	// clean up encode set
	defer encoder.cleanUpSet(encodeSet)

	// run encoding
	if !encoder.runEncode(job, encodeSet) {
		return false
	}

	// parse segment playlist
	segmentHLS := encoder.parseHLSPlaylist(job, encodeSet, true,
		filepath.Join(hlsSegmentFolderPrefix+"0", hlsStreamPlaylist),
		[]string{hls.TagNameM3U, hls.TagNameVersion, hls.TagNameTargetDuration, hls.TagNameMediaSequence,
			hls.TagNamePlaylistType, hls.TagNameIndependentSegments, hls.TagNameMap, hls.TagNameSegment,
			hls.TagNameEndList})
	if segmentHLS == nil {
		return false
	}

	if encodeSet.encodeSetVideo != nil {
		// extract resolution
		initFileName, ok := encoder.extractInitSegment(segmentHLS)
		if !ok {
			return false
		}
		baseDir := filepath.Dir(segmentHLS.File)
		if !encoder.parseResolution(job, encodeSet, filepath.Join(baseDir, initFileName)) {
			return false
		}
	}

	// validate duration: the sum should match the file duration
	duration := 0.0
	for _, tag := range segmentHLS.Tags {
		if tag.Name != hls.TagNameSegment {
			continue
		}

		// extract duration
		if len(tag.Attributes) == 0 {
			log.Warn("missing attributes of hls segment for duration calculation")
			continue
		}
		currentDuration, err := strconv.ParseFloat(tag.Attributes[0].Value, 64)
		if err != nil {
			log.WithFields(log.Fields{
				"tag":      tag.Name,
				"value":    tag.Value,
				"duration": tag.Attributes[0].Value,
			}).Error("unable to parse segment duration")
			continue
		}
		duration += currentDuration
	}
	log.WithFields(log.Fields{
		"duration": duration,
	}).Debug("duration calculation")

	// check stream duration (allow half second offset)
	if !job.mediaFormat.Duration.Valid || duration < job.mediaFormat.Duration.Float64-0.5 ||
		duration > job.mediaFormat.Duration.Float64+0.5 {
		log.WithFields(log.Fields{
			"duration": job.mediaFormat.Duration,
		}).Error("invalid media duration")
		return false
	}

	// upload encoding result
	if !encoder.uploadHLS(job, encodeSet, segmentHLS) {
		return false
	}

	// insert encoding set to database
	if !encoder.storeDatabase(job, encodeSet) {
		return false
	}

	return true
}

func (encoder *Encoder) runEncode(job *encodeJob, encodeSet *encodeSet) bool {
	switch encodeSet.mediaStream.CodecType {
	case database.MediaStreamCodecTypeVideo:
		return encoder.runEncodeVideo(job, encodeSet)
	case database.MediaStreamCodecTypeAudio:
		return encoder.runEncodeAudio(job, encodeSet)
	}

	return false
}

func (encoder *Encoder) runEncodeVideo(job *encodeJob, encodeSet *encodeSet) bool {
	// prepare encoding arguments
	encodeArgs, err := encoder.buildEncodeArgsVideo(job, encodeSet, 1)
	if err != nil {
		log.WithField("inherit", err).Error("unable to prepare video encode set")
		return false
	}

	// run 1st pass
	arguments := encoder.buildFFmpeg(job, encodeSet, encodeArgs, 1)
	ff1, err := ffmpeg.Encode(arguments, "")
	if err != nil {
		log.WithField("inherit", err).Error("unable to prepare 1st pass")
		return false
	}
	encodeSet.workingDirectory = ff1.Directory
	if err := ff1.Run(); err != nil {
		log.WithField("inherit", err).Error("unable to run 1st pass")
		return false
	}

	// run 2nd pass
	encodeArgs, _ = encoder.buildEncodeArgsVideo(job, encodeSet, 2)
	arguments = encoder.buildFFmpeg(job, encodeSet, encodeArgs, 2)
	ff2, err := ffmpeg.Encode(arguments, ff1.Directory)
	if err != nil {
		log.WithField("inherit", err).Error("unable to prepare 2st pass")
		return false
	}
	if err := ff2.Run(); err != nil {
		log.WithField("inherit", err).Error("unable to run 2st pass")
		return false
	}

	// parse main playlist after encoding
	mainPlaylist := encoder.parseHLSPlaylist(job, encodeSet, false, hlsMainPlaylist,
		[]string{hls.TagNameM3U, hls.TagNameVersion, hls.TagNameStream})
	if mainPlaylist == nil {
		return false
	}

	// find stream tag
	streamTag := mainPlaylist.FindTagByName(hls.TagNameStream)
	if streamTag == nil {
		log.Error("missing stream tag after encoding")
		return false
	}

	// set resolution: resolution that ffmpeg writes into the HLS playlist is sometimes incorrect
	// this seems to be related to the downscaling with division by 2 or 4
	// parse the correct resolution from the init.mp4 file

	// set full codec
	codecAttribute := streamTag.AttributeByName(hls.AttributeCodecs)
	if codecAttribute == nil || codecAttribute.Value == "" {
		log.Error("missing stream tag codec attribute")
		return false
	}
	encodeSet.dbEncodeSet.FullCodec = codecAttribute.Value

	return true
}

func (encoder *Encoder) parseResolution(job *encodeJob, encodeSet *encodeSet, fileName string) bool {
	// read init file
	initData, err := os.ReadFile(fileName)
	if err != nil {
		log.WithField("inherit", err).Error("unable to read file")
		return false
	}

	// parse init file
	dataReader := bytes.NewReader(initData)
	mp4Parser := gomp4.NewParser(dataReader)
	mp4Parser.IgnoreUnknownBoxes = true
	if err := mp4Parser.Parse(); err != nil {
		log.WithFields(log.Fields{
			"inherit":  err,
			"fileName": fileName,
		}).Error("unable to parse mp4")
		return false
	}

	// extract resolution
	resolutionFound := false
	for _, box := range mp4Parser.Content {
		movieBox, ok := box.(*gomp4.MovieBox)
		if !ok {
			continue
		}

		// found movie box, search for track
		for _, box2 := range movieBox.ChildBoxes {
			trackBox, ok := box2.(*gomp4.TrackBox)
			if !ok {
				continue
			}

			for _, box3 := range trackBox.ChildBoxes {
				trackHeaderBox, ok := box3.(*gomp4.TrackHeaderBox)
				if !ok {
					continue
				}

				// extract resolution
				height := trackHeaderBox.Height.ToFloat32()
				width := trackHeaderBox.Width.ToFloat32()
				encodeSet.dbEncodeSet.Height = sql.NullInt64{Int64: int64(math.Round(float64(height))), Valid: true}
				encodeSet.dbEncodeSet.Width = sql.NullInt64{Int64: int64(math.Round(float64(width))), Valid: true}
				resolutionFound = true
				break
			}
		}
	}

	if !resolutionFound {
		log.WithField("inherit", fileName).Error("unable to find resolution")
		return false
	}

	return true
}

func (encoder *Encoder) runEncodeAudio(job *encodeJob, encodeSet *encodeSet) bool {
	encodeArgs, err := encoder.buildEncodeArgsAudio(job, encodeSet)
	if err != nil {
		log.WithField("inherit", err).Error("unable to prepare audio encode set")
		return false
	}

	// run encoding
	arguments := encoder.buildFFmpeg(job, encodeSet, encodeArgs, 0)
	ff, err := ffmpeg.Encode(arguments, "")
	if err != nil {
		log.WithField("inherit", err).Error("unable to prepare audio encoding")
		return false
	}
	encodeSet.workingDirectory = ff.Directory
	if err := ff.Run(); err != nil {
		log.WithField("inherit", err).Error("unable to run audio encoding")
		return false
	}

	return true
}

func (encoder *Encoder) buildEncodeArgsVideo(job *encodeJob, encodeSet *encodeSet, pass int) ([]string, error) {
	// closed captions are currently not supported
	if encodeSet.mediaStream.ClosedCaptions.Valid && encodeSet.mediaStream.ClosedCaptions.Bool {
		return nil, fmt.Errorf("closed captions are not supported for video encoding")
	}

	// pixel format conversion is currently not supported
	if !encodeSet.mediaStream.PixelFormat.Valid || encodeSet.mediaStream.PixelFormat.String != "yuv420p" {
		return nil, fmt.Errorf("pixel format conversion is not supported for video encoding: %s != yuv420p", encodeSet.mediaStream.PixelFormat.String)
	}

	// currently only field order progressive supported (de-interlacing might be supported in the future)
	if !encodeSet.mediaStream.FieldOrder.Valid || encodeSet.mediaStream.FieldOrder.String != "progressive" {
		return nil, fmt.Errorf("deinterlacing is not supported for video encoding: %s != progressive", encodeSet.mediaStream.FieldOrder.String)
	}

	// currently only 8 bit is supported
	if !encodeSet.mediaStream.BitsPerRawSample.Valid || encodeSet.mediaStream.BitsPerRawSample.Int64 != 8 {
		return nil, fmt.Errorf("unsupported bits per raw sample for video encoding: %d != 8", encodeSet.mediaStream.BitsPerRawSample.Int64)
	}

	// TODO: reject unsupported side data

	// build FFmpeg command
	arguments := []string{}
	postFilterArguments := []string{}
	filterComplex := []string{}
	x265Params := []string{}

	// add logging
	arguments = append(arguments, "-loglevel", "level+verbose")

	// define source codec
	if encodeSet.mediaStream.CodecName == database.MediaStreamCodecNameVideoH264 {
		arguments = append(arguments, fmt.Sprintf("-c:%d", encodeSet.mediaStream.Index), "h264")
	} else {
		return nil, fmt.Errorf("unsupported media stream codec %s", encodeSet.mediaStream.CodecName)
	}

	// add input source
	arguments = append(arguments, "-i", job.filePath)

	// convert to output color range/space/primaries and resolution
	zscaleFilter, err := encoder.buildEncodeVideoZscaleFilter(job, encodeSet)
	if err != nil {
		return nil, err
	}
	filterComplex = append(filterComplex, zscaleFilter)

	// check framerate
	fps, newFilter, err := encoder.buildEncodeVideoFramerate(job, encodeSet)
	if err != nil {
		return nil, err
	}
	filterComplex = append(filterComplex, newFilter...)

	// force keyframe every two second
	// 1.13: Key frames (IDRs) SHOULD be present every two seconds.
	roundedKeyframesCounter := int(math.Round(fps)) * 2
	switch encodeSet.encodeSetVideo.Codec {
	case EncodeSetVideoCodecH264:
		postFilterArguments = append(postFilterArguments, "-g", fmt.Sprintf("%d", roundedKeyframesCounter))

		// disable scene detection
		postFilterArguments = append(postFilterArguments, "-sc_threshold", "0")
	case EncodeSetVideoCodecH265:
		x265Params = append(x265Params, fmt.Sprintf("keyint=%d", roundedKeyframesCounter))
		x265Params = append(x265Params, fmt.Sprintf("min-keyint=%d", roundedKeyframesCounter))
	}

	// TODO: remove side data (e.g. DOVI)
	// filterComplex = append(filterComplex, "sidedata=mode=delete")

	// build filter complex
	if len(filterComplex) > 0 {
		arguments = append(arguments, "-filter_complex", encoder.filterComplexToString(filterComplex, encodeSet.mediaStream.Index))
		arguments = append(arguments, "-map", "[out]")
	} else {
		arguments = append(arguments, "-map", fmt.Sprintf("0:%d", encodeSet.mediaStream.Index))
	}

	// add post-filter arguments
	arguments = append(arguments, postFilterArguments...)

	// add encoding info
	switch encodeSet.encodeSetVideo.Codec {
	case EncodeSetVideoCodecH264:
		arguments = append(arguments, "-c", "libx264")

		// 1.4: For H.264, you SHOULD use High Profile in preference to Main or Baseline Profile.
		arguments = append(arguments, "-profile:v", "high")
	case EncodeSetVideoCodecH265:
		// 1.10: You SHOULD use video formats in which the parameter sets are stored in the sample descriptions, rather than the samples.
		// (That is, use `'avc1'`, `'hvc1'`, or `'dvh1'` rather than `'avc3'`, `'hev1'`, or `'dvhe'`.)
		// --> FFmpeg uses hev1 as default for HEVC (instead of hvc1)
		arguments = append(arguments, "-c", "libx265", "-tag:v", "hvc1")
	default:
		return nil, fmt.Errorf("unsupported video codec: %s", encodeSet.encodeSetVideo.Codec)
	}
	arguments = append(arguments, "-b:v", fmt.Sprintf("%d", encodeSet.encodeSetVideo.TargetBitrate))
	// Apple HTTP Live Streaming (HLS) Authoring Specification for Apple devices appendixes
	// Measured peak bit rate versus measured average bit rate:
	// < 200% of measured average
	// we use 120% is max and 150% for the buffer
	maxBitrate := encodeSet.encodeSetVideo.TargetBitrate * 120 / 100
	bufferSize := encodeSet.encodeSetVideo.TargetBitrate * 150 / 100

	// lower the variable bitrate to achieve the strict lower mobile limit
	// 9.21 Multivariant Playlists that are delivered over cellular networks MUST contain a variant whose peak `BANDWIDTH` is less than or equal to 192 kbit/s.
	if encodeSet.encodeSetVideo.StrictTarget {
		maxBitrate = encodeSet.encodeSetVideo.TargetBitrate
		bufferSize = encodeSet.encodeSetVideo.TargetBitrate / 2
	}
	arguments = append(arguments, "-maxrate:v", fmt.Sprintf("%d", maxBitrate))
	arguments = append(arguments, "-bufsize:v", fmt.Sprintf("%d", bufferSize))

	// remove metadata
	arguments = append(arguments, "-map_metadata", "-1")
	arguments = append(arguments, "-map_metadata:s:0", "-1")
	arguments = append(arguments, "-map_chapters", "-1")

	// add pass
	if pass > 0 {
		switch encodeSet.encodeSetVideo.Codec {
		case EncodeSetVideoCodecH264:
			arguments = append(arguments, "-pass", fmt.Sprintf("%d", pass))
		case EncodeSetVideoCodecH265:
			x265Params = append(x265Params, fmt.Sprintf("pass=%d", pass))
		}
	}

	// add encoder parameters
	if len(x265Params) > 0 {
		arguments = append(arguments, "-x265-params", strings.Join(x265Params, ":"))
	}

	return arguments, nil
}

func (encoder *Encoder) buildEncodeVideoFramerate(job *encodeJob, encodeSet *encodeSet) (float64, []string, error) {
	var filterComplex []string

	// calculate framerate float
	fps, fpsValid := encodeSet.mediaStream.GetRealFrameRate()
	if !fpsValid {
		return 0, filterComplex, fmt.Errorf("unable to get real frame rate")
	}

	// framerate change is currently not supported
	if encodeSet.encodeSetVideo.MaxFPS != nil && *encodeSet.encodeSetVideo.MaxFPS < fps {
		return 0, filterComplex, fmt.Errorf(
			"framerate change is not supported for video encoding: %f > %f",
			*encodeSet.encodeSetVideo.MaxFPS,
			fps,
		)
	}

	// add output framerate
	realFrameRate := encodeSet.mediaStream.RealFrameRate
	if !realFrameRate.Valid {
		return 0, filterComplex, fmt.Errorf("unable to get real frame rate")
	}
	switch realFrameRate.String {
	case "24000/1001":
		filterComplex = append(filterComplex, "fps=fps=ntsc_film")
		encodeSet.dbEncodeSet.FrameRate = sql.NullInt64{Int64: int64(database.EncodeSetFrameRateMovieNTSC), Valid: true}
	default:
		return 0, filterComplex, fmt.Errorf("unsupported video frame rate: %s", realFrameRate.String)
	}

	return fps, filterComplex, nil
}

func (encoder *Encoder) buildEncodeVideoZscaleFilter(job *encodeJob, encodeSet *encodeSet) (string, error) {
	var zscaleInput string

	// check input
	if !encodeSet.mediaStream.ColorPrimaries.Valid {
		// add a warning, since we're just expecting color primaries, space and transfer
		encodeSet.dbMessages = append(encodeSet.dbMessages, &database.MediaMessage{
			MessageID:     database.MediaMessageIDUnknownColorPrimaries,
			MediaID:       job.mediaItem.MediaID,
			MediaItemID:   sql.NullInt64{Int64: job.mediaItem.ID, Valid: true},
			MediaStreamID: sql.NullInt64{Int64: encodeSet.mediaStream.ID, Valid: true},
		})
		zscaleInput += ":primariesin=709"
	}
	if !encodeSet.mediaStream.ColorSpace.Valid {
		// add a warning, since we're just expecting color space
		encodeSet.dbMessages = append(encodeSet.dbMessages, &database.MediaMessage{
			MessageID:     database.MediaMessageIDUnknownColorSpace,
			MediaID:       job.mediaItem.MediaID,
			MediaItemID:   sql.NullInt64{Int64: job.mediaItem.ID, Valid: true},
			MediaStreamID: sql.NullInt64{Int64: encodeSet.mediaStream.ID, Valid: true},
		})
		zscaleInput += ":matrixin=709"
	}
	var colorTransferMatrix string
	if !encodeSet.mediaStream.ColorTransferMatrix.Valid {
		// add a warning, since we're just expecting color transfer
		encodeSet.dbMessages = append(encodeSet.dbMessages, &database.MediaMessage{
			MessageID:     database.MediaMessageIDUnknownTransferMatrix,
			MediaID:       job.mediaItem.MediaID,
			MediaItemID:   sql.NullInt64{Int64: job.mediaItem.ID, Valid: true},
			MediaStreamID: sql.NullInt64{Int64: encodeSet.mediaStream.ID, Valid: true},
		})
		zscaleInput += ":transferin=709"
	} else {
		colorTransferMatrix = encodeSet.mediaStream.ColorTransferMatrix.String
	}

	// check for tonemapping
	var zscale string
	if encodeSet.encodeSetVideo.Range == EncodeSetVideoRangeSDR &&
		(colorTransferMatrix == "arib-std-b67" || colorTransferMatrix == "smpte2084") {
		// HDR (HLG / PQ) -> SDR: add tone mapping
		zscale = "zscale=t=linear" + zscaleInput + ",tonemap=mobius"
		zscaleInput = ""
	} else if encodeSet.encodeSetVideo.Range == EncodeSetVideoRangeSDR {
		// SDR -> SDR
		// nothing special required
	} else {
		return "", fmt.Errorf("unable to find sutable video conversion")
	}

	// use scale for resolution scaling
	var scaler string
	if !encodeSet.mediaStream.Width.Valid || !encodeSet.mediaStream.Height.Valid ||
		int64(encodeSet.encodeSetVideo.MaxWidth) < encodeSet.mediaStream.Width.Int64 ||
		int64(encodeSet.encodeSetVideo.MaxHeight) < encodeSet.mediaStream.Height.Int64 {
		// add downscaling
		scaler += "scale=flags=lanczos"
		scaler += fmt.Sprintf(":width=%d:height=%d", encodeSet.encodeSetVideo.MaxWidth, encodeSet.encodeSetVideo.MaxHeight)
		scaler += ":force_original_aspect_ratio=decrease:force_divisible_by=2"

		// set downscale flag to database
		encodeSet.dbEncodeSet.Downscale = sql.NullBool{Bool: true, Valid: true}
	}

	// combine final filter
	if zscale != "" {
		zscale += ","
	}
	if scaler != "" {
		zscale += scaler + ","
	}
	if encodeSet.encodeSetVideo.Range == EncodeSetVideoRangeSDR {
		zscale += "zscale=range=limited:primaries=709:transfer=709:matrix=709" + zscaleInput + ",format=yuv420p"
		return zscale, nil
	} else if encodeSet.encodeSetVideo.Range == EncodeSetVideoRangePQ {
		zscale += "zscale=range=limited:primaries=2020:transfer=smpte2084:matrix=2020_ncl" + zscaleInput + ",format=yuv420p10"
		return zscale, nil
	}

	// TODO: add video range HLG
	return "", fmt.Errorf("unsupported video range: %s", encodeSet.encodeSetVideo.Range)
}

func (encoder *Encoder) buildEncodeArgsAudio(job *encodeJob, encodeSet *encodeSet) ([]string, error) {
	// check sample rate
	if !encodeSet.mediaStream.SampleRate.Valid || encodeSet.mediaStream.SampleRate.Int64 != 48000 {
		return nil, fmt.Errorf("unsupported sample rate: %d != 48000", encodeSet.mediaStream.SampleRate.Int64)
	}

	// check audio channels
	if !encodeSet.mediaStream.ChannelLayout.Valid ||
		(encodeSet.mediaStream.ChannelLayout.String != "stereo" &&
			encodeSet.mediaStream.ChannelLayout.String != "5.1" &&
			encodeSet.mediaStream.ChannelLayout.String != "5.1(side)") {
		return nil, fmt.Errorf("unsupported audio channel layout: %s", encodeSet.mediaStream.ChannelLayout.String)
	}

	// build FFmpeg command
	arguments := []string{}
	filterComplex := []string{}

	// add logging
	arguments = append(arguments, "-loglevel", "level+verbose")

	// check source codec
	inputArguments, err := encoder.buildEncodeArgsAudioInput(job, encodeSet)
	if err != nil {
		return nil, err
	}
	arguments = append(arguments, inputArguments...)

	// add input source
	arguments = append(arguments, "-i", job.filePath)

	// downmix to stereo
	if encodeSet.encodeSetAudio.Channels == 2 && encodeSet.mediaStream.ChannelLayout.String == "5.1" {
		filterComplex = append(filterComplex, "pan=stereo|FL = 1.0*FL + 0.707*FC + 0.707*BL|FR = 1.0*FR + 0.707*FC + 0.707*BR")
		encodeSet.dbEncodeSet.Downmix = sql.NullBool{Bool: true, Valid: true}
	} else if encodeSet.encodeSetAudio.Channels == 2 && encodeSet.mediaStream.ChannelLayout.String == "5.1(side)" {
		filterComplex = append(filterComplex, "pan=stereo|FL = 1.0*FL + 0.707*FC + 0.707*SL|FR = 1.0*FR + 0.707*FC + 0.707*SR")
		encodeSet.dbEncodeSet.Downmix = sql.NullBool{Bool: true, Valid: true}
	}

	// build filter complex
	if len(filterComplex) > 0 {
		arguments = append(arguments, "-filter_complex", encoder.filterComplexToString(filterComplex, encodeSet.mediaStream.Index))
		arguments = append(arguments, "-map", "[out]")
	} else {
		arguments = append(arguments, "-map", fmt.Sprintf("0:%d", encodeSet.mediaStream.Index))
	}

	// add encoder info
	switch encodeSet.encodeSetAudio.Codec {
	case EncodeSetAudioCodecAACLC:
		arguments = append(arguments, "-c:a", "aac", "-profile:a", "aac_low", "-sample_fmt", "fltp")
	default:
		return nil, fmt.Errorf("unknown codec %s", encodeSet.encodeSetAudio.Codec)
	}
	arguments = append(arguments, "-b:a", fmt.Sprintf("%d", encodeSet.encodeSetAudio.TargetBitrate))

	// remove metadata
	arguments = append(arguments, "-map_metadata", "-1")
	arguments = append(arguments, "-map_metadata:s:0", "-1")
	arguments = append(arguments, "-map_chapters", "-1")

	return arguments, nil
}

func (encoder *Encoder) buildEncodeArgsAudioInput(job *encodeJob, encodeSet *encodeSet) (arguments []string, err error) {
	inputIndex := fmt.Sprintf("-c:%d", encodeSet.mediaStream.Index)
	if encodeSet.mediaStream.CodecName == database.MediaStreamCodecNameAudioAAC &&
		encodeSet.mediaStream.Profile.Valid && encodeSet.mediaStream.Profile.String == database.MediaStreamProfileAACLC {
		arguments = append(arguments, inputIndex, "aac")
	} else if encodeSet.mediaStream.CodecName == database.MediaStreamCodecNameAudioAC3 {
		arguments = append(arguments, inputIndex, "ac3")
	} else if encodeSet.mediaStream.CodecName == database.MediaStreamCodecNameAudioTrueHD {
		arguments = append(arguments, inputIndex, "truehd")
	} else {
		return nil, fmt.Errorf("unsupported audio codec: %s", encodeSet.mediaStream.CodecName)
	}

	return
}

func (encoder *Encoder) buildFFmpeg(job *encodeJob, encodeSet *encodeSet, encodeArguments []string, pass int) []string {
	// build command
	arguments := []string{}

	// append stream specific arguments
	arguments = append(arguments, encodeArguments...)

	if pass == 1 {
		// skip output
		arguments = append(arguments, "-f", "null", "/dev/null")
	} else {
		// HLS output
		arguments = append(arguments, "-f", "hls")

		// 7.5. Target durations SHOULD be 6 seconds.
		arguments = append(arguments, "-hls_time", fmt.Sprintf("%d", hlsTargetDuration))

		// 7.4 Video segments MUST start with an IDR frame.
		arguments = append(arguments, "-hls_flags", "independent_segments")

		// other HLS output flags
		arguments = append(arguments, "-hls_playlist_type", "vod", "-hls_segment_type", "fmp4")
		arguments = append(arguments, "-master_pl_name", hlsMainPlaylist, "-hls_segment_filename", hlsSegmentFolderPrefix+"%v/data_%06d.m4s")
		arguments = append(arguments, "-hls_fmp4_init_filename", hlsInitName, hlsSegmentFolderPrefix+"%v/"+hlsStreamPlaylist)
	}

	return arguments
}

func (encoder *Encoder) parseHLSPlaylist(job *encodeJob, encodeSet *encodeSet, checkEnd bool, playlist string, allowedTags []string) *hls.Parser {
	// parse HLS file
	m3u8Path := filepath.Join(encodeSet.workingDirectory, playlist)
	parser := hls.NewParser(m3u8Path)
	if !parser.Parse() {
		return nil
	}

	// check, if HLS encoding was successfully
	if !parser.HasStart() {
		log.Error("invalid HLS content after encoding")
		return nil
	}

	// check, if HLS encoding was successfully completed
	if checkEnd && !parser.HasEnd() {
		log.Error("missing HLS end in playlist")
		return nil
	}

	// check allowed tags
	for _, tag := range parser.Tags {
		if !slices.Contains(allowedTags, tag.Name) {
			log.WithField("tag", tag.Name).Error("invalid HLS tag")
			return nil
		}
	}

	return parser
}

type hlsElement struct {
	sourceFile     string
	segmentCounter int64
	duration       *float64
	isMap          bool
	fileExtension  string
	mimeType       string
}

func (encoder *Encoder) extractInitSegment(segmentHLS *hls.Parser) (string, bool) {
	initTags := common.FilterSlice(segmentHLS.Tags, func(tag hls.Tag) bool {
		return tag.Name == hls.TagNameMap
	})
	if len(initTags) != 1 {
		log.WithFields(log.Fields{
			"tags": len(initTags),
		}).Error("found none or more than one init tag")
		return "", false
	}
	return initTags[0].AttributeByName("URI").Value, true
}

func (encoder *Encoder) uploadHLS(job *encodeJob, encodeSet *encodeSet, segmentHLS *hls.Parser) bool {
	baseDir := filepath.Dir(segmentHLS.File)
	s3Connection := GetS3ByLocation(job.targetLibraryLocation)

	// build new location
	targetLocation := job.media.UID

	// generate hex value for encode set
	encodeSetPath, err := common.RandomHex(common.RandomLengthEncodeSet)
	if err != nil {
		log.WithField("inherit", err).Error("unable to generate new random hex")
		return false
	}
	encodeSet.dbEncodeSet.Path = encodeSetPath
	targetLocation = filepath.Join(targetLocation, encodeSetPath)
	encodeSet.dbEncodeSet.FullPath = targetLocation

	// add local folder prefix
	storeLocation := targetLocation
	if job.targetLibraryLocation.Type == database.LibraryLocationTypeLocalFolder {
		storeLocation = filepath.Join(job.targetLibraryLocation.Location, storeLocation)
		if err := os.MkdirAll(storeLocation, 0750); err != nil {
			log.WithFields(log.Fields{
				"inherit": err,
				"folder":  storeLocation,
			}).Error("unable to create encoding target folder")
			return false
		}
	}

	// initialization file
	initTags := common.FilterSlice(segmentHLS.Tags, func(tag hls.Tag) bool {
		return tag.Name == hls.TagNameMap
	})
	if len(initTags) != 1 {
		log.WithFields(log.Fields{
			"tags": len(initTags),
		}).Error("found more than one init tag")
		return false
	}
	initFileName, ok := encoder.extractInitSegment(segmentHLS)
	if !ok {
		return false
	}
	if !encoder.uploadHLSFile(job, encodeSet, storeLocation, s3Connection, hlsElement{
		sourceFile:     filepath.Join(baseDir, initFileName),
		segmentCounter: 0,
		isMap:          true,
		fileExtension:  hls.FileExtensionInit,
		mimeType:       hls.MIMEInit,
	}) {
		return false
	}

	// process segments
	segmentCounter := int64(0)
	for _, tag := range segmentHLS.Tags {
		// check for relevant tags
		if tag.Name != hls.TagNameSegment {
			continue
		}

		// increase segment counter
		segmentCounter++

		// extract duration
		if len(tag.Attributes) == 0 {
			log.Error("missing attributes of hls segment")
			return false
		}
		duration, err := strconv.ParseFloat(tag.Attributes[0].Value, 64)
		if err != nil {
			log.WithFields(log.Fields{
				"tag":      tag.Name,
				"value":    tag.Value,
				"duration": tag.Attributes[0].Value,
			}).Error("unable to parse segment duration")
			return false
		}

		// store file
		sourceFile := filepath.Join(baseDir, tag.Value)
		if !encoder.uploadHLSFile(job, encodeSet, storeLocation, s3Connection, hlsElement{
			sourceFile:     sourceFile,
			segmentCounter: segmentCounter,
			duration:       &duration,
			isMap:          false,
			fileExtension:  hls.FileExtensionSegment,
			mimeType:       hls.MIMEMP4Segment,
		}) {
			return false
		}
	}

	// update bandwidth
	maxSegmentSize := int64(0)
	sumSegmentSize := int64(0)
	for _, segment := range encodeSet.dbEncodeSegments {
		sumSegmentSize += segment.Size
		if maxSegmentSize < segment.Size {
			maxSegmentSize = segment.Size
		}
	}
	encodeSet.dbEncodeSet.Bandwidth = maxSegmentSize * 8 / hlsTargetDuration
	encodeSet.dbEncodeSet.AverageBandwidth = sumSegmentSize * 8 / hlsTargetDuration / int64(len(encodeSet.dbEncodeSegments))

	return true
}

func (encoder *Encoder) uploadHLSFile(job *encodeJob, encodeSet *encodeSet, storeLocation string,
	s3Connection *s3.S3, element hlsElement) bool {
	// load segment
	segmentData, err := os.ReadFile(element.sourceFile)
	if err != nil {
		log.WithField("inherit", err).Error("failed to read segment")
		return false
	}

	// build segment name
	randomNumber, err := common.RandomInt(65_536) // use random number to prevent name guessing
	if err != nil {
		log.WithField("inherit", err).Error("failed to generate random number")
		return false
	}
	segmentName := fmt.Sprintf("seg-%06d-%d.%s", element.segmentCounter, randomNumber, element.fileExtension)
	fullSegmentName := filepath.Join(storeLocation, segmentName)

	// store segment
	switch job.targetLibraryLocation.Type {
	case database.LibraryLocationTypeLocalFolder:
		// store to folder
		if err := os.WriteFile(fullSegmentName, segmentData, 0640); err != nil {
			log.WithField("inherit", err).Error("failed to write segment")
			return false
		}
	case database.LibraryLocationTypeS3:
		// upload segment
		_, err = s3Connection.PutObject(s3.PutObjectRequest{
			Name:        fullSegmentName,
			Data:        segmentData,
			ContentType: element.mimeType,
		})
		if err != nil {
			log.WithField("inherit", err).Error("failed to upload hls segment")
			return false
		}
	default:
		log.WithField("type", job.targetLibraryLocation.Type).Error("unknown library location type")
		return false
	}

	// calculate checksum
	checksum, err := calculateChecksumSha256(element.sourceFile)
	if err != nil {
		log.WithField("inherit", err).Error("failed to calculate checksum")
		return false
	}

	// prepare database entry
	dbDuration := sql.NullFloat64{}
	if element.duration != nil {
		dbDuration.Float64 = *element.duration
		dbDuration.Valid = true
	}
	encodeSet.dbEncodeSegments = append(encodeSet.dbEncodeSegments, &database.EncodeSegment{
		Sequence:       element.segmentCounter,
		Name:           segmentName,
		Size:           int64(len(segmentData)),
		ChecksumSha256: checksum,
		Duration:       dbDuration,
		Map:            element.isMap,
	})

	return true
}

func (encoder *Encoder) storeDatabase(job *encodeJob, encodeSet *encodeSet) bool {
	// create database transaction
	trx, err := database.Instance.Transaction()
	if err != nil {
		log.WithField("inherit", err).Error("unable to create transaction")
		return false
	}
	defer func() {
		if err := trx.Rollback(); err != nil {
			log.WithField("inherit", err).Error("unable to rollback transaction")
		}
	}()

	// store encode set
	encodeSetID, err := trx.CreateEncodeSet(*encodeSet.dbEncodeSet)
	if err != nil {
		log.WithField("inherit", err).Error("unable to create encodeSet")
		return false
	}

	// store encode segments
	for _, segment := range encodeSet.dbEncodeSegments {
		segment.EncodeSetID = encodeSetID
		if _, err := trx.CreateEncodeSegment(*segment); err != nil {
			log.WithField("inherit", err).Error("unable to create encodeSegment")
			return false
		}
	}

	// store messages
	for _, message := range encodeSet.dbMessages {
		message.EncodeSetID = sql.NullInt64{Int64: encodeSetID, Valid: true}
		if _, err := trx.CreateMediaMessage(*message); err != nil {
			log.WithField("inherit", err).Error("unable to create MediaMessage")
			return false
		}
	}

	// commit changes
	if err := trx.Commit(); err != nil {
		log.WithField("inherit", err).Error("unable to commit transaction")
		return false
	}

	return true
}

func (encoder *Encoder) cleanUpJob(job *encodeJob) {
	// invalidate cache
	if job.cacheItem != nil {
		job.cacheItem.Unreference()
	}
}

func (encoder *Encoder) cleanUpSet(set *encodeSet) {
	// clean up ffmpeg directory
	if set.workingDirectory != "" {
		if err := os.RemoveAll(set.workingDirectory); err != nil {
			log.WithField("inherit", err).Error("unable to clean up working directory")
		}
	}
}

func (encodeSet EncodeSetVideo) Skip(mediaStream *database.MediaStream) bool {
	return checkSkipEncodeSet(encodeSet.Enabled, encodeSet.ConditionSets, mediaStream)
}

func (encodeSet EncodeSetAudio) Skip(mediaStream *database.MediaStream) bool {
	return checkSkipEncodeSet(encodeSet.Enabled, encodeSet.ConditionSets, mediaStream)
}

func (encodeSet EncodeSetAudio) FullCodec() (string, error) {
	// as documented by apple:
	// https://developer.apple.com/library/archive/documentation/NetworkingInternet/Conceptual/StreamingMediaGuide/FrequentlyAskedQuestions/FrequentlyAskedQuestions.html

	switch encodeSet.Codec {
	case EncodeSetAudioCodecAACLC:
		return "mp4a.40.2", nil
	default:
		return "", fmt.Errorf("unknown codec %s", encodeSet.Codec)
	}
}

func checkSkipEncodeSet(enabled bool, conditionSets []EncodeConditionSet, mediaStream *database.MediaStream) bool {
	// check, if encode set is globally disabled
	if !enabled {
		return true
	}

	// check conditions
	validConditionSet := false
	for _, conditionSet := range conditionSets {
		// validate condition set
		if conditionSet.IsValid(mediaStream) {
			validConditionSet = true
			break
		}
	}
	return !validConditionSet
}

func (conditionSet EncodeConditionSet) IsValid(mediaStream *database.MediaStream) bool {
	// validate only enabled conditions
	if !conditionSet.Enabled {
		return false
	}

	// check all conditions
	validConditions := true
	for _, condition := range conditionSet.Conditions {
		switch condition.Type {
		case "true":
			continue
		case "video_min_resolution":
			width, _ := strconv.Atoi(condition.Value1)
			height, _ := strconv.Atoi(condition.Value2)
			if mediaStream.Width.Int64 >= int64(width) || mediaStream.Height.Int64 >= int64(height) {
				continue
			}
			validConditions = false
		default:
			log.WithField("conditionType", condition.Type).Fatal("invalid condition type")
		}
	}

	return validConditions
}
