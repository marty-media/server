/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package playback

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/marty-media/server/internal/common"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/hls"
	"gitlab.com/marty-media/server/internal/webserver"
	"net/http"
	"slices"
	"strconv"
)

const (
	playlistVersion = 6
)

func (ws *WebServer) servePlaylistMain(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo, playback *database.Playback) {
	encodeSets, ok := loadEncodeSets(playback)
	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// build playlist
	playlist, err := buildMainPlaylist(encodeSets)
	if err != nil {
		log.WithField("error", err).Error("unable to build playlist")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// serve playlist
	w.Header().Set("Content-Type", hls.MIMEHLS)
	if _, err := webserver.WriteGzip(w, r, []byte(playlist.String())); err != nil {
		log.WithField("error", err).Error("unable to send playlist")
	}
}

type encodeSetData struct {
	encodeSet   *database.EncodeSet
	mediaFormat *database.MediaFormat
	mediaStream *database.MediaStream
}

func loadEncodeSets(playback *database.Playback) ([]*encodeSetData, bool) {
	// load encode sets
	encodeSets, err := database.Instance.FindEncodeSetsByMediaID(playback.MediaID)
	if err != nil {
		log.WithField("error", err).Error("unable to find encode sets")
		return nil, false
	}

	var encodeSetDataList []*encodeSetData
	for _, encodeSet := range encodeSets {
		// load media format
		mediaFormat, err := database.Instance.GetMediaFormat(encodeSet.MediaFormatID)
		if err != nil {
			log.WithField("error", err).Warn("unable to find media format")
			continue
		}

		// load media stream item
		mediaStream, err := database.Instance.GetMediaStream(encodeSet.MediaStreamID)
		if err != nil {
			log.WithField("error", err).Warn("unable to find media stream")
			continue
		}

		encodeSetDataList = append(encodeSetDataList, &encodeSetData{
			encodeSet:   encodeSet,
			mediaFormat: mediaFormat,
			mediaStream: mediaStream,
		})
	}

	// filter by latest fields of media format and media stream
	// use latest version, if available, otherwise use the newest version
	groupedSets := map[string][]*encodeSetData{}
	for _, encodeSetDataEntry := range encodeSetDataList {
		id := fmt.Sprintf("%d#%s", encodeSetDataEntry.mediaStream.Index, encodeSetDataEntry.encodeSet.Name)
		groupedSets[id] = append(groupedSets[id], encodeSetDataEntry)
	}
	var result []*encodeSetData
	for _, encodeSetDataGroup := range groupedSets {
		// use latest version if available
		data := encodeSetDataGroup
		slices.SortFunc(data, func(a, b *encodeSetData) int {
			if a.mediaFormat.Latest && a.mediaStream.Latest {
				return -1
			} else if b.mediaFormat.Latest && b.mediaStream.Latest {
				return 1
			}

			return 0
		})
		result = append(result, data[0])
	}

	return result, true
}

type mediaGroup struct {
	ID               string
	Codec            string
	Bandwidth        int64
	AverageBandwidth int64
}

func buildMainPlaylist(encodeSets []*encodeSetData) (*hls.Writer, error) {
	// create hls writer
	writer := hls.NewWriter()
	writer.AddTagStart()
	writer.AddTagVersion(playlistVersion)

	// prepare data
	var videoSets []encodeSetData
	var audioSets []encodeSetData
	for _, set := range encodeSets {
		switch set.encodeSet.MediaType {
		case database.EncodeSetMediaTypeMovie:
			videoSets = append(videoSets, *set)
		case database.EncodeSetMediaTypeAudio:
			audioSets = append(audioSets, *set)
		}
	}

	// build media groups
	audioGroups := map[string]mediaGroup{}
	for _, audioSet := range audioSets {
		// retrieve ISO language code
		language := audioSet.encodeSet.Language
		if !language.Valid {
			continue
		}
		languageISO, found := common.ISO639_3[language.String]
		if !found {
			continue
		}

		// add media entry
		writer.AddAlternativeStream(hls.AddAlternativeStream{
			Type:     hls.MediaTypeAudio,
			URL:      fmt.Sprintf("s/%d/%s", audioSet.encodeSet.ID, hls.StreamPlaylistName),
			Group:    audioSet.encodeSet.Name,
			Language: languageISO.ISO639_1,
			Name:     languageISO.LocalizedName,
			//Default:         true,   // TODO
			//AutoSelect:      true,   // TODO
			//Forced:          true,   // TODO
			//SampleRate: "TODO", // TODO: sample rate
			//Characteristics: "", // TODO: add characteristics
			Channels: strconv.Itoa(int(audioSet.encodeSet.Channels.Int64)),
		})

		// store media group
		if _, found := audioGroups[audioSet.encodeSet.Name]; !found {
			audioGroups[audioSet.encodeSet.Name] = mediaGroup{
				ID:               audioSet.encodeSet.Name,
				Codec:            audioSet.encodeSet.FullCodec,
				Bandwidth:        audioSet.encodeSet.Bandwidth,
				AverageBandwidth: audioSet.encodeSet.AverageBandwidth,
			}
		}
	}

	// add streams
	for _, videoSet := range videoSets {
		videoSets, err := buildTagStreams(videoSet.encodeSet, audioGroups)
		if err != nil {
			return nil, err
		}
		for _, videoSet := range videoSets {
			writer.AddTagStream(videoSet)
		}
	}

	return writer, nil
}

func buildTagStreams(videoSet *database.EncodeSet, audioGroups map[string]mediaGroup) ([]hls.AddTagStream, error) {
	// check, if there are audio groups (otherwise return video only)
	var result []hls.AddTagStream
	if len(audioGroups) == 0 {
		tagStream, err := buildTagStream(videoSet)
		if err != nil {
			return nil, err
		}

		result = append(result, tagStream)
		return result, nil
	}

	// add audio groups
	for _, audioGroup := range audioGroups {
		tagStream, err := buildTagStream(videoSet)
		if err != nil {
			return nil, err
		}

		// add audio info
		tagStream.AudioGroup = audioGroup.ID
		tagStream.Codecs += "," + audioGroup.Codec
		tagStream.Bandwidth += audioGroup.Bandwidth
		tagStream.AverageBandwidth += audioGroup.AverageBandwidth

		// add stream
		result = append(result, tagStream)
	}

	return result, nil
}

func buildTagStream(videoSet *database.EncodeSet) (stream hls.AddTagStream, err error) {
	// build frame rate
	if !videoSet.FrameRate.Valid {
		log.Error("missing video set frame rate")
		return stream, fmt.Errorf("missing video set frame rate")
	}
	fps := 0.0
	switch videoSet.FrameRate.Int64 {
	case int64(database.EncodeSetFrameRateMovie):
		fps = 24
	case int64(database.EncodeSetFrameRatePAL):
		fps = 25
	case int64(database.EncodeSetFrameRateMovieNTSC):
		fps = 23.976
	default:
		log.WithField("frameRate", videoSet.FrameRate.Int64).Error("unknown video frame rate")
		return stream, fmt.Errorf("unknown video frame rate: %d", videoSet.FrameRate.Int64)
	}

	// build video range
	if !videoSet.VideoRange.Valid {
		log.Error("missing video range")
		return stream, fmt.Errorf("missing video range")
	}
	var videoRange string
	switch videoSet.VideoRange.Int64 {
	case int64(database.EncodeSetVideoRangeSDR):
		videoRange = "SDR"
	case int64(database.EncodeSetVideoRangeHLG):
		videoRange = "HLG"
	case int64(database.EncodeSetVideoRangePQ):
		videoRange = "PQ"
	default:
		log.WithField("videoRange", videoSet.VideoRange.Int64).Error("unknown video range")
		return stream, fmt.Errorf("unknown video range: %d", videoSet.VideoRange.Int64)
	}

	// build video stream
	stream.URL = fmt.Sprintf("s/%d/%s", videoSet.ID, hls.StreamPlaylistName)
	stream.Bandwidth = videoSet.Bandwidth
	stream.AverageBandwidth = videoSet.AverageBandwidth
	stream.Codecs = videoSet.FullCodec
	stream.Resolution = fmt.Sprintf("%dx%d", videoSet.Width.Int64, videoSet.Height.Int64)
	stream.FrameRate = fps
	stream.VideoRange = videoRange

	return
}

func (ws *WebServer) servePlaylist(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo, playback *database.Playback) {
	// load encode set
	encodeSet := ws.checkSet(w, r, info, playback)
	if encodeSet == nil {
		return
	}

	// load encode segments
	encodeSegments, err := database.Instance.FindEncodeSegmentsByEncodeSetID(encodeSet.ID)
	if err != nil {
		log.WithField("error", err).Error("unable to find encode segments for encode set")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// build playlist
	playlist := buildPlaylist(encodeSegments, encodeSet)

	// serve playlist
	w.Header().Set("Content-Type", hls.MIMEHLS)
	if _, err := webserver.WriteGzip(w, r, []byte(playlist.String())); err != nil {
		log.WithField("error", err).Error("unable to send playlist")
	}
}

func buildPlaylist(encodeSegments []*database.EncodeSegment, encodeSet *database.EncodeSet) *hls.Writer {
	// create hls writer
	writer := hls.NewWriter()
	writer.AddTagStart()
	writer.AddTagVersion(playlistVersion)
	writer.AddTagPlaylistType(hls.PlaylistTypeVOD)
	writer.AddTagTargetDuration(encodeSet.TargetDuration.Int64)
	if encodeSet.IndependentSegments {
		writer.AddTagIndependentSegments()
	}

	// sort segments by sequence
	slices.SortFunc(encodeSegments, func(a, b *database.EncodeSegment) int {
		return int(a.Sequence - b.Sequence)
	})

	// filter for map segment
	mapSegments := common.FilterSlice(encodeSegments, func(segment *database.EncodeSegment) bool {
		return segment.Map
	})

	// add map segment
	if len(mapSegments) > 0 {
		mapSegment := mapSegments[0]
		writer.AddTagMap(fmt.Sprintf("f/%d/%s", mapSegment.ID, mapSegment.Name))
	}

	// filter for content segments
	contentSegments := common.FilterSlice(encodeSegments, func(segment *database.EncodeSegment) bool {
		return !segment.Map
	})

	// add segments
	for _, segment := range contentSegments {
		url := fmt.Sprintf("f/%d/%s", segment.ID, segment.Name)
		writer.AddTagSegment(url, segment.Duration.Float64)
	}

	writer.AddTagEnd()
	return writer
}
