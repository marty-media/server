/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package playback

import (
	log "github.com/sirupsen/logrus"
	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/marty-media/server/internal/backend"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/hls"
	"gitlab.com/marty-media/server/internal/storage/s3"
	"net/http"
	"os"
	"path/filepath"
)

func (ws *WebServer) serveFile(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo, playback *database.Playback) {
	// load encode set
	encodeSet := ws.checkSet(w, r, info, playback)
	if encodeSet == nil {
		return
	}

	// load encode segment
	encodeSegment := ws.checkSegment(w, r, info, playback, encodeSet)
	if encodeSegment == nil {
		return
	}

	// validate file name
	fileName := info.Parameters["*"]
	if fileName != encodeSegment.Name {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	// load location
	location, err := database.Instance.GetLibraryLocation(encodeSet.StoredLibraryLocationID)
	if err != nil {
		log.WithField("inherit", err).Error("error retrieving library location")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// retrieve file content
	data, err := retrieveFile(location, encodeSet, encodeSegment)
	if err != nil {
		log.WithField("inherit", err).Error("error retrieving file content")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// choose correct content type
	contentType := hls.MIMEMP4Segment
	if encodeSegment.Map {
		contentType = hls.MIMEInit
	}

	// serve file
	w.Header().Set("Content-Type", contentType)
	if _, err := w.Write(data); err != nil {
		log.WithField("inherit", err).Error("error sending file")
	}
}

func retrieveFile(location *database.LibraryLocation, encodeSet *database.EncodeSet, encodeSegment *database.EncodeSegment) (data []byte, err error) {
	switch location.Type {
	case database.LibraryLocationTypeLocalFolder:
		filePath := filepath.Join(location.Location, encodeSet.FullPath, encodeSegment.Name)
		return os.ReadFile(filePath)
	case database.LibraryLocationTypeS3:
		s3Connection := backend.GetS3ByLocation(location)
		s3Object, err := s3Connection.GetObject(s3.GetObjectRequest{
			Name: filepath.Join(encodeSet.FullPath, encodeSegment.Name),
		})
		if err != nil {
			return nil, err
		}
		return s3Object.Data, nil
	default:
		log.WithField("locationType", location.Type).Warn("library location type not supported")
		return
	}
}
