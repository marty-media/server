/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package playback

import (
	log "github.com/sirupsen/logrus"
	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/hls"
	"gitlab.com/marty-media/server/internal/webserver"
	"net/http"
	"strconv"
)

type WebServer struct {
}

var WebServerInstance = &WebServer{}

func (ws *WebServer) Prepare(webServer *webserver.WebServer) {
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/playback/:playbackID/"+hls.MainPlaylistName, ws.serve(ws.servePlaylistMain)))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/playback/:playbackID/s/:setID/"+hls.StreamPlaylistName, ws.serve(ws.servePlaylist)))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/playback/:playbackID/s/:setID/f/:segmentID/*", ws.serve(ws.serveFile)))
}

func (ws *WebServer) serve(fn func(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo, playback *database.Playback)) httprouter.RouteHandler {
	return func(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
		// load and verify playback
		playback := ws.checkPlayback(w, r, info)
		if playback == nil {
			return
		}
		fn(w, r, info, playback)
	}
}

func (ws *WebServer) checkPlayback(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) *database.Playback {
	// parse request ID
	playbackUID := info.Parameters["playbackID"]

	// load playback
	playback, err := database.Instance.FindPlaybackByUID(playbackUID)
	if err != nil {
		log.WithField("error", err).Error("unable to find playback by UID")
		w.WriteHeader(http.StatusInternalServerError)
		return nil
	}

	// playback found?
	if playback == nil {
		w.WriteHeader(http.StatusNotFound)
		return nil
	}

	return playback
}

func (ws *WebServer) checkSet(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo, playback *database.Playback) *database.EncodeSet {
	// parse set ID
	setIDString := info.Parameters["setID"]
	setID, err := strconv.Atoi(setIDString)
	if err != nil {
		log.WithField("error", err).Info("unable to parse set ID")
		w.WriteHeader(http.StatusNotFound)
		return nil
	}

	// load encode set
	encodeSet, err := database.Instance.GetEncodeSet(int64(setID))
	if err != nil {
		log.WithField("error", err).Error("unable to retrieve encodeSet")
		w.WriteHeader(http.StatusInternalServerError)
		return nil
	}

	// validate set
	if encodeSet == nil || encodeSet.MediaID != playback.MediaID {
		w.WriteHeader(http.StatusNotFound)
		return nil
	}

	return encodeSet
}

func (ws *WebServer) checkSegment(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo, playback *database.Playback, encodeSet *database.EncodeSet) *database.EncodeSegment {
	// parse segment ID
	segmentIDString := info.Parameters["segmentID"]
	segmentID, err := strconv.Atoi(segmentIDString)
	if err != nil {
		log.WithField("error", err).Info("unable to parse segment ID")
		w.WriteHeader(http.StatusNotFound)
		return nil
	}

	// load encode segment
	encodeSegment, err := database.Instance.GetEncodeSegment(int64(segmentID))
	if err != nil {
		log.WithField("error", err).Error("unable to retrieve encode segment")
		w.WriteHeader(http.StatusInternalServerError)
		return nil
	}

	// validate segment
	if encodeSegment.EncodeSetID != encodeSet.ID {
		w.WriteHeader(http.StatusNotFound)
		return nil
	}

	return encodeSegment
}
