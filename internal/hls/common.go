/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package hls

const (
	MIMEHLS        = "application/vnd.apple.mpegurl"
	MIMEInit       = "video/mp4"
	MIMEMP4Segment = "video/iso.segment"
)

const (
	MainPlaylistName   = "main.n3u8"
	StreamPlaylistName = "stream.n3u8"
)

const (
	FileExtensionInit    = "mp4"
	FileExtensionSegment = "m4s"
)

const (
	TagNameM3U                 = "EXTM3U"
	TagNameSegment             = "EXTINF"
	TagNameVersion             = "EXT-X-VERSION"
	TagNameTargetDuration      = "EXT-X-TARGETDURATION"
	TagNameMediaSequence       = "EXT-X-MEDIA-SEQUENCE"
	TagNamePlaylistType        = "EXT-X-PLAYLIST-TYPE"
	TagNameIndependentSegments = "EXT-X-INDEPENDENT-SEGMENTS"
	TagNameMap                 = "EXT-X-MAP"
	TagNameStream              = "EXT-X-STREAM-INF"
	TagNameAlternativeStream   = "EXT-X-MEDIA"
	TagNameEndList             = "EXT-X-ENDLIST"
)

const (
	AttributeBandwidth        = "BANDWIDTH"
	AttributeAverageBandwidth = "AVERAGE-BANDWIDTH"
	AttributeCodecs           = "CODECS"
	AttributeResolution       = "RESOLUTION"
	AttributeFrameRate        = "FRAME-RATE"
	AttributeVideoRange       = "VIDEO-RANGE"
	AttributeAudio            = "AUDIO"
	AttributeSubtitles        = "SUBTITLES"
	AttributeClosedCaptions   = "CLOSED-CAPTIONS"
	AttributeURI              = "URI"
	AttributeType             = "TYPE"
	AttributeGroupID          = "GROUP-ID"
	AttributeLanguage         = "LANGUAGE"
	AttributeName             = "NAME"
	AttributeDefault          = "DEFAULT"
	AttributeAutoSelect       = "AUTOSELECT"
	AttributeForced           = "FORCED"
	AttributeInstreamID       = "INSTREAM-ID"
	AttributeBitDepth         = "BIT-DEPTH"
	AttributeSampleRate       = "SAMPLE-RATE"
	AttributeCharacteristics  = "CHARACTERISTICS"
	AttributeChannels         = "CHANNELS"
)

const (
	VideoRangeSDR = "SDR"
	VideoRangeHLG = "HLG"
	VideoRangePQ  = "PQ"
)

const (
	MediaTypeAudio          = "AUDIO"
	MediaTypeVideo          = "VIDEO"
	MediaTypeSubtitles      = "SUBTITLES"
	MediaTypeClosedCaptions = "CLOSED-CAPTIONS"
)

const (
	PlaylistTypeEvent = "EVENT"
	PlaylistTypeVOD   = "VOD"
)

const (
	ValueYes = "YES"
	ValueNo  = "NO"
)

const (
	newLine              = "\n"
	tagPrefix            = "#"
	tagSeparator         = ":"
	attributeSeparator   = ","
	attributeKVSeparator = "="
	attributeQuoted      = "\""
)

type Tag struct {
	Name       string
	Value      string
	Attributes []Attribute
}

type Attribute struct {
	Key      string
	Value    string
	IsQuoted bool
}

func (tag *Tag) AttributeByName(name string) *Attribute {
	for _, attr := range tag.Attributes {
		if attr.Key == name {
			return &attr
		}
	}

	return nil
}
