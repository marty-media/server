/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package hls

import "testing"

func TestParse1(t *testing.T) {
	// create new parser
	parser := NewParser("test/test1.m3u8")

	// parse file
	if !parser.Parse() {
		t.Errorf("failed to parse hls file")
	}

	// validate result
	if len(parser.Tags) != 18 {
		t.Errorf("failed to parse tags; found %d", len(parser.Tags))
	}

	// check start and end
	if !parser.HasStart() || !parser.HasEnd() {
		t.Errorf("failed to parse start or end of tags")
	}

	// check non-existing attribute
	if parser.Tags[0].AttributeByName("NOTEXIST") != nil {
		t.Errorf("found non-existing attribute %+v", parser.Tags[0].AttributeByName("NOTEXIST"))
	}

	// validate tag 1
	if parser.Tags[0].Name != TagNameM3U {
		t.Errorf("failed to validate tag 0; %+v", parser.Tags[0])
	}

	// validate tag 2
	if parser.Tags[1].Name != TagNameVersion || parser.Tags[1].Attributes[0].Value != "7" {
		t.Errorf("failed to validate tag 1; %+v", parser.Tags[1])
	}

	// validate tag 6
	if parser.Tags[5].Name != TagNameMap || parser.Tags[5].AttributeByName("URI") == nil ||
		parser.Tags[5].Attributes[0].Key != "URI" || parser.Tags[5].Attributes[0].Value != "init.mp4" {
		t.Errorf("failed to validate tag 5; %+v", parser.Tags[5])
	}

	// validate tag 7
	if parser.Tags[6].Name != TagNameSegment {
		t.Errorf("failed to validate tag 6; %+v", parser.Tags[6])
	}

	// find tag by name
	if parser.FindTagByName(TagNameVersion) == nil {
		t.Error("failed to find tag by name")
	}

	// search for non-existing tag
	if parser.FindTagByName("NOTEXIST") != nil {
		t.Error("found non-existing tag by name")
	}
}
