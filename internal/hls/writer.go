/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package hls

import (
	"fmt"
	"strconv"
	"strings"
)

type Writer struct {
	Tags []Tag
}

func NewWriter() *Writer {
	return &Writer{}
}

func (w *Writer) AddTag(tag Tag) {
	w.Tags = append(w.Tags, tag)
}

func (w *Writer) AddTagStart() {
	w.AddTag(Tag{
		Name: TagNameM3U,
	})
}

func (w *Writer) AddTagVersion(version int) {
	w.AddTag(Tag{
		Name: TagNameVersion,
		Attributes: []Attribute{{
			Value: strconv.Itoa(version),
		}},
	})
}

func (w *Writer) AddTagPlaylistType(playlistType string) {
	w.AddTag(Tag{
		Name: TagNamePlaylistType,
		Attributes: []Attribute{{
			Value: playlistType,
		}},
	})
}

func (w *Writer) AddTagIndependentSegments() {
	w.AddTag(Tag{
		Name: TagNameIndependentSegments,
	})
}

func (w *Writer) AddTagTargetDuration(duration int64) {
	w.AddTag(Tag{
		Name: TagNameTargetDuration,
		Attributes: []Attribute{{
			Value: strconv.Itoa(int(duration)),
		}},
	})
}

func (w *Writer) AddTagMap(url string) {
	w.AddTag(Tag{
		Name: TagNameMap,
		Attributes: []Attribute{{
			Key:      AttributeURI,
			Value:    url,
			IsQuoted: true,
		}},
	})
}

type AddTagStream struct {
	URL              string
	Bandwidth        int64
	AverageBandwidth int64
	Codecs           string
	Resolution       string
	FrameRate        float64
	VideoRange       string
	AudioGroup       string
	SubtitleGroup    string
	ClosedCaptions   string
}

func (w *Writer) AddTagStream(request AddTagStream) {
	tag := Tag{
		Name:  TagNameStream,
		Value: request.URL,
		Attributes: []Attribute{{
			Key:   AttributeBandwidth,
			Value: strconv.FormatInt(request.Bandwidth, 10),
		}},
	}

	if request.AverageBandwidth > 0 {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:   AttributeAverageBandwidth,
			Value: strconv.FormatInt(request.AverageBandwidth, 10),
		})
	}

	if request.Codecs != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:      AttributeCodecs,
			Value:    request.Codecs,
			IsQuoted: true,
		})
	}

	if request.Resolution != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:   AttributeResolution,
			Value: request.Resolution,
		})
	}

	if request.FrameRate > 0 {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:   AttributeFrameRate,
			Value: strconv.FormatFloat(request.FrameRate, 'f', -1, 64),
		})
	}

	if request.VideoRange != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:   AttributeVideoRange,
			Value: request.VideoRange,
		})
	}

	if request.AudioGroup != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:      AttributeAudio,
			Value:    request.AudioGroup,
			IsQuoted: true,
		})
	}

	if request.SubtitleGroup != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:      AttributeSubtitles,
			Value:    request.SubtitleGroup,
			IsQuoted: true,
		})
	}

	if request.ClosedCaptions != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:      AttributeClosedCaptions,
			Value:    request.ClosedCaptions,
			IsQuoted: true,
		})
	}

	w.AddTag(tag)
}

type AddAlternativeStream struct {
	Type            string
	URL             string
	Group           string
	Language        string
	Name            string
	Default         bool
	AutoSelect      bool
	Forced          bool
	InstreamID      string
	BitDepth        int
	SampleRate      int
	Characteristics string
	Channels        string
}

func (w *Writer) AddAlternativeStream(request AddAlternativeStream) {
	tag := Tag{
		Name: TagNameAlternativeStream,
		Attributes: []Attribute{{
			Key:   AttributeType,
			Value: request.Type,
		}, {
			Key:      AttributeGroupID,
			Value:    request.Group,
			IsQuoted: true,
		}},
	}

	if request.URL != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:      AttributeURI,
			Value:    request.URL,
			IsQuoted: true,
		})
	}

	if request.Language != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:      AttributeLanguage,
			Value:    request.Language,
			IsQuoted: true,
		})
	}

	if request.Name != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:      AttributeName,
			Value:    request.Name,
			IsQuoted: true,
		})
	}

	if request.Default {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:   AttributeDefault,
			Value: ValueYes,
		})
	}

	if request.AutoSelect {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:   AttributeAutoSelect,
			Value: ValueYes,
		})
	}

	if request.Forced {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:   AttributeForced,
			Value: ValueYes,
		})
	}

	if request.InstreamID != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:      AttributeInstreamID,
			Value:    request.InstreamID,
			IsQuoted: true,
		})
	}

	if request.BitDepth > 0 {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:   AttributeBitDepth,
			Value: strconv.Itoa(request.SampleRate),
		})
	}

	if request.SampleRate > 0 {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:   AttributeSampleRate,
			Value: strconv.Itoa(request.SampleRate),
		})
	}

	if request.Characteristics != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:      AttributeCharacteristics,
			Value:    request.Characteristics,
			IsQuoted: true,
		})
	}

	if request.Channels != "" {
		tag.Attributes = append(tag.Attributes, Attribute{
			Key:      AttributeChannels,
			Value:    request.Channels,
			IsQuoted: true,
		})
	}

	w.AddTag(tag)
}

func (w *Writer) AddTagSegment(url string, duration float64) {
	tag := Tag{
		Name:  TagNameSegment,
		Value: url,
		Attributes: []Attribute{{
			Value: fmt.Sprintf("%f,", duration),
		}},
	}

	w.AddTag(tag)
}

func (w *Writer) AddTagEnd() {
	w.AddTag(Tag{
		Name: TagNameEndList,
	})
}

func (w *Writer) String() string {
	b := &strings.Builder{}
	for _, tag := range w.Tags {
		w.writeTag(b, tag)
		b.WriteString(newLine)
	}

	return b.String()
}

func (w *Writer) writeTag(builder *strings.Builder, tag Tag) {
	// add tag name
	builder.WriteString(tagPrefix)
	builder.WriteString(tag.Name)

	// add tag attributes
	if tag.Attributes != nil && len(tag.Attributes) > 0 {
		builder.WriteString(tagSeparator)
		for i, attr := range tag.Attributes {
			if i > 0 {
				builder.WriteString(attributeSeparator)
			}
			if attr.Key != "" {
				builder.WriteString(attr.Key)
				builder.WriteString(attributeKVSeparator)
			}
			if attr.IsQuoted {
				builder.WriteString(attributeQuoted)
				builder.WriteString(strings.ReplaceAll(attr.Value, attributeQuoted, "\\"+attributeQuoted))
				builder.WriteString(attributeQuoted)
			} else {
				builder.WriteString(attr.Value)
			}
		}
	}

	// add tag value
	if tag.Value != "" {
		builder.WriteString(newLine)
		builder.WriteString(tag.Value)
	}
}
