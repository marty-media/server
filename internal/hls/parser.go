/*
 * Copyright (C) 2024 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package hls

import (
	log "github.com/sirupsen/logrus"
	"os"
	"strings"
)

type Parser struct {
	File string
	Tags []Tag
}

func NewParser(file string) *Parser {
	return &Parser{
		File: file,
	}
}

func (parser *Parser) Parse() bool {
	// read file
	data, err := os.ReadFile(parser.File)
	if err != nil {
		log.WithFields(log.Fields{
			"file":    parser.File,
			"inherit": err,
		})
		return false
	}

	// parse m3u8 playlist file
	fileContent := string(data)
	lines := strings.Split(fileContent, newLine)
	var currentTag *Tag
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}

		// check for new tag
		if strings.HasPrefix(line, tagPrefix) {
			// remove prefix
			line = strings.TrimPrefix(line, tagPrefix)

			// append previous tag
			if currentTag != nil {
				parser.Tags = append(parser.Tags, *currentTag)
			}

			// parse tag name
			name, attributeText, _ := strings.Cut(line, tagSeparator)
			currentTag = &Tag{
				Name: name,
			}

			// parse attributes
			currentTag.Attributes = parser.parseAttributes(attributeText)
			continue
		}

		// parse value
		if currentTag != nil {
			currentTag.Value = line
		}
	}

	// check for last tag
	if currentTag != nil {
		parser.Tags = append(parser.Tags, *currentTag)
	}

	return true
}

func (parser *Parser) parseAttributes(str string) []Attribute {
	var result []Attribute
	parts := strings.Split(str, attributeSeparator)

	for _, part := range parts {
		k, v, found := strings.Cut(part, attributeKVSeparator)
		if found {
			result = append(result, Attribute{
				Key:   k,
				Value: parser.parseAttributeValue(v),
			})
		} else {
			result = append(result, Attribute{
				Value: parser.parseAttributeValue(part),
			})
		}
	}

	return result
}

func (parser *Parser) parseAttributeValue(str string) string {
	if strings.HasPrefix(str, attributeQuoted) && strings.HasSuffix(str, attributeQuoted) {
		return str[1 : len(str)-len(attributeQuoted)]
	}

	return str
}

func (parser *Parser) HasStart() bool {
	if len(parser.Tags) == 0 {
		return false
	}
	firstTag := parser.Tags[0]
	return firstTag.Name == TagNameM3U
}

func (parser *Parser) HasEnd() bool {
	if len(parser.Tags) == 0 {
		return false
	}
	lastTag := parser.Tags[len(parser.Tags)-1]
	return lastTag.Name == TagNameEndList
}

func (parser *Parser) FindTagByName(name string) *Tag {
	for _, tag := range parser.Tags {
		if tag.Name == name {
			return &tag
		}
	}

	return nil
}
