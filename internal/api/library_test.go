/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package api

import (
	"fmt"
	"net/http"
	"strings"
	"testing"
)

func TestLibrary(t *testing.T) {
	id := testNewLibrary(t)
	testLibraryRescan(t, id)
	testInvalidLibrary(t, id)
	testUpdateLibrary(t, id)
	testDeleteLibrary(t, id)
}

func testNewLibrary(t *testing.T) int64 {
	// create test library
	testLibrary := `{"name": "API Test Library", "type": 1, "language": "eng"}`
	var location string
	callHTTP(t, http.MethodPost, "/api/v1/libraries", strings.NewReader(testLibrary), "json", http.StatusCreated, &location)
	id := parseRedirectHeader(t, location, "/api/v1/libraries/")

	// re-load library
	var library Library
	callHTTP(t, http.MethodGet, fmt.Sprintf("/api/v1/libraries/%d?includeLocations=true", id), nil, "json", http.StatusOK, &library)
	if library.ID != id {
		t.Error("received invalid library ID")
	}

	// check for existing library
	var libraries []Library
	callHTTP(t, http.MethodGet, "/api/v1/libraries", nil, "json", http.StatusOK, &libraries)
	foundLibrary := false
	for _, lib := range libraries {
		if lib.ID == id {
			foundLibrary = true
			break
		}
	}
	if !foundLibrary {
		t.Error("created library not found")
	}

	return id
}

func testLibraryRescan(t *testing.T, id int64) {
	// test with valid ID
	callHTTP(t, http.MethodPost, fmt.Sprintf("/api/v1/libraries/%d/rescan", id), nil, "json", http.StatusAccepted, nil)

	// test with invalid ID
	callHTTP(t, http.MethodPost, fmt.Sprintf("/api/v1/libraries/%d/rescan", 999), nil, "json", http.StatusNotFound, nil)
}

func testUpdateLibrary(t *testing.T, id int64) {
	var location string
	callHTTP(t, http.MethodPatch, fmt.Sprintf("/api/v1/libraries/%d", id), strings.NewReader(`{"name": "YEA"}`), "json", http.StatusNoContent, &location)
	newID := parseRedirectHeader(t, location, "/api/v1/libraries/")
	if newID != id {
		t.Error("received invalid library ID form patch", newID, id)
	}
}

func testDeleteLibrary(t *testing.T, id int64) {
	callHTTP(t, http.MethodDelete, fmt.Sprintf("/api/v1/libraries/%d", id), nil, "json", http.StatusNoContent, nil)
}

func TestLibraryLanguages(t *testing.T) {
	var languages []Language
	callHTTP(t, http.MethodGet, "/api/v1/libraries/languages", nil, "json", http.StatusOK, &languages)
	if len(languages) < 50 {
		t.Error("received too less languages")
	}
}

func testInvalidLibrary(t *testing.T, libraryID int64) {
	// invalid JSON request
	callHTTP(t, http.MethodPost, "/api/v1/libraries", strings.NewReader("invalidJSON"), "json", http.StatusBadRequest, nil)

	// invalid library type
	callHTTP(t, http.MethodPost, "/api/v1/libraries", strings.NewReader(`{"type": 999}`), "json", http.StatusBadRequest, nil)

	// invalid library ID
	callHTTP(t, http.MethodGet, "/api/v1/libraries/12345", nil, "json", http.StatusNotFound, nil)

	// invalid library ID (no integer)
	callHTTP(t, http.MethodGet, "/api/v1/libraries/asdf", nil, "json", http.StatusBadRequest, nil)

	// invalid ID for patching
	callHTTP(t, http.MethodPatch, "/api/v1/libraries/asdf", nil, "json", http.StatusBadRequest, nil)

	// invalid content for patching
	callHTTP(t, http.MethodPatch, fmt.Sprintf("/api/v1/libraries/%d", libraryID), strings.NewReader("{asdf}"), "json", http.StatusBadRequest, nil)

	// delete not existing library
	callHTTP(t, http.MethodDelete, "/api/v1/libraries/12345", nil, "json", http.StatusNotFound, nil)
}
