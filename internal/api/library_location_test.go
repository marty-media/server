/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package api

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"testing"
)

func TestLibraryLocation(t *testing.T) {
	libraryID := testNewLibrary(t)
	defer testDeleteLibrary(t, libraryID)

	id := testNewLibraryLocation(t, libraryID)
	testDeleteLibraryLocation(t, libraryID, id)
}

func testNewLibraryLocation(t *testing.T, libraryID int64) int64 {
	// create test library location
	folder, err := os.MkdirTemp("", "mms")
	if err != nil {
		t.Fatal("unable to create temporary folder")
	}
	testLocation := fmt.Sprintf(`{"type": 1, "usageType": 1, "location": "%s"}`, folder)
	var location string
	callHTTP(t, http.MethodPost, fmt.Sprintf("/api/v1/libraries/%d/locations", libraryID), strings.NewReader(testLocation), "json", http.StatusCreated, &location)
	id := parseRedirectHeader(t, location, fmt.Sprintf("/api/v1/libraries/%d/locations/", libraryID))

	// re-load library location
	var libraryLocation LibraryLocation
	callHTTP(t, http.MethodGet, fmt.Sprintf("/api/v1/libraries/%d/locations/%d", libraryID, id), nil, "json", http.StatusOK, &libraryLocation)
	if libraryLocation.ID != id {
		t.Error("received invalid library location ID")
	}

	// check for existing location
	var locations []LibraryLocation
	callHTTP(t, http.MethodGet, fmt.Sprintf("/api/v1/libraries/%d/locations", libraryID), nil, "json", http.StatusOK, &locations)
	locationFound := false
	for _, location := range locations {
		if location.ID == id {
			locationFound = true
			break
		}
	}
	if !locationFound {
		t.Error("created library location not found")
	}

	// re-scan library location
	callHTTP(t, http.MethodPost, fmt.Sprintf("/api/v1/libraries/%d/locations/%d/rescan", libraryID, id), nil, "json", http.StatusAccepted, nil)

	return id
}

func testDeleteLibraryLocation(t *testing.T, libraryID int64, id int64) {
	callHTTP(t, http.MethodDelete, fmt.Sprintf("/api/v1/libraries/%d/locations/%d", libraryID, id), nil, "json", http.StatusNoContent, nil)
}

func TestInvalidLibraryLocation(t *testing.T) {
	// create test data
	libraryID := testNewLibrary(t)
	defer testDeleteLibrary(t, libraryID)
	id := testNewLibraryLocation(t, libraryID)
	defer testDeleteLibraryLocation(t, libraryID, id)

	// invalid library ID
	callHTTP(t, http.MethodGet, "/api/v1/libraries/12345/locations", nil, "json", http.StatusNotFound, nil)
	callHTTP(t, http.MethodGet, fmt.Sprintf("/api/v1/libraries/12345/locations/%d", id), nil, "json", http.StatusNotFound, nil)
	callHTTP(t, http.MethodPost, "/api/v1/libraries/12345/locations", strings.NewReader(`{}`), "json", http.StatusNotFound, nil)

	// invalid location ID (no number)
	callHTTP(t, http.MethodGet, fmt.Sprintf("/api/v1/libraries/%d/locations/asdf", libraryID), nil, "json", http.StatusBadRequest, nil)

	// invalid location for rescan
	callHTTP(t, http.MethodPost, fmt.Sprintf("/api/v1/libraries/%d/locations/123456/rescan", libraryID), nil, "json", http.StatusNotFound, nil)

	// invalid body for location creation
	callHTTP(t, http.MethodPost, fmt.Sprintf("/api/v1/libraries/%d/locations", libraryID), strings.NewReader(`asdf`), "json", http.StatusBadRequest, nil)

	// missing required fields for location creation
	callHTTP(t, http.MethodPost, fmt.Sprintf("/api/v1/libraries/%d/locations", libraryID), strings.NewReader(`{"type": 12345}`), "json", http.StatusBadRequest, nil)

	// delete not existing location
	callHTTP(t, http.MethodDelete, fmt.Sprintf("/api/v1/libraries/%d/locations/12345", libraryID), nil, "json", http.StatusNotFound, nil)
}
