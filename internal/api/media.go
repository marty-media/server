/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package api

import (
	"encoding/xml"
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/marty-media/server/internal/database"
)

type Media struct {
	XMLName          xml.Name               `json:"-" xml:"media"`
	ID               int64                  `json:"id" xml:"id" mmsSortable:"true"`
	Name             string                 `json:"name" xml:"name" mmsSortable:"true"`
	CreatedAt        time.Time              `json:"createdAt" xml:"createdAt" mmsSortable:"true"`
	StreamAttributes *MediaStreamAttributes `json:"streamAttributes,omitempty" xml:"streamAttributes"`
	VideoStreams     []MediaStreamVideo     `json:"videoStreams,omitempty" xml:"videoStreams"`
	AudioStreams     []MediaStreamAudio     `json:"audioStreams,omitempty" xml:"audioStreams"`
	SubtitleStreams  []MediaStreamSubtitle  `json:"subtitleStreams,omitempty" xml:"subtitleStreams"`
}

type MediaStreamAttributes struct {
	Has4kVideo           bool `json:"has4kVideo" xml:"has4kVideo"`
	HasFullHDVideo       bool `json:"hasFullHDVideo" xml:"hasFullHDVideo"`
	HasDolbyDigitalAudio bool `json:"hasDolbyDigitalAudio" xml:"hasDolbyDigitalAudio"`
}

type MediaStreamVideo struct {
	XMLName xml.Name `json:"-" xml:"videoStream"`
	Codec   string   `json:"codec" xml:"codec"`
	Height  int64    `json:"height" xml:"height"`
	Width   int64    `json:"width" xml:"width"`
}

type MediaStreamAudio struct {
	XMLName xml.Name `json:"-" xml:"audioStream"`
	Codec   string   `json:"codec" xml:"codec"`
}

type MediaStreamSubtitle struct {
	XMLName xml.Name `json:"-" xml:"subtitleStream"`
	Codec   string   `json:"codec" xml:"codec"`
}

func newAPIMedia(media *database.Media) Media {
	return Media{
		ID:        media.ID,
		Name:      media.Path,
		CreatedAt: media.CreatedAt,
	}
}

func (media *Media) addStream(stream *database.MediaStream) {
	// initialize stream attributes
	if media.StreamAttributes == nil {
		media.StreamAttributes = &MediaStreamAttributes{}
	}

	// parse stream
	switch stream.CodecType {
	case database.MediaStreamCodecTypeVideo:
		videoStream := newAPIMediaStreamVideo(stream)
		media.StreamAttributes.Has4kVideo = media.StreamAttributes.Has4kVideo || videoStream.Is4k()
		media.StreamAttributes.HasFullHDVideo = media.StreamAttributes.HasFullHDVideo || videoStream.IsFullHD()
		media.VideoStreams = append(media.VideoStreams, videoStream)
	case database.MediaStreamCodecTypeAudio:
		audioStream := newAPIMediaStreamAudio(stream)
		media.StreamAttributes.HasDolbyDigitalAudio = media.StreamAttributes.HasDolbyDigitalAudio || audioStream.IsDolbyDigital()
		media.AudioStreams = append(media.AudioStreams, audioStream)
	case database.MediaStreamCodecTypeSubtitle:
		subtitleStream := newAPIMediaStreamSubtitle(stream)
		media.SubtitleStreams = append(media.SubtitleStreams, subtitleStream)
	default:
		log.WithFields(log.Fields{
			"codecType": stream.CodecType,
		}).Error("unknown media stream type")
	}
}

func newAPIMediaStreamVideo(mediaStream *database.MediaStream) MediaStreamVideo {
	return MediaStreamVideo{
		Codec:  mediaStream.CodecName,
		Height: mediaStream.Height.Int64,
		Width:  mediaStream.Width.Int64,
	}
}

func (mediaStream *MediaStreamVideo) Is4k() bool {
	// use some tolerance for cropped video (10%)
	return mediaStream.Height >= 2160*0.9 || mediaStream.Width >= 3840*0.9
}

func (mediaStream *MediaStreamVideo) IsFullHD() bool {
	// check, if the video is already marked as 4K
	if mediaStream.Is4k() {
		return false
	}

	// use some tolerance for cropped video (10%)
	return mediaStream.Height >= 1080*0.9 || mediaStream.Width >= 1920*0.9
}

func newAPIMediaStreamAudio(mediaStream *database.MediaStream) MediaStreamAudio {
	return MediaStreamAudio{
		Codec: mediaStream.CodecName,
	}
}

func (mediaStream *MediaStreamAudio) IsDolbyDigital() bool {
	return mediaStream.Codec == "ac3"
}

func newAPIMediaStreamSubtitle(mediaStream *database.MediaStream) MediaStreamSubtitle {
	return MediaStreamSubtitle{
		Codec: mediaStream.CodecName,
	}
}

func (api *API) serveMediaList(w http.ResponseWriter, r *http.Request, _ httprouter.RoutingInfo) {
	// load media entries
	libraryIDString := r.URL.Query().Get("libraryID")
	libraryID, err := strconv.Atoi(libraryIDString)
	if err != nil {
		api.sendResponse(w, r, badRequestError)
		return
	}

	// check for sorting
	sorting := parseSorting(api, w, r, "name", Media{})
	if sorting == nil {
		// invalid request
		return
	}

	// load media entries from database
	rows, err := database.Instance.FindMediaEntriesByLibrary(int64(libraryID))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// build response
	var response []Media
	for _, row := range rows {
		response = append(response, newAPIMedia(row))
	}

	// sort response
	runGenericSort(*sorting, response)

	// marshal and send response
	api.sendResponse(w, r, response)
}

func (api *API) GetMedia(id int64, loadStreams bool) (*Media, error) {
	// load media entry
	media, err := database.Instance.GetMedia(id)
	if err != nil {
		log.WithField("inherit", err).Error("unable to load media entry")
		return nil, err
	}

	// build API response
	var response *Media
	if media != nil {
		apiMedia := newAPIMedia(media)
		response = &apiMedia

		// load media items
		if loadStreams {
			if err := response.loadStreams(); err != nil {
				return nil, err
			}
		}
	}

	return response, nil
}

func (media *Media) loadStreams() error {
	mediaItems, err := database.Instance.FindMediaItemsByMedia(media.ID)
	if err != nil {
		log.WithField("inherit", err).Error("unable to load media items")
		return err
	}

	// load media streams
	for _, mediaItem := range mediaItems {
		mediaStreams, err := database.Instance.FindMediaStreamsByMediaItem(mediaItem.ID)
		if err != nil {
			log.WithField("inherit", err).Error("unable to load media items")
			return err
		}

		for _, mediaStream := range mediaStreams {
			media.addStream(mediaStream)
		}
	}

	return nil
}

func (api *API) serveMedia(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// parse media ID
	mediaIDString := info.Parameters["mediaID"]
	mediaID, err := strconv.Atoi(mediaIDString)
	if err != nil {
		log.WithFields(log.Fields{
			"mediaID": mediaIDString,
			"inherit": err,
		}).Error("unable to parse media ID")
		api.sendResponse(w, r, badRequestError)
		return
	}

	// parse load streams flag
	includeStreamsString := r.URL.Query().Get("includeStreams")
	includeStreams, _ := strconv.ParseBool(includeStreamsString)

	// load media entry
	media, err := api.GetMedia(int64(mediaID), includeStreams)
	if err != nil {
		api.sendResponse(w, r, internalServerError)
		return
	}

	// marshal and send response
	api.sendResponse(w, r, media)
}
