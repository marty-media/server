# API Changelog

## 0.5.0

- NEW: `usageType` field in `library_location` object
- CHANGE: `id` field of `playback` is now of type string instead of integer

## 0.4.0

- REMOVED: `hasDolbyTrueHDAudio` field of `media`.`streamAttributes`

## 0.3.0

- NEW: `language` field in `library` object
- NEW: `/library/languages` endpoint
- NEW: `type` S3 value for `library_location`
- NEW: `username`, `password` and `region` for `library_location`

## 0.2.0

- NEW: `createAt` field in `media` object
- NEW: `sort` parameter for `media` endpoint
- NEW: `/media/{mediaID}` endpoint

## 0.1.0

- initial release of API
