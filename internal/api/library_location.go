/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package api

import (
	"database/sql"
	"encoding/xml"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/marty-media/server/internal/backend"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/storage/s3"
)

type LibraryLocation struct {
	XMLName    xml.Name                          `json:"-" xml:"location"`
	ID         int64                             `json:"id" xml:"id"`
	LibraryID  int64                             `json:"-" xml:"-"`
	Type       database.LibraryLocationType      `json:"type" xml:"type"`
	UsageType  database.LibraryLocationUsageType `json:"usageType" xml:"usageType"`
	Location   string                            `json:"location" xml:"location"`
	User       *string                           `json:"user" xml:"user"`
	Password   *string                           `json:"password" xml:"password"`
	Region     *string                           `json:"region" xml:"region"`
	CreatedAt  time.Time                         `json:"createdAt" xml:"createdAt"`
	ModifiedAt *time.Time                        `json:"modifiedAt" xml:"modifiedAt"`
}

func newAPILibraryLocation(libraryLocation *database.LibraryLocation) LibraryLocation {
	response := LibraryLocation{
		ID:        libraryLocation.ID,
		LibraryID: libraryLocation.LibraryID,
		Type:      libraryLocation.Type,
		UsageType: libraryLocation.UsageType,
		Location:  libraryLocation.Location,
		CreatedAt: libraryLocation.CreatedAt,
	}

	if libraryLocation.User.Valid {
		response.User = &libraryLocation.User.String
	}

	if libraryLocation.Region.Valid {
		response.Region = &libraryLocation.Region.String
	}

	if libraryLocation.ModifiedAt.Valid {
		response.ModifiedAt = &libraryLocation.ModifiedAt.Time
	}

	return response
}

func (api *API) serveLibraryLocations(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// check library
	library := api.checkLibrary(w, r, info)
	if library == nil {
		return
	}

	// load library locations
	response, err := api.GetLibraryLocations(library.ID)
	if err != nil {
		api.sendResponse(w, r, internalServerError)
		return
	}

	// marshal and send response
	api.sendResponse(w, r, response)
}

func (api *API) GetLibraryLocations(libraryID int64) ([]LibraryLocation, error) {
	// load locations from database
	locations, err := database.Instance.GetLibraryLocations(libraryID)
	if err != nil {
		log.WithFields(log.Fields{
			"libraryID": libraryID,
			"inherit":   err,
		}).Error("unable to load library locations")
		return nil, err
	}

	// build API response
	response := []LibraryLocation{}
	for _, location := range locations {
		response = append(response, newAPILibraryLocation(location))
	}

	return response, err
}

func (api *API) serveLibraryLocationsPost(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// parse request
	location := parseRequestBody[LibraryLocation](api, w, r)
	if location == nil {
		return
	}

	// check library
	library := api.checkLibrary(w, r, info)
	if library == nil {
		return
	}

	// map used fields and validate them
	newLocation := database.LibraryLocation{
		LibraryID: library.ID,
		Type:      location.Type,
		UsageType: location.UsageType,
		Location:  location.Location,
	}
	if location.User != nil {
		newLocation.User = sql.NullString{String: *location.User, Valid: true}
	}
	if location.Password != nil {
		newLocation.Password = sql.NullString{String: *location.Password, Valid: true}
	}
	if location.Region != nil {
		newLocation.Region = sql.NullString{String: *location.Region, Valid: true}
	}

	if err := newLocation.Validate(); err != nil {
		api.sendResponse(w, r, Error{
			StatusCode: http.StatusBadRequest,
			Message:    err.Error(),
		})
		return
	}

	// run additional validations
	err := additionalLibraryLocationValidation(location)
	if err != nil {
		api.sendResponse(w, r, Error{
			StatusCode: http.StatusBadRequest,
			Message:    err.Error(),
		})
		return
	}

	// create location
	id, err := database.Instance.CreateLibraryLocation(newLocation)
	if err != nil {
		log.WithField("inherit", err).Error("unable to create new library location")
		api.sendResponse(w, r, internalServerError)
		return
	}

	// queue for scan of new location
	backend.ScannerInstance.NewLocationScan(id)

	// redirect to new ID
	w.Header().Add("location", fmt.Sprintf("/api/v1/libraries/%d/locations/%d", library.ID, id))
	w.WriteHeader(http.StatusCreated)
}

func additionalLibraryLocationValidation(location *LibraryLocation) error {
	// additional validation by type
	switch location.Type {
	case database.LibraryLocationTypeLocalFolder:
		// check, if folder exists
		info, err := os.Stat(location.Location)
		if os.IsNotExist(err) {
			return fmt.Errorf("location %s doesn't exist", location.Location)
		}

		// check, if location is a folder (not a file)
		if !info.IsDir() {
			return fmt.Errorf("location is invalid / not a directory")
		}
	case database.LibraryLocationTypeS3:
		// check S3 connection
		s3connection := location.GetS3()
		if s3connection == nil {
			return fmt.Errorf("unable to get S3 connection")
		}

		// list objects of S3 bucket
		_, err := s3connection.ListObjectsV2(s3.ListBucketV2Request{})
		if err != nil {
			return fmt.Errorf("unable to list objects of S3 bucket")
		}
	}

	return nil
}

func (api *API) GetLibraryLocation(locationID int64) (*LibraryLocation, error) {
	// load location from database
	location, err := database.Instance.GetLibraryLocation(locationID)
	if err != nil {
		log.WithFields(log.Fields{
			"locationID": locationID,
			"inherit":    err,
		}).Error("unable to load library location")
		return nil, err
	}

	// build API response
	var response *LibraryLocation
	if location != nil {
		apiLocation := newAPILibraryLocation(location)
		response = &apiLocation
	}

	return response, err
}

func (api *API) serveLibraryLocation(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// check location
	_, location := api.checkLibraryLocation(w, r, info)
	if location == nil {
		return
	}

	// send response
	api.sendResponse(w, r, location)
}

func (api *API) serveLibraryLocationDelete(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// load library location
	_, libraryLocation := api.checkLibraryLocation(w, r, info)
	if libraryLocation == nil {
		return
	}

	// delete library location
	err := database.Instance.DeleteLibraryLocation(libraryLocation.ID)
	if err != nil {
		api.sendResponse(w, r, internalServerError)
		return
	}

	// send response
	w.WriteHeader(http.StatusNoContent)
}

func (api *API) serveLibraryLocationRescan(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// load library location
	_, libraryLocation := api.checkLibraryLocation(w, r, info)
	if libraryLocation == nil {
		return
	}

	// enqueue library location for rescan
	backend.ScannerInstance.NewLocationScan(libraryLocation.ID)

	// send response
	api.sendResponse(w, r, acceptedResponse)
}

func (api *API) checkLibraryLocation(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) (*Library, *LibraryLocation) {
	// check library
	library := api.checkLibrary(w, r, info)
	if library == nil {
		return nil, nil
	}

	// parse request ID
	locationIDString := info.Parameters["locationID"]
	locationID, err := strconv.Atoi(locationIDString)
	if err != nil {
		log.WithFields(log.Fields{
			"locationID": locationIDString,
			"inherit":    err,
		}).Error("unable to parse library location ID")
		api.sendResponse(w, r, badRequestError)
		return nil, nil
	}

	// load library location
	location, err := api.GetLibraryLocation(int64(locationID))
	if err != nil {
		api.sendResponse(w, r, internalServerError)
		return nil, nil
	}

	// library found?
	if location == nil || location.LibraryID != library.ID {
		api.sendResponse(w, r, notFoundError)
		return nil, nil
	}

	return library, location
}

func (location *LibraryLocation) GetS3() *s3.S3 {
	request := s3.S3Request{
		Address:   location.Location,
		AccessKey: *location.User,
		SecretKey: *location.Password,
	}

	if location.Region != nil {
		request.Region = *location.Region
	}

	return s3.NewS3(request)
}
