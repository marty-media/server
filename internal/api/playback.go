/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package api

import (
	"encoding/xml"
	"fmt"
	log "github.com/sirupsen/logrus"
	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/marty-media/server/internal/common"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/hls"
	"net/http"
)

type PlaybackRequest struct {
	XMLName xml.Name `json:"-" xml:"playback"`
	MediaID int64    `json:"mediaID" xml:"mediaID"`
}

func (api *API) servePlaybackPost(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// parse request
	playbackRequest := parseRequestBody[PlaybackRequest](api, w, r)
	if playbackRequest == nil {
		return
	}

	// check, if media exists
	mediaID := playbackRequest.MediaID
	media, err := database.Instance.GetMedia(mediaID)
	if err != nil {
		log.WithFields(log.Fields{
			"mediaID": mediaID,
			"inherit": err,
		}).Error("unable to load media by ID")
		api.sendResponse(w, r, internalServerError)
		return
	} else if media == nil {
		log.WithField("mediaID", mediaID).Error("media not found")
		api.sendResponse(w, r, notFoundError)
		return
	}

	// generate new UID for playback
	uid, err := common.RandomHex(common.RandomLengthPlayback)
	if err != nil {
		log.WithField("error", err).Error("unable to generate playback UID")
		api.sendResponse(w, r, notFoundError)
		return
	}

	// register new playback
	_, err = database.Instance.CreatePlayback(database.Playback{
		UID:     uid,
		MediaID: mediaID,
	})
	if err != nil {
		log.WithField("error", err).Error("unable to create playback")
		api.sendResponse(w, r, internalServerError)
		return
	}

	// redirect to new stream ID
	w.Header().Add("location", fmt.Sprintf("/api/v1/playback/%s", uid))
	w.WriteHeader(http.StatusCreated)
}

type PlaybackResponse struct {
	XMLName   xml.Name `json:"-" xml:"playback"`
	ID        string   `json:"id" xml:"id"`
	MediaID   int64    `json:"mediaID" xml:"mediaID"`
	StreamURL string   `json:"streamURL" xml:"streamURL"`
}

func (api *API) servePlayback(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// get playback
	playback := api.checkPlayback(w, r, info)
	if playback == nil {
		return
	}

	// build streaming URL
	streamURL := fmt.Sprintf("/playback/%s/%s", playback.UID, hls.MainPlaylistName)

	// build response
	response := PlaybackResponse{
		ID:        playback.UID,
		MediaID:   playback.MediaID,
		StreamURL: streamURL,
	}

	// send response
	api.sendResponse(w, r, response)
}

func (api *API) checkPlayback(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) *database.Playback {
	// parse request ID
	playbackUID := info.Parameters["playbackUID"]

	// load playback
	playback, err := database.Instance.FindPlaybackByUID(playbackUID)
	if err != nil {
		log.WithField("error", err).Error("unable to find playback by UID")
		api.sendResponse(w, r, internalServerError)
		return nil
	}

	// playback found?
	if playback == nil {
		api.sendResponse(w, r, notFoundError)
		return nil
	}

	return playback
}
