/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package api

import (
	"net/http"
	"testing"
)

func TestServerBandwidth(t *testing.T) {
	// test scopes
	testServerBandwidth(t, "minute")
	testServerBandwidth(t, "hour")

	// test invalid scope
	callHTTP(t, http.MethodGet, "/api/v1/server/bandwidth?scope=asdf", nil, "json", http.StatusBadRequest, nil)
}

func testServerBandwidth(t *testing.T, scope string) {
	var responseJSON map[string]any
	callHTTP(t, http.MethodGet, "/api/v1/server/bandwidth?scope="+scope, nil, "json", http.StatusOK, &responseJSON)
	callHTTP(t, http.MethodGet, "/api/v1/server/bandwidth?scope="+scope, nil, "xml", http.StatusOK, nil)
	if responseJSON["outgoing"] == nil {
		t.Error("missing response data", responseJSON)
	}
}

func TestServerInfo(t *testing.T) {
	var responseJSON map[string]any
	callHTTP(t, http.MethodGet, "/api/v1/server/info", nil, "json", http.StatusOK, &responseJSON)
	callHTTP(t, http.MethodGet, "/api/v1/server/info", nil, "xml", http.StatusOK, nil)
	if responseJSON["version"] == nil || responseJSON["apiVersion"] == nil {
		t.Error("missing response data", responseJSON)
	}
}
