/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package api

import (
	"encoding/xml"
	"net/http"

	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/marty-media/server/internal/backend"
	"gitlab.com/marty-media/server/internal/version"
)

type BandwidthResponse struct {
	XMLName  xml.Name `json:"-" xml:"bandwidth"`
	Outgoing []int64  `json:"outgoing" xml:"outgoing"`
}

func (api *API) serveServerBandwidth(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// check parameter for scope (live, last hour, ...)
	requestedScope := r.URL.Query().Get("scope")
	var data []int64
	switch requestedScope {
	case "minute":
		data = backend.BandwidthInstance.StatsLive()
	case "hour":
		data = backend.BandwidthInstance.StatsHour()
	default:
		api.sendResponse(w, r, Error{
			StatusCode: http.StatusBadRequest,
			Message:    "invalid value for parameter 'scope'",
		})
		return
	}

	// build and send response
	response := BandwidthResponse{
		Outgoing: data,
	}
	api.sendResponse(w, r, response)
}

type ServerInfo struct {
	XMLName    xml.Name `json:"-" xml:"info"`
	Version    string   `json:"version" xml:"version"`
	APIVersion string   `json:"apiVersion" xml:"apiVersion"`
}

func (api *API) serveServerInfo(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	response := ServerInfo{
		Version:    version.Version,
		APIVersion: Version,
	}
	api.sendResponse(w, r, response)
}
