/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package api

import (
	"embed"
	"encoding/json"
	"encoding/xml"
	"io"
	"net/http"
	"reflect"
	"sort"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/marty-media/server/internal/webserver"
)

const (
	ContentTypeApplicationJSON = "application/json"
	ContentTypeApplicationXML  = "application/xml"
)

var Instance = &API{}

//go:embed documentation/*.json
var apiDocStaticFiles embed.FS

//go:embed documentation/swagger/*
var swaggerStaticFiles embed.FS

type API struct {
}

func (api *API) Prepare(webServer *webserver.WebServer) {
	// library API
	const librariesAPI = "/api/v1/libraries"
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, librariesAPI, api.serveLibraries))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodPost, librariesAPI, api.serveLibrariesPost))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, librariesAPI+"/languages", api.serveLibrariesLanguages))
	const libraryAPI = librariesAPI + "/:libraryID"
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, libraryAPI, api.serveLibrary))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodPatch, libraryAPI, api.serveLibraryPatch))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodDelete, libraryAPI, api.serveLibraryDelete))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodPost, libraryAPI+"/rescan", api.serveLibraryRescan))

	// library location API
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, librariesAPI+"/:libraryID/locations", api.serveLibraryLocations))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodPost, librariesAPI+"/:libraryID/locations", api.serveLibraryLocationsPost))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, librariesAPI+"/:libraryID/locations/:locationID", api.serveLibraryLocation))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodDelete, librariesAPI+"/:libraryID/locations/:locationID", api.serveLibraryLocationDelete))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodPost, librariesAPI+"/:libraryID/locations/:locationID/rescan", api.serveLibraryLocationRescan))

	// media API
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/api/v1/media", api.serveMediaList))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/api/v1/media/:mediaID", api.serveMedia))

	// playback API
	httprouter.Must(webServer.Router.HandleFunc(http.MethodPost, "/api/v1/playback", api.servePlaybackPost))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/api/v1/playback/:playbackUID", api.servePlayback))
	//httprouter.Must(webServer.Router.HandleFunc(http.MethodPost, "/api/v1/playback/:playbackUID/progress",))

	// server API
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/api/v1/server/bandwidth", api.serveServerBandwidth))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/api/v1/server/info", api.serveServerInfo))

	// documentation redirects
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/apidoc", api.redirect("/apidoc/v1")))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/apidoc/v1", api.redirect("/apidoc/v1/swagger")))
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/apidoc/v1/swagger", api.redirect("/apidoc/v1/swagger/index.html")))

	// serve static files
	staticFS := httprouter.NewFS(&apiDocStaticFiles)
	staticFS.FolderPrefix = "documentation"
	staticFS.LocalFolderPrefix = "internal/api"
	staticFS.UseLocalFolder = webServer.DeveloperMode
	httprouter.Must(webServer.Router.Handle(http.MethodGet, "/apidoc/v1/*", staticFS))

	// serve static swagger-ui files
	swaggerStaticFS := httprouter.NewFS(&swaggerStaticFiles)
	swaggerStaticFS.FolderPrefix = "documentation/swagger"
	swaggerStaticFS.LocalFolderPrefix = "internal/api"
	swaggerStaticFS.UseLocalFolder = webServer.DeveloperMode
	httprouter.Must(webServer.Router.Handle(http.MethodGet, "/apidoc/v1/swagger/*", swaggerStaticFS))
}

type ResponseStatus interface {
	StatusNumber() int
}

type GenericResponse struct {
	XMLName    xml.Name `json:"-" xml:"response"`
	StatusCode int      `json:"status" xml:"statusCode"`
	Message    string   `json:"message" xml:"message"`
}

func (genericResponse GenericResponse) StatusNumber() int {
	return genericResponse.StatusCode
}

var acceptedResponse = GenericResponse{
	StatusCode: http.StatusAccepted,
	Message:    "accepted",
}

type Error GenericResponse

func (err Error) StatusNumber() int {
	return err.StatusCode
}

var notFoundError = Error{
	StatusCode: http.StatusNotFound,
	Message:    "not found",
}

var badRequestError = Error{
	StatusCode: http.StatusBadRequest,
	Message:    "bad request",
}

var internalServerError = Error{
	StatusCode: http.StatusInternalServerError,
	Message:    "internal server error",
}

type responseWrapper struct {
	XMLName xml.Name `json:"-" xml:"response"`
	Content any
}

func (api *API) sendResponse(w http.ResponseWriter, r *http.Request, content any) {
	// check for nil response
	if reflect.TypeOf(content).Kind() == reflect.Pointer && reflect.ValueOf(content).IsNil() {
		api.sendResponse(w, r, notFoundError)
		return
	}

	// get requested response format
	acceptHeader := strings.ToLower(r.Header.Get("accept"))

	// send response in requested format
	var data []byte
	switch acceptHeader {
	case "*/*":
		acceptHeader = ContentTypeApplicationJSON
		fallthrough
	case ContentTypeApplicationJSON:
		jsonData, err := json.Marshal(content)
		if err != nil {
			log.WithField("inherit", err).Error("unable to marshal JSON response")
			api.sendResponse(w, r, internalServerError)
			return
		}
		data = jsonData
	case ContentTypeApplicationXML:
		// arrays needs a wrapper
		xmlConent := content
		if reflect.TypeOf(xmlConent).Kind() == reflect.Slice {
			xmlConent = responseWrapper{
				Content: xmlConent,
			}
		}

		// marshal XML
		xmlData, err := xml.Marshal(xmlConent)
		if err != nil {
			log.WithField("inherit", err).Error("unable to marshal XML response")
			api.sendResponse(w, r, internalServerError)
			return
		}
		data = []byte(xml.Header)
		data = append(data, xmlData...)
	default:
		r.Header.Set("accept", ContentTypeApplicationJSON)
		api.sendResponse(w, r, Error{
			StatusCode: http.StatusUnsupportedMediaType,
			Message:    "invalid accept header",
		})
		return
	}

	// set additional header
	w.Header().Add("Content-Type", acceptHeader)

	// set status code
	if responseStatus, ok := content.(ResponseStatus); ok {
		w.WriteHeader(responseStatus.StatusNumber())
	}

	// send response
	if _, err := w.Write(data); err != nil {
		log.WithField("inherit", err).Error("unable to send response")
	}
}

func (api *API) redirect(target string) httprouter.RouteHandler {
	return func(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
		http.Redirect(w, r, target, http.StatusTemporaryRedirect)
	}
}

type genericSorting struct {
	elements []sortElement
}

func runGenericSort[T any](sorting genericSorting, data []T) {
	sort.SliceStable(data, func(i, j int) bool {
		a := data[i]
		b := data[j]

		for _, elm := range sorting.elements {
			isEqual, sorting := runGenericSortField(a, b, elm)
			if isEqual {
				// check next sorting priority
				continue
			}

			// invert sorting for descending order
			if elm.descending {
				return !sorting
			}
			return sorting
		}

		return false
	})
}

func runGenericSortField(a any, b any, elm sortElement) (isEqual bool, sort bool) {
	// extract fields
	aReflect := reflect.ValueOf(a).FieldByName(elm.field.Name)
	bReflect := reflect.ValueOf(b).FieldByName(elm.field.Name)

	// compare based on data type
	dataType := aReflect.Type().Name()
	switch dataType {
	case "int64":
		return aReflect.Int() == bReflect.Int(), aReflect.Int() < bReflect.Int()
	case "string":
		return aReflect.String() == bReflect.String(), aReflect.String() < bReflect.String()
	case "Time":
		t1 := aReflect.Interface().(time.Time)
		t2 := bReflect.Interface().(time.Time)
		return t1.Equal(t2), t1.Before(t2)
	default:
		log.Error("invalid data type for generic sorting ", dataType)
		return true, false
	}
}

type sortElement struct {
	field      reflect.StructField
	publicName string
	descending bool
}

func parseSorting(api *API, w http.ResponseWriter, r *http.Request, defaultSorting string, t any) *genericSorting {
	// parse possible fields
	sortableFields := parseSortableFields(t)

	// parse URL query string
	sortQuery := r.URL.Query().Get("sort")

	// check sorting request
	sorting := parseSortingString(api, w, r, sortQuery, sortableFields)
	if sorting == nil {
		// http response already sent
		return nil
	} else if len(sorting.elements) > 0 {
		return sorting
	}

	// return default sorting
	defaultSort := parseSortingString(api, w, r, defaultSorting, sortableFields)
	return defaultSort
}

func parseSortingString(api *API, w http.ResponseWriter, r *http.Request, sortString string, sortableFields []sortElement) *genericSorting {
	// multiple sorting are possible
	sortParts := strings.Split(sortString, ",")

	// check each sorting entry
	sortElements := []sortElement{}
	for _, sortEntry := range sortParts {
		// check, if entry is empty
		if len(sortEntry) == 0 {
			continue
		}

		// build response fields
		fieldName := sortEntry
		descending := false

		// check for asc/desc suffix
		if pos := strings.LastIndex(fieldName, ":asc"); pos > 0 {
			fieldName = fieldName[:pos]
		} else if pos := strings.LastIndex(fieldName, ":desc"); pos > 0 {
			fieldName = fieldName[:pos]
			descending = true
		}

		// validate field name
		var field *sortElement
		for _, sortableField := range sortableFields {
			if sortableField.publicName == fieldName {
				field = &sortableField
				break
			}
		}
		if field == nil {
			api.sendResponse(w, r, badRequestError)
			return nil
		}

		// build response
		sortElements = append(sortElements, sortElement{
			field:      field.field,
			publicName: field.publicName,
			descending: descending,
		})
	}

	return &genericSorting{
		elements: sortElements,
	}
}

func parseSortableFields(t any) []sortElement {
	tpe := reflect.TypeOf(t)
	response := []sortElement{}
	for i := 0; i < tpe.NumField(); i++ {
		currentField := tpe.Field(i)

		// parse tags
		tagValue, tagFound := currentField.Tag.Lookup("mmsSortable")
		if !tagFound {
			continue
		}

		// check, if field is sortable
		if tagValue != "true" {
			continue
		}

		// parse public JSON name
		// TODO: handle tag attributes
		jsonTagValue, jsonTagFound := currentField.Tag.Lookup("json")
		if !jsonTagFound {
			continue
		}

		response = append(response, sortElement{
			field:      currentField,
			publicName: jsonTagValue,
		})
	}

	return response
}

func parseRequestBody[T any](api *API, w http.ResponseWriter, r *http.Request) *T {
	// read body
	bodyData, err := io.ReadAll(r.Body)
	if err != nil {
		log.WithField("inherit", err).Error("reading request body failed")
		api.sendResponse(w, r, internalServerError)
		return nil
	}

	// get request body format
	contentType := r.Header.Get("content-type")

	// parse request
	var obj T
	switch contentType {
	case ContentTypeApplicationJSON:
		if err := json.Unmarshal(bodyData, &obj); err != nil {
			log.WithField("inherit", err).Error("unable to unmarshal request body JSON")
			api.sendResponse(w, r, badRequestError)
			return nil
		}
	case ContentTypeApplicationXML:
		if err := xml.Unmarshal(bodyData, &obj); err != nil {
			log.WithField("inherit", err).Error("unable to unmarshal request body XML")
			api.sendResponse(w, r, badRequestError)
			return nil
		}
	default:
		api.sendResponse(w, r, badRequestError)
		return nil
	}

	return &obj
}
