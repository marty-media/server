/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package api

import (
	"encoding/json"
	"encoding/xml"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"strings"
	"testing"

	"gitlab.com/marty-media/server/internal/backend"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/webserver"
)

func TestMain(m *testing.M) {
	// init database
	if err := database.InitDatabase(); err != nil {
		log.Println("unable to initialize database")
		os.Exit(1)
	}
	if err := database.Instance.Reset(); err != nil {
		log.Println("unable to clear database", err)
		os.Exit(1)
	}
	if err := database.Instance.Upgrade(); err != nil {
		log.Println("unable to prepare database (upgrade)")
		os.Exit(1)
	}

	// init backend/webserver
	backend.Instance.Start()
	webserver.Instance.Prepare([]webserver.ServerInterface{
		Instance,
	})

	// run tests
	response := m.Run()
	os.Exit(response)
}

func TestInvalidAcceptHeader(t *testing.T) {
	// build request
	request, _ := http.NewRequest(http.MethodGet, "/api/v1/server/info", nil)
	request.Header.Add("accept", "not/supported")

	// run API call
	response := httptest.NewRecorder()
	webserver.Instance.ServeHTTP(response, request)

	// check status code
	if response.Result().StatusCode != http.StatusUnsupportedMediaType {
		t.Error("invalid response status code for unsupported accept header", response.Result().StatusCode)
	}
}

func callHTTP(t *testing.T, method string, url string, body io.Reader, contentType string, statusCode int, response any) {
	// build API call
	request, _ := http.NewRequest(method, url, body)
	switch contentType {
	case "json":
		request.Header.Add("Content-Type", "application/json")
		request.Header.Add("accept", "application/json")
	case "xml":
		request.Header.Add("Content-Type", "application/xml")
		request.Header.Add("accept", "application/xml")
	default:
		t.Error("invalid content type", contentType)
	}

	// run API call
	httpResponse := httptest.NewRecorder()
	webserver.Instance.ServeHTTP(httpResponse, request)

	// parse response
	responseData, err := io.ReadAll(httpResponse.Body)
	if err != nil {
		t.Fatal("unable to parse http response", err)
	}

	// check status code
	if httpResponse.Result().StatusCode != statusCode {
		t.Fatalf("invalid http response code for API '%s'; status %d; expected %d; method: %s, contentType: %s; content: %s", url, httpResponse.Result().StatusCode, statusCode, method, contentType, string(responseData))
	}

	// check created header
	if statusCode == http.StatusCreated || statusCode == http.StatusNoContent {
		if response == nil {
			return
		}

		// parse new location header
		newLocation := httpResponse.Result().Header.Get("location")
		*(response.(*string)) += newLocation
		return
	}

	// parse response
	switch contentType {
	case "json":
		if err := json.Unmarshal(responseData, &response); err != nil {
			t.Error(err)
		}
	case "xml":
		if err := xml.Unmarshal(responseData, &response); err != nil {
			t.Error(err)
		}
	}
}

func parseRedirectHeader(t *testing.T, val string, prefix string) int64 {
	// split ID based on prefix
	parts := strings.SplitAfter(val, prefix)
	if len(parts) != 2 {
		t.Fatal("retrieved invalid location header", val, prefix, parts)
	}

	// parse ID
	id, err := strconv.Atoi(parts[1])
	if err != nil {
		t.Fatal("unable to parse ID from location header", val, parts[1])
	}

	return int64(id)
}
