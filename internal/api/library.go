/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package api

import (
	"encoding/xml"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/marty-media/server/internal/backend"
	"gitlab.com/marty-media/server/internal/common"
	"gitlab.com/marty-media/server/internal/database"
)

type Library struct {
	XMLName    xml.Name             `json:"-" xml:"library"`
	ID         int64                `json:"id" xml:"id"`
	Name       string               `json:"name" xml:"name"`
	Type       database.LibraryType `json:"type" xml:"type"`
	Language   string               `json:"language" xml:"language"`
	CreatedAt  time.Time            `json:"createdAt" xml:"createdAt"`
	ModifiedAt *time.Time           `json:"modifiedAt" xml:"modifiedAt"`
	Locations  []LibraryLocation    `json:"locations,omitempty" xml:"locations"`
}

type Language struct {
	XMLName xml.Name `json:"-" xml:"language"`
	Key     string   `json:"key" xml:"key"`
	Name    string   `json:"name" xml:"name"`
}

type patchLibrary struct {
	Name *string `json:"name" xml:"name"`
}

func newAPILibrary(library *database.Library) Library {
	response := Library{
		ID:        library.ID,
		Name:      library.Name,
		Type:      library.Type,
		Language:  library.Language,
		CreatedAt: library.CreatedAt,
	}

	if library.ModifiedAt.Valid {
		response.ModifiedAt = &library.ModifiedAt.Time
	}

	return response
}

func newLanguage(language common.ISO639) Language {
	return Language{
		Key:  language.ISO639_3,
		Name: language.Name,
	}
}

func (api *API) serveLibrariesLanguages(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// build response
	response := []Language{}
	for _, lang := range common.ISO639_3 {
		response = append(response, newLanguage(lang))
	}

	// sort response
	sort.Slice(response, func(i, j int) bool {
		return response[i].Name < response[j].Name
	})

	// send response
	api.sendResponse(w, r, response)
}

func (api *API) serveLibraries(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// load libraries
	response, err := api.GetLibraries()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// marshal and send response
	api.sendResponse(w, r, response)
}

func (api *API) GetLibraries() ([]Library, error) {
	libraries, err := database.Instance.GetLibraries()
	if err != nil {
		log.WithField("inherit", err).Error("unable to load library entries")
		return nil, err
	}

	// build API response
	response := []Library{}
	for _, library := range libraries {
		response = append(response, newAPILibrary(library))
	}

	return response, err
}

func (api *API) serveLibrariesPost(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// parse request
	library := parseRequestBody[Library](api, w, r)
	if library == nil {
		return
	}

	// map used fields and validate them
	newLibrary := database.Library{
		Name:     library.Name,
		Type:     library.Type,
		Language: library.Language,
	}
	if err := newLibrary.Validate(); err != nil {
		api.sendResponse(w, r, Error{
			StatusCode: http.StatusBadRequest,
			Message:    err.Error(),
		})
		return
	}

	// create library
	id, err := database.Instance.CreateLibrary(newLibrary)
	if err != nil {
		log.WithField("inherit", err).Error("unable to create new library")
		api.sendResponse(w, r, internalServerError)
		return
	}

	// redirect to new ID
	w.Header().Add("location", fmt.Sprintf("/api/v1/libraries/%d", id))
	w.WriteHeader(http.StatusCreated)
}

func (api *API) serveLibrary(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// load library
	library := api.checkLibrary(w, r, info)
	if library == nil {
		return
	}

	// check for including locations
	includeLocationsString := r.URL.Query().Get("includeLocations")
	if includeLocations, _ := strconv.ParseBool(includeLocationsString); includeLocations {
		locations, err := api.GetLibraryLocations(library.ID)
		if err != nil {
			api.sendResponse(w, r, internalServerError)
			return
		}
		library.Locations = locations
	}

	// marshal and send response
	api.sendResponse(w, r, library)
}

func (api *API) GetLibrary(id int64) (*Library, error) {
	// load library
	library, err := database.Instance.GetLibrary(id)
	if err != nil {
		log.WithField("inherit", err).Error("unable to load library")
		return nil, err
	}

	// build API response
	var response *Library
	if library != nil {
		apiLibrary := newAPILibrary(library)
		response = &apiLibrary
	}

	return response, nil
}

func (api *API) serveLibraryPatch(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// load library
	library := api.checkLibrary(w, r, info)
	if library == nil {
		return
	}

	// parse request
	libraryPatch := parseRequestBody[patchLibrary](api, w, r)
	if libraryPatch == nil {
		return
	}

	// execute update
	newName := ""
	updateName := false
	if libraryPatch.Name != nil {
		newName = *libraryPatch.Name
		updateName = true
	}
	if err := database.Instance.UpdateLibrary(library.ID, newName, updateName); err != nil {
		log.WithField("inherit", err).Error("unable to update library")
		api.sendResponse(w, r, internalServerError)
		return
	}

	// executed successfully
	w.Header().Add("location", fmt.Sprintf("/api/v1/libraries/%d", library.ID))
	w.WriteHeader(http.StatusNoContent)
}

func (api *API) serveLibraryDelete(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// load library
	library := api.checkLibrary(w, r, info)
	if library == nil {
		return
	}

	// delete library
	err := database.Instance.DeleteLibrary(library.ID)
	if err != nil {
		api.sendResponse(w, r, internalServerError)
		return
	}

	// send response
	w.WriteHeader(http.StatusNoContent)
}

func (api *API) serveLibraryRescan(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// load library
	library := api.checkLibrary(w, r, info)
	if library == nil {
		return
	}

	// load locations
	locations, err := api.GetLibraryLocations(library.ID)
	if err != nil {
		api.sendResponse(w, r, internalServerError)
		return
	}

	// create new scan request for each location
	for _, location := range locations {
		backend.ScannerInstance.NewLocationScan(location.ID)
	}

	// send response
	api.sendResponse(w, r, acceptedResponse)
}

func (api *API) checkLibrary(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) *Library {
	// parse request ID
	libraryIDString := info.Parameters["libraryID"]
	libraryID, err := strconv.Atoi(libraryIDString)
	if err != nil {
		log.WithFields(log.Fields{
			"libraryID": libraryIDString,
			"inherit":   err,
		}).Error("unable to parse library ID")
		api.sendResponse(w, r, badRequestError)
		return nil
	}

	// load library
	library, err := api.GetLibrary(int64(libraryID))
	if err != nil {
		api.sendResponse(w, r, internalServerError)
		return nil
	}

	// library found?
	if library == nil {
		api.sendResponse(w, r, notFoundError)
		return nil
	}

	return library
}
