/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package common

import (
	_ "embed"
	"encoding/json"
)

// List of ISO639 codes
// Filter by Language Type = "Living"
// https://iso639-3.sil.org/code_tables/639/data

//go:embed iso639.json
var iso639Data string
var ISO639_2 = parseISO639_2()
var ISO639_3 = parseISO639_3()

type ISO639 struct {
	ISO639_1      string `json:"iso639-1"`
	ISO639_2T     string `json:"iso639-2T"`
	ISO639_2B     string `json:"iso639-2B"`
	ISO639_3      string `json:"iso639-3"`
	Name          string `json:"name"`
	LocalizedName string `json:"localizedName"`
}

func parseJSON() []ISO639 {
	var response []ISO639
	if err := json.Unmarshal([]byte(iso639Data), &response); err != nil {
		panic(err)
	}
	return response
}

func parseISO639_2() map[string]ISO639 {
	data := parseJSON()

	// filter not relevant entries
	response := map[string]ISO639{}
	for _, lang := range data {
		if entry, found := response[lang.ISO639_2T]; found && entry.Name != lang.Name {
			panic("duplicate entry for " + lang.ISO639_2T)
		}
		response[lang.ISO639_2T] = lang
		if entry, found := response[lang.ISO639_2B]; found && entry.Name != lang.Name {
			panic("duplicate entry for " + lang.ISO639_2B)
		}
		response[lang.ISO639_2B] = lang
	}

	return response
}

func parseISO639_3() map[string]ISO639 {
	data := parseJSON()

	// filter not relevant entries
	response := map[string]ISO639{}
	for _, lang := range data {
		response[lang.ISO639_3] = lang
	}

	return response
}
