/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package common

import "testing"

func TestRandomHex(t *testing.T) {
	t1, err := RandomHex(RandomLengthMedia)
	if err != nil {
		t.Error(err)
	}
	t2, err := RandomHex(RandomLengthMedia)
	if err != nil {
		t.Error(err)
	}
	if t1 == t2 {
		t.Error("random is not random")
	}
}

func TestRandomInt(t *testing.T) {
	t1, err := RandomInt(100_000)
	if err != nil {
		t.Error(err)
	}
	t2, err := RandomInt(100_000)
	if err != nil {
		t.Error(err)
	}
	if t1 == t2 {
		t.Error("random is not random", t1, t2)
	}
}
