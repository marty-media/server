/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package common

import "testing"

func TestISO639(t *testing.T) {
	for _, lang := range ISO639_3 {
		// check by testing german language
		if lang.Name != "German" {
			continue
		}

		// check values
		if lang.ISO639_1 != "de" {
			t.Error("invalid ISO639_1 value for german language", lang.ISO639_1)
		}
		if lang.ISO639_3 != "deu" {
			t.Error("invalid ISO639_3 value for german language", lang.ISO639_3)
		}
		break
	}
}
