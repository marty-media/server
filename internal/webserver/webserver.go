/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package webserver

import (
	"bytes"
	"compress/gzip"
	"context"
	"errors"
	"net/http"
	"slices"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/marty-media/server/internal/settings"
	"golang.org/x/sync/errgroup"
)

var Instance = &WebServer{}
var developMode = false

type WebServer struct {
	DeveloperMode bool
	Router        *httprouter.Router
	http          *http.Server
	server        []ServerInterface
	cancel        context.CancelFunc
}

type ServerInterface interface {
	Prepare(*WebServer)
}

func (webServer *WebServer) Prepare(server []ServerInterface) {
	webServer.DeveloperMode = developMode
	webServer.Router = httprouter.New()
	webServer.server = server

	// add root folder redirect
	webServer.Router.HandleFunc(http.MethodGet, "/", webServer.serveRoot)

	// register all URLs
	for _, server := range webServer.server {
		server.Prepare(webServer)
	}
}

func (webServer *WebServer) Run() error {
	listen := settings.Instance.GetWebServerListen()
	log.WithField("listen", listen).Info("starting webserver")

	// create new web server instance
	webServer.http = &http.Server{
		Addr:    listen,
		Handler: webServer,
	}

	// start background handling
	ctx, cancel := context.WithCancel(context.Background())
	webServer.cancel = cancel
	g, gCtx := errgroup.WithContext(ctx)

	// start server
	g.Go(func() error {
		defer webServer.cancel()
		err := webServer.http.ListenAndServe()
		if !errors.Is(err, http.ErrServerClosed) {
			return err
		}
		return nil
	})
	// register shutdown
	g.Go(func() error {
		// wait for closed context
		<-gCtx.Done()

		// create new context for HTTP timeout handling (wait 10 seconds; then close anyway)
		timeoutContext, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		return webServer.http.Shutdown(timeoutContext)
	})

	// wait until webserver closes
	if err := g.Wait(); err != nil {
		log.WithField("inherit", err).Error("error in webserver")
		return err
	}
	return nil
}

func (webServer *WebServer) Shutdown() {
	// send cancel request
	webServer.cancel()
}

func (webServer *WebServer) serveRoot(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	http.Redirect(w, r, "/web", http.StatusTemporaryRedirect)
}

func WriteGzip(w http.ResponseWriter, r *http.Request, data []byte) (int, error) {
	content := data

	// check for gzip compression
	acceptEncodingHeader := strings.Split(r.Header.Get("Accept-Encoding"), ",")
	gzipRequested := slices.ContainsFunc(acceptEncodingHeader, func(s string) bool {
		return strings.TrimSpace(strings.ToLower(s)) == "gzip"
	})
	if gzipRequested {
		// compress data
		var buf bytes.Buffer
		writer := gzip.NewWriter(&buf)
		_, err := writer.Write(content)
		if err != nil {
			return 0, err
		}

		// close writer
		if err := writer.Close(); err != nil {
			return 0, err
		}
		content = buf.Bytes()

		// send gzip compressed data
		w.Header().Set("Content-Encoding", "gzip")
	}

	// send response
	return w.Write(content)
}
