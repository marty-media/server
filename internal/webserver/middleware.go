/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package webserver

import (
	"bufio"
	"net"
	"net/http"

	"gitlab.com/marty-media/server/internal/backend"
)

func (webServer *WebServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// check, if the response writer supports the hijacker interface
	var responseWriter http.ResponseWriter
	if _, ok := w.(http.Hijacker); ok {
		responseWriter = CustomWriterHijack{
			w: w,
		}
	} else {
		responseWriter = CustomWriter{
			w: w,
		}
	}

	webServer.Router.ServeHTTP(responseWriter, r)
}

type CustomWriter struct {
	w http.ResponseWriter
}

func (w CustomWriter) Header() http.Header {
	return w.w.Header()
}

func (w CustomWriter) Write(b []byte) (int, error) {
	middlewareWriter(len(b))
	return w.w.Write(b)
}

func (w CustomWriter) WriteHeader(statusCode int) {
	w.w.WriteHeader(statusCode)
}

type CustomWriterHijack struct {
	w http.ResponseWriter
}

func (w CustomWriterHijack) Header() http.Header {
	return w.w.Header()
}

func (w CustomWriterHijack) Write(b []byte) (int, error) {
	middlewareWriter(len(b))
	return w.w.Write(b)
}

func (w CustomWriterHijack) WriteHeader(statusCode int) {
	w.w.WriteHeader(statusCode)
}

func (w CustomWriterHijack) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	return w.w.(http.Hijacker).Hijack()
}

func middlewareWriter(bytes int) {
	backend.BandwidthInstance.AddOutgoing(int64(bytes))
}
