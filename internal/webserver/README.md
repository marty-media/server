# WebServer
## Frontend (`/web`)
This is the entry point for the WebUI of the server. If you do not enter any path you will be redirected to this URL.

## API (`/api`)
### v1 (`/api/v1`)
Documentation of the API via Swagger-UI available under `/apidoc/v1/swagger/index.html`.

## For Developers
Use the build flag `-tags="develop"` to load local files of static content (no restart / rebuild of server is necessary).
