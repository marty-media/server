/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package settingsdb

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/marty-media/server/internal/database"
)

var Instance = &SettingsDB{}

type SettingsDB struct {
}

func (settings *SettingsDB) RefreshDebugLog() error {
	defer log.WithField("level", log.GetLevel()).Debug("log level updated")

	// load current setting
	setting, err := database.Instance.GetSettingDebugLog()
	if err != nil {
		log.WithField("inherit", err).Error("unable to load setting for debug log")
		return err
	}

	// check for debug level
	if setting == nil || setting.Value != "true" {
		log.SetLevel(log.InfoLevel)
		return nil
	}

	log.SetLevel(log.DebugLevel)
	return nil
}
