/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package settingsdb

import (
	"log"
	"os"
	"testing"

	"gitlab.com/marty-media/server/internal/database"
)

func TestMain(m *testing.M) {
	// prepare database
	if err := database.InitDatabase(); err != nil {
		log.Println("unable to initialize database", err)
		os.Exit(1)
	}
	if err := database.Instance.Upgrade(); err != nil {
		log.Println("unable to run database upgrade", err)
		os.Exit(1)
	}

	// run tests
	os.Exit(m.Run())
}

func TestDebugLog(t *testing.T) {
	if err := Instance.RefreshDebugLog(); err != nil {
		t.Error("refreshing debug log failed", err)
	}
}
