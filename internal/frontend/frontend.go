/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package frontend

import (
	"embed"
	"net/http"

	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/marty-media/server/internal/webserver"
)

var Instance = &Frontend{}

//go:embed gui/dist
var staticFiles embed.FS

type Frontend struct {
}

func (frontend *Frontend) Prepare(webServer *webserver.WebServer) {
	// add redirects
	httprouter.Must(webServer.Router.HandleFunc(http.MethodGet, "/web", frontend.serveRoot))

	// serve static files
	staticFS := httprouter.NewFS(&staticFiles)
	staticFS.FolderPrefix = "gui/dist"
	httprouter.Must(webServer.Router.Handle(http.MethodGet, "/web/*", staticFS))
}

func (frontend *Frontend) serveRoot(w http.ResponseWriter, r *http.Request, _ httprouter.RoutingInfo) {
	http.Redirect(w, r, "/web/index.html", http.StatusTemporaryRedirect)
}
