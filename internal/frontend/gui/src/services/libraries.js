/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
import { del, get, patch, post } from "../services/base";

export const Types = {
  Movie: {
    ID: 1,
    Order: 1,
    Name: "Movie"
  },
  TVShow: {
    ID: 2,
    Order: 2,
    Name: "TV-Show"
  }
};

export const LocationTypes = {
  LocalFolder: {
    ID: 1,
    Order: 1,
    Name: "Local Folder"
  },
  S3Bucket: {
    ID: 2,
    Order: 2,
    Name: "S3 Bucket"
  }
}

export async function getLibraries() {
  return get({
    url: '/api/v1/libraries'
  });
};

export async function getLibrary({ id, includeLocations }) {
  let params = new URLSearchParams();
  if (includeLocations) {
    params.append("includeLocations", includeLocations)
  }
  return get({
    url: '/api/v1/libraries/' + id + "?" + params
  });
};

export async function createLibrary({ name, type, language }) {
  return post({
    url: '/api/v1/libraries'
  }, {
    name: name,
    type: type,
    language: language
  });
};

export async function updateLibrary(id, name) {
  return patch({
    url: '/api/v1/libraries/' + id
  }, {
    name: name
  });
};

export async function deleteLibrary(id) {
  return del({
    url: '/api/v1/libraries/' + id
  });
};

export async function getLanguages() {
  return get({
    url: '/api/v1/libraries/languages'
  });
};

export async function createLocation({ libraryID, type, location, user, password, region }) {
  let data = {
    type: type,
    location: location
  };
  if (user) {
    data.user = user;
  }
  if (password) {
    data.password = password;
  }
  if (region) {
    data.region = region;
  }
  return post({
    url: '/api/v1/libraries/' + libraryID + '/locations'
  }, data);
};

export async function deleteLocation({ libraryID, id }) {
  return del({
    url: '/api/v1/libraries/' + libraryID + '/locations/' + id
  });
};

export async function rescanLocation({ libraryID, id }) {
  return post({
    url: '/api/v1/libraries/' + libraryID + '/locations/' + id + '/rescan'
  });
};
