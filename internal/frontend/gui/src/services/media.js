/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
import { get } from "../services/base";

export async function getMedia({ libraryID, sort }) {
  let params = new URLSearchParams({
    libraryID: libraryID
  });
  if (sort) {
    params.append("sort", sort)
  }
  return get({
    url: '/api/v1/media?' + params
  });
}

export async function getSingleMedia({ mediaID, includeStreams }) {
  let params = new URLSearchParams();
  if (includeStreams) {
    params.append("includeStreams", includeStreams);
  }
  return get({
    url: '/api/v1/media/' + mediaID + "?" + params
  });
}
