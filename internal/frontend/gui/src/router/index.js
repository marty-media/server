/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
import { createRouter, createWebHashHistory } from 'vue-router';
import { useUIStore } from "../stores/ui";

const router = createRouter({
  history: createWebHashHistory(),
  routes: [{
    path: '/',
    name: 'home',
    redirect: {
      name: 'welcome'
    },
    component: () => import('../layouts/Home.vue'),
    children: [{
      path: '',
      name: 'welcome',
      component: () => import('../views/main/Welcome.vue')
    }, {
      path: 'library/:libraryID',
      name: 'library',
      component: () => import('../views/main/Library.vue')
    }, {
      path: 'media/:mediaID',
      name: 'media',
      component: () => import('../views/main/Media.vue')
    }]
  }, {
    path: '/playback/:playbackID',
    name: 'playback',
    component: () => import('../views/Player.vue')
  }, {
    path: '/settings',
    name: 'settings',
    redirect: {
      name: 'settings-dashboard'
    },
    component: () => import('../layouts/Settings.vue'),
    children: [{
      path: '',
      name: 'settings-dashboard',
      component: () => import('../views/settings/Dashboard.vue')
    }, {
      path: 'libraries',
      name: 'settings-libraries',
      component: () => import('../views/settings/Libraries.vue')
    }, {
      path: 'libraries/:libraryID',
      name: 'settings-library',
      component: () => import('../views/settings/Library.vue')
    }]
  }]
});

router.afterEach((to, _from, _failure) => {
  // check settings status
  const uiStore = useUIStore();
  if (to.name.startsWith("settings-")) {
    uiStore.isSettings = true;
  } else {
    uiStore.isSettings = false;
  }
});

export default router;
