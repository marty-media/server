/*
 * Copyright (C) 2022 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */

package main

import (
	"context"
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/marty-media/server/internal/api"
	"gitlab.com/marty-media/server/internal/backend"
	"gitlab.com/marty-media/server/internal/database"
	"gitlab.com/marty-media/server/internal/frontend"
	"gitlab.com/marty-media/server/internal/playback"
	"gitlab.com/marty-media/server/internal/settings"
	"gitlab.com/marty-media/server/internal/settingsdb"
	"gitlab.com/marty-media/server/internal/version"
	"gitlab.com/marty-media/server/internal/webserver"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

var (
	flagSetServer    = flag.NewFlagSet("server", flag.ContinueOnError)
	flagServerConfig = flagSetServer.String("config", "", "path to config file (json)")
)

func init() {
	// prepare logger
	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})
	// start with debug level
	log.SetLevel(log.DebugLevel)
	log.SetOutput(os.Stdout)
	log.SetReportCaller(true)
}

func main() {
	// check for flag set
	if len(os.Args) <= 1 {
		showHelp()
		os.Exit(1)
	}

	// flag handling based on flag set
	switch os.Args[1] {
	case "server":
		if err := flagSetServer.Parse(os.Args[2:]); err != nil {
			os.Exit(1)
		}

		// start server instance
		runServer(*flagServerConfig)
	case "version":
		fmt.Println(version.Version)
	case "help":
		showHelp()
	default:
		fmt.Println("invalid subcommand", os.Args[1])
		fmt.Println()
		showHelp()
		os.Exit(1)
	}
}

func showHelp() {
	fmt.Println("server: run as main server process")
	flagSetServer.PrintDefaults()
	fmt.Println()
	fmt.Println("version: shows the binary version")
	fmt.Println()
	fmt.Println("help: shows this help")
}

func runServer(settingsLocation string) {
	// load settings
	if err := settings.Instance.Load(settingsLocation); err != nil {
		log.WithField("inherit", err).Fatal("unable to load settings")
	}

	// initialize database
	if err := database.InitDatabase(); err != nil {
		log.WithField("inherit", err).Fatal("unable to initialize database")
	}
	if err := database.Instance.Upgrade(); err != nil {
		log.WithField("inherit", err).Fatal("unable to upgrade database")
	}

	// load settings
	settingsdb.Instance.RefreshDebugLog()

	// start backend instance
	backend.Instance.Start()

	// wait for shutdown request
	signalChannel := make(chan os.Signal, 1)

	// Ctrl+C ==> Interrupt
	signal.Notify(signalChannel, os.Interrupt, syscall.SIGTERM)

	// create new shutdown context for webserver
	webServerContext, cancel := context.WithCancel(context.Background())

	// wait for signal
	go func() {
		<-signalChannel
		log.Info("shutdown signal received")
		cancel()
	}()

	// run webserver
	runWebServer(webServerContext, cancel)
}

func runWebServer(ctx context.Context, cancel context.CancelFunc) {
	var waitGroup sync.WaitGroup
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()

		// prepare webserver
		webserver.Instance.Prepare([]webserver.ServerInterface{
			frontend.Instance,
			api.Instance,
			playback.WebServerInstance,
		})

		// start and run webserver
		if err := webserver.Instance.Run(); err != nil {
			log.WithField("inherit", err).Fatal("unable to start webserver")
			cancel()
			return
		}
	}()
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		<-ctx.Done()
		log.Info("initiate webserver shutdown")
		webserver.Instance.Shutdown()
	}()

	waitGroup.Wait()
	log.Info("webserver closed")
}
